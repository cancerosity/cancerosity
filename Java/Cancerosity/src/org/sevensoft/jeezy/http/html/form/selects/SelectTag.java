package org.sevensoft.jeezy.http.html.form.selects;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.sevensoft.commons.collections.Transformer;
import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.superstrings.HtmlHelper;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.AbstractTag;

/**
 * @author sks 03-Nov-2005 00:28:14
 * 
 */
public class SelectTag extends AbstractTag {

	private MultiValueMap<String, SelectOption>						options;
	private String											any;
	private Comparator<org.sevensoft.jeezy.http.html.form.selects.SelectOption>	comparator;

	public SelectTag(RequestContext context, String name) {
		this(context, name, null);
	}

	public SelectTag(RequestContext context, String name, Object value) {
		super(context);

		setName(name);
		setSize(1);

		setValue(EntityObject.getEqualityString(value));
		this.options = new MultiValueMap(new LinkedHashMap());
	}

	public SelectTag(RequestContext context, String name, Object value, Collection options) {
		this(context, name, value, options, null);
	}

	public SelectTag(RequestContext context, String name, Object value, Collection options, String any) {
		this(context, name, value);

		this.any = any;

		this.any = any;
		if (options != null) {
			addOptions(options);
		}
	}

	public SelectTag(RequestContext context, String name, Object value, Map options, String any) {
		this(context, name, value);

		this.any = any;
		if (options != null) {
			addOptions(options);
		}
	}

	public SelectTag(RequestContext context, String name, Object value, Object[] options) {
		this(context, name, value, Arrays.asList(options), null);
	}

	public SelectTag(RequestContext context, String name, Object value, Object[] options, String any) {
		this(context, name, value, Arrays.asList(options), any);
	}

	public void addOption(Object obj) {
		addOption(null, obj, obj);
	}

	public void addOption(Object value, Object label) {
		addOption(null, value, label);
	}

	public void addOption(String optGroup, Object value, Object label) {
		options.put(optGroup, new SelectOption(value, label));
	}

	public void addOptions(Collection c) {
		addOptions(null, c);
	}

	public <E extends Object, F extends Object> void addOptions(Collection<E> objs, Transformer<E, F> transformer) {
		for (E e : objs) {
			addOption(transformer.transform(e));
		}
	}

	public void addOptions(Map map) {
		addOptions(null, map);
	}

	public void addOptions(Object[] array) {
		addOptions(null, array);
	}

	public void addOptions(String optGroup, Collection c) {
		for (Object obj : c) {
			addOption(optGroup, obj, obj);
		}
	}

	public void addOptions(String optGroup, Map map) {
		Set<Map.Entry> entries = map.entrySet();
		for (Map.Entry entry : entries) {
			addOption(optGroup, entry.getKey(), entry.getValue());
		}
	}

	public void addOptions(String optGroup, Object[] array) {
		addOptions(optGroup, Arrays.asList(array));
	}

	public void removeOptions() {
		options.clear();
	}

	public void setAny(String any) {
		this.any = any;
	}

	public SelectTag setAutoSubmit() {
		setAutoSubmit(true);
		return this;
	}

	public void setComparator(Comparator<SelectOption> comparator) {
		this.comparator = comparator;
	}

	public void setLabelComparator() {
		
		setComparator(new Comparator<SelectOption>() {

			public int compare(SelectOption o1, SelectOption o2) {
				return o1.getLabel().compareTo(o2.getLabel());
			}
		});
	}

	public void setOptGroupComparator(Comparator c) {
		this.options = new MultiValueMap(new TreeMap(c));
	}

	@Override
	public String toString() {
        String value = null;
        if (!ignoreParamValue) {
            value = getParam();
        }

        if (value == null) {
            value = getValue();
        }

        if (value != null) {
            value = HtmlHelper.escape(value);
        }
        //value can have only <options>
        setValue(null);

		StringBuilder sb = doOpenTag("select");

		if (any != null) {
			sb.append("\n<option value=''>");
			sb.append(any);
			sb.append("</option>");
		}



		for (Map.Entry<String, List<SelectOption>> entry : options.entryListSet()) {

			String group = entry.getKey();
			List<SelectOption> o = entry.getValue();

			if (comparator != null) {
				Collections.sort(o, comparator);
			}

			if (group != null) {
				sb.append("<optgroup label=\"" + group + "\"/>\n");
			}

			for (SelectOption option : o) {
				option.render(sb, value, width);
			}
		}

		sb.append("\n</select>");

		return sb.toString();
	}

}
