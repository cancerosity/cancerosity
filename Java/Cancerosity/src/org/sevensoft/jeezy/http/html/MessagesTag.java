package org.sevensoft.jeezy.http.html;

import java.util.Map;

import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.RequestContext.MessageType;

/**
 * @author sks 30-Nov-2005 09:32:03
 * 
 */
public class MessagesTag {

	private final RequestContext	context;

	public MessagesTag(RequestContext context) {
		this.context = context;
	}

	@Override
	public String toString() {

		if (!context.hasMessages())
			return "";

		StringBuilder sb = new StringBuilder();

		for (Map.Entry<MessageType, String> entry : context.getMessages().entrySet()) {

			switch (entry.getKey()) {

			default:
			case Error:
			case Info:
			case Warning:

				sb.append("<div class=\"message_");
				sb.append(entry.getKey().toString().toLowerCase());
				sb.append("\">");
				sb.append(entry.getValue());
				sb.append("</div>");

				break;

			case Popup:

				// fix string for popup - no line breaks in javascript please !
				String popup = entry.getValue().replace("'", "\\'");
				popup = popup.replace("\n", "\\n");

				sb.append("<script>");
				sb.append("window.alert('" + popup + "');");
				sb.append("</script>");

				break;
			}
		}

		return sb.toString();
	}
}
