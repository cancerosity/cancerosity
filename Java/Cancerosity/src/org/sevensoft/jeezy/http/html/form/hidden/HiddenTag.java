package org.sevensoft.jeezy.http.html.form.hidden;

import java.util.Map;
import java.util.Map.Entry;

import org.sevensoft.jeezy.http.html.AbstractTag;

/**
 * @author sks 07-Nov-2005 15:38:44
 * 
 */
public class HiddenTag extends AbstractTag {

	public HiddenTag(Entry<String, String> entry) {
		this(entry.getKey(), entry.getValue());
	}

	public HiddenTag(String name, Object value) {
		super(null);
		setName(name);
		setValue(value);
	}

	@Override
	public String toString() {

		if (getValue() == null) {
			return "";
		}

		Map<String, String> params = getParameters();
		params.put("type", "hidden");

		//		String param = getParam();
		//		if (param != null) {
		//			params.put("value", param);
		//		}

		return doTag("input", params) + "\n";

	}

}
