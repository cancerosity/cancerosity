package org.sevensoft.jeezy.http.html.table;

/**
 * @author sks 22-Nov-2005 15:50:20
 * 
 */
public class TableTag {

	private String	clazz;
	private int		cellpadding, cellspacing;
	private String	align;
	private String	id;
	private String	caption;
	private String	style;
	private String	width;
	private String	height;
	private String	onMouseOut, onMouseOver;

	public TableTag() {
		this(null, null, 0, 0);
	}

	public TableTag(String clazz) {
		this(clazz, null, 0, 0);
	}

	public TableTag(String clazz, int cellspacing, int cellpadding) {
		this(clazz, null, cellspacing, cellpadding);
	}

	public TableTag(String clazz, String align) {
		this(clazz, align, 0, 0);
	}

	public TableTag(String clazz, String align, int cellspacing, int cellpadding) {
		this.clazz = clazz;
		this.align = align;
		this.cellspacing = cellspacing;
		this.cellpadding = cellpadding;
	}

	public TableTag setAlign(String align) {
		this.align = align;
		return this;
	}

	public TableTag setCaption(String caption) {
		this.caption = caption;
		return this;
	}

	public TableTag setHeight(String height) {
		this.height = height;
		return this;
	}

	public TableTag setId(String id) {
		this.id = id;
		return this;
	}

	public void setOnMouseOut(String string) {
		onMouseOut = string;
	}

	public final void setOnMouseOver(String onMouseOver) {
		this.onMouseOver = onMouseOver;
	}

	public TableTag setStyle(String style) {
		this.style = style;
		return this;
	}

	public TableTag setWidth(String width) {
		this.width = width;
		return this;
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		sb.append("\n<table cellspacing=\"");
		sb.append(cellspacing);
		sb.append("\" cellpadding=\"");
		sb.append(cellpadding);
		sb.append("\"");

		if (width != null) {
			sb.append(" width=\"");
			sb.append(width);
			sb.append("\"");
		}

		if (height != null) {
			sb.append(" height=\"");
			sb.append(height);
			sb.append("\"");
		}

		if (clazz != null) {
			sb.append(" class=\"");
			sb.append(clazz);
			sb.append("\"");
		}

		if (id != null) {
			sb.append(" id=\"");
			sb.append(id);
			sb.append("\"");
		}

		if (align != null) {
			sb.append(" align=\"");
			sb.append(align);
			sb.append("\"");
		}

		if (style != null) {
			sb.append(" style=\"");
			sb.append(style);
			sb.append("\"");
		}

		if (onMouseOver != null) {
			sb.append(" onmouseover=\"");
			sb.append(onMouseOver);
			sb.append("\"");
		}

		if (onMouseOut != null) {
			sb.append(" onmouseout=\"");
			sb.append(onMouseOut);
			sb.append("\"");
		}

		sb.append(">\n");

		if (caption != null) {
			sb.append("<caption>");
			sb.append(caption);
			sb.append("</caption>\n");
		}

		return sb.toString();
	}

}
