package org.sevensoft.jeezy.http.html.form.bool;

import org.sevensoft.jeezy.http.RequestContext;

import java.util.Map;

/**
 * @author RoshkovskayaTI
 * @version: $id$
 */
public class SimpleRadioTag extends RadioTag{

    public SimpleRadioTag(RequestContext context, String name, Object value) {
        super(context, name, value);
    }

    public SimpleRadioTag(RequestContext context, String name, Object value, boolean checked) {
        super(context, name, value, checked);
    }

    @Override
    public String toString() {
        Map<String, String> map = getParameters();
		map.put("type", "radio");
        return doTag("input", map).toString();
    }
}
