package org.sevensoft.jeezy.http.html.form.submit;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.Handler;
import org.sevensoft.jeezy.http.Urls;
import org.sevensoft.jeezy.http.html.AbstractTag;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 17-Nov-2005 18:26:40
 * 
 */
public class ButtonTag extends AbstractTag {

	private Object	label;
	private String	href;
	private Map		params;
	private String	confirm;
	private Link	link;

	public ButtonTag(Class<? extends Handler> clazz, String action, Object label) {
		this(clazz, action, label, (Object[]) null);
	}

	public ButtonTag(Class<? extends Handler> clazz, String action, Object label, Object... array) {

		super(null);

		this.label = label;
		this.href = Handler.getPath(clazz);

		params = new LinkedHashMap();
		if (array != null) {

			if (array.length % 2 == 1) {
				throw new RuntimeException("unequal params");
			}

			for (int n = 0; n < array.length; n = n + 2) {
				if (array[n] != null && array[n + 1] != null) {
					params.put(array[n], array[n + 1]);
				}
			}
		}

		if (action != null) {
			params.put("_a", action);
		}
	}

	public ButtonTag(Link link, Object label) {

		super(null);

		this.link = link;
		this.label = label;
        params = new LinkedHashMap();
	}

	public ButtonTag(Object label) {
		super(null);
		this.label = label;
	}

	public ButtonTag(String href, Object label) {
		super(null);

		this.href = href;
		this.label = label;
        params = new LinkedHashMap();
	}

	public void addParameter(String name, Object value) {
		params.put(name, value);
	}

	public void addParameters(String name, Map map) {
		Set<Map.Entry> set = map.entrySet();
		for (Map.Entry entry : set) {
			params.put(name + "_" + EntityObject.getEqualityString(entry.getKey()), entry.getValue());
		}
	}

	public String getUrl() {

		if (link != null) {
			return link.toString();
		}

		else if (href != null) {
			return Urls.buildUrl(href, params);
		}

		else {
			return null;
		}
	}

	public ButtonTag setConfirmation(String confirm) {
		this.confirm = confirm.replace("'", "\\'");
		return this;
	}

	@Override
	public String toString() {

		Map<String, String> map = getParameters();
		map.put("type", "button");
		map.put("value", EntityObject.getEqualityString(label));

		String url = getUrl();

		if (url != null) {
			if (confirm == null) {
				map.put("onclick", "javascript: window.location='" + url + "';");
			} else {
				map.put("onclick", "javascript: if (window.confirm('" + confirm + "')) { window.location='" + url + "'; } ");
			}
		}

		return doTag("input", map).toString();
	}

}
