package org.sevensoft.jeezy.http.html.form.date;

import org.sevensoft.commons.samdate.Day;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;


/**
 * @author sks 19 Mar 2007 12:11:05
 *
 */
public class DayTag extends SelectTag {

	public DayTag(RequestContext context, String name) {
		super(context, name);
		addOptions(Day.values());
	}

}
