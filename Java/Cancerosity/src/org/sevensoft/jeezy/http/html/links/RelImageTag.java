package org.sevensoft.jeezy.http.html.links;

import org.sevensoft.jeezy.http.html.AbstractTag;

import java.util.Map;

/**
 * User: Tanya
 * Date: 22.05.2012
 */
public class RelImageTag extends AbstractTag {


    public RelImageTag(Object label) {
        super(null);
        setContent(label);
    }

    @Override
    public String toString() {

        Map<String, String> params = getParameters();
        return doTag("a", params).toString();
    }
}
