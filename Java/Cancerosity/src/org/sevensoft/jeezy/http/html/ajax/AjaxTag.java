package org.sevensoft.jeezy.http.html.ajax;

import java.util.Map;

import org.sevensoft.jeezy.http.html.AbstractTag;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 7 Jun 2006 15:49:33
 *
 */
public class AjaxTag extends AbstractTag {

	public AjaxTag(Link link, String label, String elementId, String jsEl) {

		super(null);
		setValue(label);
		setOnClick("ajaxRequest('" + link.toString() + "&" + jsEl + "=' + document.getElementById('" + jsEl + "').value, '" + elementId + "');");
	}

	@Override
	public String toString() {

		Map<String, String> map = getParameters();
		map.put("type", "button");

		return doTag("input", map).toString();
	}
}
