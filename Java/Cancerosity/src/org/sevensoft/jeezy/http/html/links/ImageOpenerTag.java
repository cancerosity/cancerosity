package org.sevensoft.jeezy.http.html.links;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

import java.util.Map;

/**
 * @author sks 17-Nov-2005 18:26:40
 */
public class ImageOpenerTag extends WindowOpenerTag {

    private final String src;


    public ImageOpenerTag(RequestContext context, Link link, String src, int width, int height) {
        super(context, link.toString(), width, height);
        this.src = src;
    }

    @Override
    public String toString() {

        setOnClick("javascript:window.open('" + getUrl() + "','" + getWindowName() + "', '" + StringHelper.implode(getArgs(), ",", true)
                + "'); return false;");

        Map<String, String> params = getParameters();
        params.put("type", "image");
        params.put("src", src);

        return doTag("input", params).toString();
    }

}