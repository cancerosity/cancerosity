package org.sevensoft.jeezy.http.html.form.bool;

import java.util.Map;

import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.AbstractTag;

/**
 * @author sks 14-Nov-2005 00:24:26
 * 
 */
public class RadioTag extends AbstractTag {

	public RadioTag(RequestContext context, String name, Object value) {
		super(context);
		setName(name);
		setValue(value);
	}

	public RadioTag(RequestContext context, String name, Object value, boolean checked) {
		this(context, name, value);
		super.setChecked(checked);
	}

	@Override
	public String toString() {

		Map<String, String> map = getParameters();
		map.put("type", "radio");

		String param = getParam();
		if (param != null) {

			if (param.equals(getValue())) {

				map.put("checked", "true");

			} else {

				map.remove("checked");
			}
		}

		return doTag("input", map).toString();
	}

}
