package org.sevensoft.jeezy.http.html.form.hidden;

import java.util.Map;

import org.sevensoft.jeezy.db.EntityObject;

/**
 * @author sks 08-Mar-2006 13:58:10
 *
 */
public class HiddenTags {

	private final String					name;
	private Map<? extends Object, ? extends Object>	map;

	public <K, V> HiddenTags(String string, Map map) {
		this.name = string;
		this.map = map;
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();
		for (Map.Entry<? extends Object, ? extends Object> entry : map.entrySet()) {
			sb.append(new HiddenTag(name + "~" + EntityObject.getEqualityString(entry.getKey()), entry.getValue()));
		}
		return sb.toString();
	}

}
