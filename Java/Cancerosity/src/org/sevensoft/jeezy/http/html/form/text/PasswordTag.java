package org.sevensoft.jeezy.http.html.form.text;

import java.util.Map;

import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.AbstractTag;

/**
 * @author sks 03-Nov-2005 00:28:14
 * 
 */
public class PasswordTag extends AbstractTag {

	public PasswordTag(RequestContext context, String name) {
		super(context);
		setName(name);
	}

	public PasswordTag(RequestContext context, String name, int size) {
		this(context, name);
		setSize(size);
	}

	public PasswordTag(RequestContext context, String name, Object value) {
		this(context, name);
		if (value != null)
			setValue(escape(value.toString()));
	}

	public PasswordTag(RequestContext context, String name, Object value, int size) {
		this(context, name, value);
		setSize(size);
	}

	protected String escape(String string) {
		return string.replace("\"", "&quot;");
	}

	@Override
	public String toString() {

		Map<String, String> params = getParameters();
		params.put("type", "password");

		String param = getParam();
		if (param != null) {
			params.put("value", param);
		}

		if (!params.containsKey("class")) {
			params.put("class", "password");
		}

		return "\n" + doTag("input", params);
	}
}
