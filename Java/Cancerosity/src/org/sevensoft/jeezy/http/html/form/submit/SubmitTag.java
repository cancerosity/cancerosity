package org.sevensoft.jeezy.http.html.form.submit;

import java.util.Map;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.html.AbstractTag;

/**
 * @author sks 07-Nov-2005 15:51:03
 * 
 */
public class SubmitTag extends AbstractTag {

	private String	confirm;

	public SubmitTag(Object label) {
		this(null, label);
	}

	public SubmitTag(String name, Object label) {
		super(null);
		super.setValue(EntityObject.getEqualityString(label));
		super.setName(name);
	}

	public AbstractTag setConfirmation(String confirm) {
		this.confirm = confirm;
		return this;
	}

	@Override
	public String toString() {

		Map<String, String> params = getParameters();
		params.put("type", "submit");

		if (confirm != null)
			params.put("onclick", "javascript: if (window.confirm('" + confirm + "')) { this.form.submit(); } else { return false; } ");

		return doTag("input", params).toString();
	}
}
