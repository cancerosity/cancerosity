package org.sevensoft.jeezy.http.html.links;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.Handler;
import org.sevensoft.jeezy.http.Urls;
import org.sevensoft.jeezy.http.html.AbstractTag;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 10-Nov-2005 10:35:28
 * 
 */
public class LinkTag extends AbstractTag {

	private String		href;
	private MultiValueMap	params;
	private Link		link;

	public LinkTag(Class<? extends Handler> clazz, String action, Object label) {
		this(clazz, action, label, (Object[]) null);
	}

	public LinkTag(Class<? extends Handler> clazz, String action, Object label, Object... array) {
		this(Handler.getPath(clazz), label, array);

		if (action != null) {
			params.put("_a", action);
		}
	}

	public LinkTag(Link link, Object label) {

		super(null);

		this.link = link;
		this.params = new MultiValueMap(new LinkedHashMap());

		setContent(label);
	}

	public LinkTag(String url) {
		this(url, url);
	}

	public LinkTag(String href, Object label) {
		this(href, label, (Object[]) null);
	}

	public LinkTag(String href, Object label, Object... array) {

		super(null);

		this.href = href;
		setContent(label);

		if (array != null && array.length % 2 > 0) {
			throw new RuntimeException("Unequal number of params: " + array);
		}

		this.params = new MultiValueMap(new LinkedHashMap());

		if (array != null) {
			for (int n = 0; n < array.length; n = n + 2) {
				if (array[n] != null && array[n + 1] != null)
					params.put(array[n], array[n + 1]);
			}
		}

	}

	public void addParameter(String name, Object value) {
		params.put(name, value);
	}

	public void addParameters(String name, Map map) {
		Set<Map.Entry> set = map.entrySet();
		for (Map.Entry entry : set) {
			params.put(name + "_" + EntityObject.getEqualityString(entry.getKey()), entry.getValue());
		}
	}

	public String getUrl() {
		if (link != null) {
			return link.toString();
		} else {
			return Urls.buildUrl(href, params);
		}
	}

	public LinkTag removeParameters(String key) {
		params.removeAll(key);
		return this;
	}

	public LinkTag setAjaxRequest(Link link) {
		setOnClick("makeRequest('" + link + "');");
		return this;
	}

	public LinkTag setConfirmation(String confirm) {
		setOnClick("if (window.confirm('" + confirm + "')) {  return true; } else { return false; }");
		return this;
	}

	public LinkTag setLabel(Object label) {
		setContent(label);
		return this;
	}

	public void setParameter(String name, Object value) {
		params.set(name, value);
	}

	public void setParameters(String key, Map map) {
		removeParameters("attributeValues");
		addParameters(key, map);
	}

	/**
	 * 
	 */
	public void setPrompt(String prompt, String var) {
		setOnClick("var v = prompt('" + prompt.replace("'", "\'") + "'); if (v == null) return false; this.href = this.href + '&" + var
				+ "=' + v; return true; ");
	}

	@Override
	public String toString() {

        //alt is not supported by <a/>
        setAlt(null);

		Map<String, String> params = getParameters();
		params.put("href", getUrl());

		return doTag("a", params).toString();
	}
}
