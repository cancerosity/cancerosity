package org.sevensoft.jeezy.http.html.form.date;

import java.text.ParseException;
import java.util.logging.Logger;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;

/**
 * @author sks 4 Aug 2006 11:09:06
 * This class uses the date picker
 * 
 */
public class DateTag {

	private static Logger		logger	= Logger.getLogger("tags");

	/**
	 * 
	 */
	private final RequestContext	context;

	/** 
	 * start year, zero = current
	 */
	private int				startYear;

	/**
	 * A number of years starting from the start year
	 */
	private int				yearRange;

	/**
	 * Date used for current value if set
	 */
	private Date			value;

	/**
	 * If hide day is set then we don't show a day selector, instead we default to the first of that month
	 */
	private boolean			hideDay;

	private final String		param;

	private boolean			optional;

	public DateTag(RequestContext context, String name) {
		this(context, name, (Date) null);
	}

	public DateTag(RequestContext context, String name, Date value) {

		logger.fine("created date tag, name=" + name + ", value=" + value);

		this.context = context;
		this.value = value;
		this.param = context.getCount() + name;

		//		String paramValue = context.getParameter(name);
		//
		//		// if param value is set use that rather than any passed in value
		//		if (paramValue != null) {
		//
		//			try {
		//
		//				logger.fine("replacing with context value=" + paramValue);
		//				value = new Date(paramValue);
		//
		//			} catch (ParseException e) {
		//				e.printStackTrace();
		//			}
		//		}
	}

	public DateTag(RequestContext context, String name, long timestamp) {
		this(context, name, new Date(timestamp));
	}

	public DateTag(RequestContext context, String paramName, String value) throws ParseException {
		this(context, paramName, new Date(value));
	}

	String getParam() {
		return param;
	}

	public final boolean isOptional() {
		return optional;
	}

	public void setHideDay(boolean hideDay) {
		this.hideDay = hideDay;
	}

	public final void setOptional(boolean optional) {
		this.optional = optional;
	}

	public void setStartYear(int startYear) {
		this.startYear = startYear;
	}

	public void setYearRange(int yearRange) {
		this.yearRange = yearRange;
	}

	@Override
	public String toString() {

		if (value == null && !optional) {
			value = new Date();
		}

		StringBuilder sb = new StringBuilder();

		// DAY
		{

			SelectTag dayTag = new SelectTag(null, param);
			//			if (optional) {
			dayTag.setAny("--");
			//			}
			dayTag.setIgnoreParamValue(true);

			if (value != null) {

				String dayValue = value.toString("dd");

				logger.fine("[DateTag] setting day tag value to " + dayValue);
				dayTag.setValue(dayValue);

			}

			for (int n = 1; n <= 31; n++) {
				if (n < 10) {
					dayTag.addOption("0" + n);
				} else {
					dayTag.addOption(n);
				}
			}

			sb.append(dayTag);
		}

		// MONTH
		{

			SelectTag monthTag = new SelectTag(null, param);
			monthTag.setIgnoreParamValue(true);

			if (value != null) {

				String monthValue = value.toString("-MM-");

				logger.fine("[DateTag] setting month tag value to " + monthValue);
				monthTag.setValue(monthValue);

			}

			//			if (optional) {
			monthTag.setAny("--");
			//			}
			monthTag.addOption("-01-", "Jan");
			monthTag.addOption("-02-", "Feb");
			monthTag.addOption("-03-", "Mar");
			monthTag.addOption("-04-", "Apr");
			monthTag.addOption("-05-", "May");
			monthTag.addOption("-06-", "Jun");
			monthTag.addOption("-07-", "Jul");
			monthTag.addOption("-08-", "Aug");
			monthTag.addOption("-09-", "Sep");
			monthTag.addOption("-10-", "Oct");
			monthTag.addOption("-11-", "Nov");
			monthTag.addOption("-12-", "Dec");

			sb.append(monthTag);
		}

		// YEAR
		{

			//			if (hideYear) {
			//
			//				int yearValue = new Date().getYear();
			//				logger.fine("[DateTag] Year is hidden so rendering hidden tag with current year=" + yearValue);
			//
			//				sb.append(new HiddenTag(param, yearValue));

			//			if (yearRange == 0 && endYear == 0) {
			//
			//				logger.fine("[DateTag] rendering year tag as text box");
			//
			//				TextTag yearTag = new TextTag(null, param, 4);
			//				if (value != null) {
			//
			//					String yearValue = value.toString("yyyy");
			//
			//					logger.fine("setting year tag value to " + yearValue);
			//					yearTag.setValue(yearValue);
			//					yearTag.setIgnoreParamValue(true);
			//				}
			//				sb.append(yearTag);
			//
			//			} else {
			//
			//			}

			int start;
			if (startYear == 0) {
				start = new Date().getYear();
			} else {
				start = startYear;
			}

			int end = start + yearRange;

			SelectTag yearTag = new SelectTag(null, param);
			//			if (optional) {
			yearTag.setAny("--");
			//			}
			if (value != null) {
				String yearValue = value.toString("yyyy");
				yearTag.setValue(yearValue);
			}

			for (int n = start; n <= end; n++) {
				yearTag.addOption(String.valueOf(n), String.valueOf(n));
			}

			sb.append(yearTag);
		}

		return sb.toString();
	}
}
