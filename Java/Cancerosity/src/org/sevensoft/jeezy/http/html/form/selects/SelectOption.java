package org.sevensoft.jeezy.http.html.form.selects;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.superstrings.HtmlHelper;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.util.Label;
import org.sevensoft.jeezy.http.util.Selectable;

/**
 * @author sks 23 Sep 2006 11:12:17
 *
 */
public class SelectOption {

    protected String	value;
    protected String	label;

    SelectOption(Object obj) {
        this(obj, obj);
    }

    SelectOption(Object value, Object label) {

        if (value instanceof Selectable) {
            this.value = ((Selectable) value).getValue();

        } else if (value instanceof Date) {
            this.value = ((Date) value).getTimestampString();

        } else if (value instanceof Class) {

            Class clazz = (Class) value;
            this.value = clazz.getName();

        } else if (value.getClass().isEnum()) {
            this.value = ((Enum) value).name();

        } else {
            this.value = EntityObject.getEqualityString(value);
        }

        if (label instanceof Selectable) {
            this.label = ((Selectable) label).getLabel();

        } else if (label instanceof Date) {
            this.label = ((Date) label).toString("dd/MM/yyyy");

        } else if (label instanceof Class) {

            /*
                * Check for select label annotation
                */
            Class clazz = (Class) label;
            Label labelAnno = (Label) clazz.getAnnotation(Label.class);

            if (labelAnno == null) {
                this.label = clazz.getSimpleName();
            } else {
                this.label = labelAnno.value();
            }

        } else if (label != null){
            this.label = label.toString();
        }
    }

    public String getLabel() {
        return label;
    }

    public String getValue() {
        return value;
    }

    public void render(StringBuilder sb, String selectedValue, int width) {

        String optionValue = HtmlHelper.escape(value);
        String optionLabel = HtmlHelper.escape(label);


            if (width > 0) {
                if (optionLabel.length() > width) {
                    optionLabel = optionLabel.substring(0, width);
                }
            }

            sb.append("\n<option value=\"");
            sb.append(optionValue);

            if (optionValue.equalsIgnoreCase(selectedValue)) {
                sb.append("\" selected='selected'>");
            } else {
                sb.append("\">");
            }

            sb.append(optionLabel);
            sb.append(" </option>");
        }
}