package org.sevensoft.jeezy.http.html.form.text;

import java.util.Map;

import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.AbstractTag;

/**
 * @author sks 14-Nov-2005 15:09:40
 * 
 */
public class TextAreaTag extends AbstractTag {

	public TextAreaTag(RequestContext context, String name) {
		this(context, name, null, 0, 0);
	}

	public TextAreaTag(RequestContext context, String name, int cols, int rows) {
		this(context, name, null, cols, rows);
	}

	public TextAreaTag(RequestContext context, String name, Object value) {
		this(context, name, value, 0, 0);
	}

	public TextAreaTag(RequestContext context, String name, Object value, int cols, int rows) {

		super(context);

		setName(name);

		setCols(cols);
		setRows(rows);

		if (value == null)
			setContent("");
		else
			setContent(escape((String) value));
	}

	protected String escape(String string) {
		return string.replace("<", "&lt;").replace(">", "&gt;");
	}

	@Override
	public String toString() {

		Map<String, String> params = getParameters();

		String param = getParam();
		if (param != null)
			setContent(escape(param));

		return "\n" + doTag("textarea", params) + "\n";
	}

}
