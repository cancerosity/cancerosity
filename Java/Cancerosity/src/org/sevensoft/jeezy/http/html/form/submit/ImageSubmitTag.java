package org.sevensoft.jeezy.http.html.form.submit;

import java.util.Map;

import org.sevensoft.jeezy.http.html.AbstractTag;
import org.sevensoft.jeezy.http.html.ImageTag;

/**
 * @author sks 15-Dec-2005 11:13:16
 * 
 */
public class ImageSubmitTag extends AbstractTag {

	public ImageSubmitTag(String src) {
		super(null);
		setSrc(src);
	}

	public ImageSubmitTag(ImageTag imageTag) {
		super(null);
		setSrc(imageTag.toString());
	}

	@Override
	public String toString() {

		Map<String, String> params = getParameters();
		params.put("type", "image");

		return "\n" + doTag("input", params) + "\n";
	}
}
