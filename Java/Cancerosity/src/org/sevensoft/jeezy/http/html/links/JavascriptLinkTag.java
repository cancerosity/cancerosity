package org.sevensoft.jeezy.http.html.links;

import java.util.Map;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 10 Aug 2006 17:34:46
 *
 */
public class JavascriptLinkTag extends WindowOpenerTag {

	public JavascriptLinkTag(RequestContext context, Link link, Object label, int width, int height) {
		this(context, link.toString(), label, width, height);
	}

	public JavascriptLinkTag(RequestContext context, String url, Object label, int width, int height) {
		super(context, url, width, height);
		setContent(label);
	}

	@Override
	public String toString() {

		setOnClick("javascript:window.open('" + getUrl() + "','" + getWindowName() + "', '" + StringHelper.implode(getArgs(), ",", true)
				+ "'); return false;");

		Map<String, String> params = getParameters();
		params.put("href", "#");

		return doTag("a", params).toString();
	}
}
