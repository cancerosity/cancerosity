package org.sevensoft.jeezy.http.html.form;

import java.util.Map;

import org.sevensoft.jeezy.http.html.AbstractTag;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.ecreator.model.stats.browsers.Browser;

/**
 * @author sks 29-Nov-2005 09:35:53
 * 
 */
public class FileTag extends AbstractTag {

	public FileTag(String name) {
		super(null);
		setName(name);
	}

	public FileTag(String name, int size) {
		this(name);
		setSize(size);
	}

	@Override
	public String toString() {
        if (Browser.iOS.equals(Browser.getBrowser(RequestContext.getInstance().getUserAgent()))){
            setId("photo");
            setOnFocus("\n var currentParams = {};\n" +
                    " \n" +
                    " \n" +
                    "document.observe('dom:loaded', function(){\n" +
                    " \n" +
                    "  $(document.body).addClassName('iphone');\n" +
                    " \n" +
                    "\t// We'll check the hash when the page loads in-case it was opened in a new page\n" +
                    "\t// due to memory constraints\n" +
                    "    Picup.checkHash();\t\n" +
                    " \n" +
                    "   // Set some starter params\t\n" +
                    "   currentParams = {\n" +
                    "\t'callbackURL' \t\t: '"+RequestContext.getInstance().getGetRequest()+"',\t\t\t\t\n" +
                    "\t'referrername' \t\t: escape('App Name'),\n" +
//                    "\t'referrerfavicon' \t: escape('http://your-website.com/favicon.ico'),\n" +
                    "\t'purpose'               : escape('Select your photo for the our App.'),\n" +
                    "\t'debug' \t\t: 'false',\n" +
                    "\t'returnThumbnailDataURL': 'true',\n" +
                    "\t'thumbnailSize'         : '80'\n" +
                    "\t  };\n" +
                    " \n" +
                    "    Picup.convertFileInput($('photo'), currentParams);\n" +
                    " \n" +
                    "\t});");
        }
		Map<String, String> params = getParameters();
		params.put("type", "file");

		if (!params.containsKey("class")) {
			params.put("class", "file");
		}

		return doTag("input", params).toString();
	}

}
