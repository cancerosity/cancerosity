package org.sevensoft.jeezy.http.html;

import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 29 Mar 2007 10:08:40
 *
 */
public class DivTag extends AbstractTag {

	public DivTag() {
		super(null);
	}

	public DivTag(RequestContext context) {
		super(context);
	}

	@Override
	public String toString() {
		return super.doTag("div").toString();
	}

}
