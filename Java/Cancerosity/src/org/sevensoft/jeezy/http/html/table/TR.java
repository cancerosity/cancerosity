package org.sevensoft.jeezy.http.html.table;

import org.sevensoft.jeezy.http.html.AbstractTag;

/**
 * @author sks 28 Apr 2006 17:25:31
 *
 */
public class TR extends AbstractTag {

	public TR() {
		super(null);
	}

	@Override
	public String toString() {
		return doOpenTag("tr").toString();
	}
}
