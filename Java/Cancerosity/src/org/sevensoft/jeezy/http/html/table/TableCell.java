package org.sevensoft.jeezy.http.html.table;

import org.sevensoft.jeezy.http.html.AbstractTag;

/**
 * @author sks 28 Apr 2006 17:25:31
 *
 */
public class TableCell extends AbstractTag {

	public TableCell() {
		super(null);
	}

	@Override
	public String toString() {
		return doOpenTag("td").toString();
	}
}
