package org.sevensoft.jeezy.http.html.form.bool;

import java.util.List;
import java.util.Map;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.AbstractTag;

/**
 * @author sks 14-Nov-2005 00:24:26
 * 
 */
public class CheckTag extends AbstractTag {

	public CheckTag(RequestContext context, String name, Object value) {
		this(context, name, value, false);
	}

	public CheckTag(RequestContext context, String name, Object value, boolean checked) {
		super(context);
		setName(name);
		setChecked(checked);
		setValue(EntityObject.getEqualityString(value));
	}

	@Override
	public String toString() {

		Map<String, String> map = getParameters();
		map.put("type", "checkbox");

		List<String> params = getParams();
		if (params != null)
			if (params.contains(getValue()))
				map.put("checked", "true");

		return doTag("input", map).toString();
	}
}
