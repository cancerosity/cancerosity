package org.sevensoft.jeezy.http.html.links;

import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Link;

import java.util.Map;

/**
 * @author sks 17-Nov-2005 18:26:40
 */
public class ButtonOpenerTag extends WindowOpenerTag {

    public ButtonOpenerTag(RequestContext context, Link link, Object label, int width, int height) {
        this(context, link.toString(), label, width, height);
    }

    public ButtonOpenerTag(RequestContext context, String url, Object label, int width, int height) {
        super(context, url, width, height);
        setValue(label);
    }

    @Override
    public String toString() {

        setOnClick("javascript:window.open('" + getUrl() + "','" + getWindowName() + "', '" + StringHelper.implode(getArgs(), ",", true)
                + "'); return false;");

        Map<String, String> params = getParameters();
        params.put("type", "button");

        return doTag("input", params).toString();
    }

}
