package org.sevensoft.jeezy.http.html.form.selects;

import java.util.List;

/**
 * @author sks 11 Aug 2006 22:21:55
 *
 */
public class Node {

	private final List<Object>	data;
	private final Object		owner;

	public Node(Object owner, List<Object> data) {
		this.owner = owner;
		this.data = data;
	}

}
