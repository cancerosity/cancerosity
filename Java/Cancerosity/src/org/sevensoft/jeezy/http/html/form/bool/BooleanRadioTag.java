package org.sevensoft.jeezy.http.html.form.bool;

import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.AbstractTag;

/**
 * @author sks 14-Nov-2005 00:24:26
 * 
 */
public class BooleanRadioTag extends AbstractTag {

	private final String	name;
	private final boolean	checked;
	private boolean		autoSubmit;
	private String		yesLabel, noLabel;

	public BooleanRadioTag(RequestContext context, String name, boolean checked) {
		super(context);
		this.name = name;
		this.checked = checked;

		this.yesLabel = "Yes";
		this.noLabel = "No";
	}

	public BooleanRadioTag setAutoSubmit(boolean autoSubmit) {
		this.autoSubmit = autoSubmit;
		return this;
	}

	public BooleanRadioTag setTitles(String yesLabel, String noLabel) {
		this.yesLabel = yesLabel;
		this.noLabel = noLabel;
		return this;
	}

	public String toString() {

		RadioTag yes = new RadioTag(context, name, true, checked == true);
		yes.setAutoSubmit(autoSubmit);

		RadioTag no = new RadioTag(context, name, false, checked == false);
		no.setAutoSubmit(autoSubmit);

		StringBuilder sb = new StringBuilder();
		sb.append(yesLabel);
		sb.append(" ");
		sb.append(yes);

		sb.append(" ");
		sb.append(noLabel);
		sb.append(" ");
		sb.append(no);

		return sb.toString();
	}

}
