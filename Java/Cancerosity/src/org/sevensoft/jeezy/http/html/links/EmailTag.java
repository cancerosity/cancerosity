package org.sevensoft.jeezy.http.html.links;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @author sks 19-Dec-2005 18:18:45
 * 
 */
public class EmailTag {

	private final String	email;
	private final String	text;
	private String		cc;
	private String		bcc;

	private String		subject;

    private String onClick;

	public EmailTag(String email) {
		this.email = email;
		this.text = null;
	}

	public EmailTag(String email, String text) {
		this.email = email;
		this.text = text;
	}

	public String getBcc() {
		return bcc;
	}

	public String getCc() {
		return cc;
	}

    public String getOnClick() {
        return onClick;
    }

    public String getEmail() {
		return email;
	}

	public String getSubject() {
		return subject;
	}

	public String getText() {
		return text;
	}

	public void setBcc(String bcc) {
		this.bcc = bcc;
	}

	public void setCc(String cc) {
		this.cc = cc;
	}

	public final void setSubject(String subject) {
		this.subject = subject;
	}

    public void setOnClick(String onClick) {
        this.onClick = onClick;
    }

    @Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		sb.append("<a href='mailto:");
		sb.append(email);

		Map<String, String> params = new HashMap();
		if (subject != null) {
			params.put("subject", subject);
		}
		if (bcc != null) {
			params.put("bcc", bcc);
		}
		if (cc != null) {
			params.put("cc", cc);
		}

		if (params.size() > 0) {
			sb.append("?");

			Iterator<Map.Entry<String, String>> iter = params.entrySet().iterator();
			while (iter.hasNext()) {

				Map.Entry<String, String> entry = iter.next();

				sb.append(entry.getKey());
				sb.append("=");
				sb.append(entry.getValue());

				if (iter.hasNext()) {
					sb.append("&");
				}
			}
		}

        sb.append("' ");

        if (onClick != null) {
            sb.append(" onclick=\"" + onClick + "\"");
        }

		sb.append(">");

		if (text == null) {
			sb.append(email);
		} else {
			sb.append(text);
		}

		sb.append("</a>");

		return sb.toString();
	}

}
