package org.sevensoft.jeezy.http.html.form.date;

import org.sevensoft.commons.samdate.Month;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;

/**
 * @author sks 19 Mar 2007 12:07:23
 *
 */
public class MonthTag extends SelectTag {

	public MonthTag(RequestContext context, String name) {
		super(context, name);

		addOptions(Month.values());
	}

}
