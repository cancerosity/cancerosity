package org.sevensoft.jeezy.http.html.form.date;

import java.util.logging.Logger;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.form.selects.SelectTag;

/**
 * @author sks 6 Dec 2006 16:29:00
 *
 */
public class MonthDateTag {

	private static final Logger	logger	= Logger.getLogger("tags");

	private final RequestContext	context;
	private final String		name;
	private Date			value;
	private Date			end;
	private Date			start;
	private boolean			autoSubmit;

	public MonthDateTag(RequestContext context, String name, Date v) {
		this.context = context;
		this.name = name;

		// reset value to start of month as this is a month tag!
		if (v != null) {
			this.value = v.beginMonth();
		}

		this.end = new Date().beginMonth();
		setMonths(12);

		logger.fine("[MonthTag] created, value=" + value.toDateString() + ", name=" + name + ", end=" + end.toDateString() + ", start="
				+ start.toDateString());
	}

	public final void setAutoSubmit(boolean autoSubmit) {
		this.autoSubmit = autoSubmit;
	}

	public final MonthDateTag setEnd(Date end) {
		this.end = end;
		return this;
	}

	public MonthDateTag setMonths(int i) {
		this.start = end.removeMonths(i);
		return this;
	}

	@Override
	public String toString() {

		SelectTag tag = new SelectTag(context, name, value);
		tag.setIgnoreParamValue(true);
		tag.setAutoSubmit(autoSubmit);

		Date date = start;
		while (!date.isAfter(end)) {
			tag.addOption(date.getTimestampString(), date.toString("MMM yyyy"));
			date = date.nextMonth();
		}

		return tag.toString();

	}

}
