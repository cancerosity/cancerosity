package org.sevensoft.jeezy.http.html.form.text;

import java.util.Map;

import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.html.AbstractTag;

/**
 * @author sks 03-Nov-2005 00:28:14
 * 
 */
public class TextTag extends AbstractTag {

	public TextTag(RequestContext context, String name) {
		this(context, name, null, 0);
	}

	public TextTag(RequestContext context, String name, int size) {
		this(context, name, null, size);
	}

	public TextTag(RequestContext context, String name, Object value) {
		this(context, name, value, 0);
	}

	public TextTag(RequestContext context, String name, Object value, int size) {
		super(context);

		setSize(size);
		if (value != null) {
			setValue(value);
		}

		setName(name);
	}

	@Override
	protected String escape(String string) {
		return string.replace("&", "&amp;").replace("\"", "&quot;");
	}

	@Override
	public String toString() {

		Map<String, String> params = getParameters();
		params.put("type", "text");

		if (!ignoreParamValue) {
			String param = getParam();
			if (param != null) {
				params.put("value", escape(param));
			}
		}

		if (!params.containsKey("class")) {
			params.put("class", "text");
		}

		return "\n" + doTag("input", params);
	}
}
