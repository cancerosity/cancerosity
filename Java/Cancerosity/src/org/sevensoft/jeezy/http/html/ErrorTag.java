package org.sevensoft.jeezy.http.html;

import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 27-Nov-2005 15:15:18
 * 
 */
public class ErrorTag {

	private RequestContext	context;
	private String		name;
	private String		message;
	private String		prefix;

	public ErrorTag(RequestContext context, String name) {
		this(context, name, null, null);
	}

	public ErrorTag(RequestContext context, String name, String prefix) {
		this(context, name, null, prefix);
	}

	@Deprecated
	public ErrorTag(RequestContext context, String name, String message, String prefix) {
		this.context = context;
		this.name = name;
		this.message = message;
		this.prefix = prefix;
	}

	public final ErrorTag setMessage(String message) {
		this.message = message;
		return this;
	}

	public final ErrorTag setPrefix(String prefix) {
		this.prefix = prefix;
		return this;
	}

	@Override
	public String toString() {

		String error = context.getError(name);
		if (error == null) {
			return "";
		}

		if (message != null) {
			error = message;
		}

		StringBuilder sb = new StringBuilder();
		if (prefix != null) {
			sb.append(prefix);
		}

		sb.append("<span class='ec_error'>" + error + "</span>");

		return sb.toString();
	}
}
