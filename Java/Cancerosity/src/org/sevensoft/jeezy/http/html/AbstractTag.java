package org.sevensoft.jeezy.http.html;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 10-Nov-2005 10:45:00
 * 
 */
public class AbstractTag {

	private String				name, value;
	private String				onClick, onChange, onSubmit, onMouseOver, onMouseOut, onKeyDown;
	private boolean				disabled;
	protected final RequestContext	context;
	private String				clazz;
	private String				id;
	private String				style;
	protected String				target;
	private boolean				readOnly;
	protected int				width;
	private int					height;
	private String				alt;
	private int					border;
	private String				align;
	private String				onKeyUp;
	private int					rows;
	private int					cols;
	private String				content;
	private int					size;
	private String				src;
	private boolean				checked;
	private String				onFocus;
	protected boolean				ignoreParamValue;
	private String				legend;
	private boolean				multiple;
	private String				rel;
	private String				title;

	protected AbstractTag(RequestContext context) {
		this.context = context;
		this.border = -1;
	}

	public AbstractTag addOnClick(String s) {

		if (onClick == null) {
			onClick = "";
		}

		onClick = onClick + " " + s;

		return this;
	}

	protected final StringBuilder doOpenTag(String tag) {
		return doOpenTag(tag, getParameters());
	}

	protected final StringBuilder doOpenTag(String tag, Map<String, String> params) {

		StringBuilder sb = new StringBuilder("\n<");
		sb.append(tag);
		sb.append(" ");

		for (Entry<String, String> entry : params.entrySet()) {
			sb.append(entry.getKey());
			sb.append("=\"");
			sb.append(entry.getValue());
			sb.append("\" ");
		}

		sb.append(">");

		if (content != null) {
			sb.append(content);
		}

		return sb;
	}

	public StringBuilder doTag(String string) {
		return doTag(string, getParameters());
	}

	protected final StringBuilder doTag(String tag, Map<String, String> params) {

		StringBuilder sb = new StringBuilder("<");
		sb.append(tag);
		sb.append(" ");

		for (Entry<String, String> entry : params.entrySet()) {
			sb.append(entry.getKey());
			sb.append("=\"");
			sb.append(entry.getValue());
			sb.append("\" ");
		}

		if (content == null) {

			sb.append("/>");

		} else {

			sb.append(">");
			sb.append(content);

			sb.append("</");
			sb.append(tag);
			sb.append(">");
		}

		return sb;
	}

	protected String escape(String string) {
		return string;
	}

	/**
	 * Returns the parameter value using the name of this tag as the key.
	 * @return
	 */
	protected final String getParam() {
		return context == null ? null : context.getParameter(name);
	}

	protected final Map<String, String> getParameters() {
		Map<String, String> params = new HashMap<String, String>();
		populate(params);
		return params;
	}

	protected final List<String> getParams() {
		return context == null ? null : context.getParamValues(name);
	}

	//	protected String getValue() {
	//
	//		if (context == null)
	//			return value;
	//
	//		String param = context.getParameter(name);
	//		if (param == null)
	//			return value;
	//		else
	//			return param;
	//	}

	protected final String getValue() {
		return value;
	}

	/**
	 * @return
	 */
	protected boolean isChecked() {
		return checked;
	}

	public final boolean isMultiple() {
		return multiple;
	}

	/**
	 * @param map
	 */
	protected final void populate(Map<String, String> map) {

		if (id != null) {
			map.put("id", id);
		}

		if (value != null) {
			map.put("value", value);
		}

		if (style != null) {
			map.put("style", style);
		}

		if (rows > 0) {
			map.put("rows", String.valueOf(rows));
		}

		if (cols > 0) {
			map.put("cols", String.valueOf(cols));
		}

		if (width > 0) {
			map.put("width", String.valueOf(width));
		}

		if (height > 0) {
			map.put("height", String.valueOf(height));
		}

		if (rel != null) {
			map.put("rel", rel);
		}

		if (title != null) {
			map.put("title", title);
		}

		if (name != null) {
			map.put("name", name);
		}

		if (clazz != null) {
			map.put("class", clazz);
		}

		if (alt != null) {
			map.put("alt", alt);
		}

		if (checked) {
			map.put("checked", String.valueOf(checked));
		}

		if (border > -1) {
			map.put("border", String.valueOf(border));
		}

		if (target != null) {
			map.put("target", target);
		}

		if (src != null) {
			map.put("src", src);
		}

		if (size > 0) {
			map.put("size", String.valueOf(size));
		}

		if (align != null) {
			map.put("align", align);
		}

		if (readOnly) {
			map.put("readonly", "true");
		}

		if (onMouseOver != null) {
			map.put("onmouseover", onMouseOver);
		}

		if (onMouseOut != null) {
			map.put("onmouseout", onMouseOut);
		}

		if (onChange != null) {
			map.put("onchange", onChange);
		}

		if (onClick != null) {
			map.put("onclick", onClick);
		}

		if (onSubmit != null) {
			map.put("onSubmit", onSubmit);
		}

		if (disabled) {
			map.put("disabled", "true");
		}

		if (onFocus != null) {
			map.put("onfocus", onFocus);
		}

		if (onKeyUp != null) {
			map.put("onkeyup", onKeyUp);
		}

		if (onKeyDown != null) {
			map.put("onkeydown", onKeyDown);
		}

		if (legend != null) {
			map.put("legend", legend);
		}
	}

	public final void removeValue() {
		value = null;
	}

	public final AbstractTag setAlign(String align) {
		this.align = align;
		return this;
	}

	public final AbstractTag setAlt(String alt) {
		this.alt = (alt == null ? null : alt.replace("\"", ""));
		return this;
	}

	public AbstractTag setAutoSubmit(boolean autoSubmit) {

		if (autoSubmit) {
			setOnChange("this.form.submit()");
		}

		return this;
	}

	public final AbstractTag setBorder(int border) {
		this.border = border;
		return this;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public AbstractTag setClass(String clazz) {
		this.clazz = clazz;
		return this;
	}

	public final AbstractTag setCols(int cols) {
		this.cols = cols;
		return this;
	}

	public final AbstractTag setContent(Object obj) {
		this.content = EntityObject.getEqualityString(obj);
		return this;
	}

	public final AbstractTag setContent(String content) {
		this.content = content;
		return this;
	}

	public final AbstractTag setDisabled(boolean disabled) {
		this.disabled = disabled;
		return this;
	}

	public AbstractTag setHeight(int height) {

		this.height = height;
		return this;
	}

	public AbstractTag setId(String id) {
		this.id = id;
		return this;
	}

	public void setIgnoreParamValue(boolean b) {
		this.ignoreParamValue = b;
	}

	public final AbstractTag setInitialContent(String string) {
		this.onFocus = "if (this.value=='" + string + "') this.value = '';";
		this.value = string;
		return this;
	}

	public final void setMultiple(boolean multiple) {
		this.multiple = multiple;
	}

	public final AbstractTag setName(String name) {
		this.name = name;
		return this;
	}

	public AbstractTag setOnChange(String onChange) {
		this.onChange = onChange;
		return this;
	}

	public AbstractTag setOnClick(String onClick) {
		this.onClick = onClick;
		return this;
	}

	public void setOnFocus(String onFocus) {
		this.onFocus = onFocus;
	}

	public AbstractTag setOnKeyDown(String onKeyDown) {
		this.onKeyDown = onKeyDown;
		return this;
	}

	public AbstractTag setOnKeyUp(String onKeyUp) {
		this.onKeyUp = onKeyUp;
		return this;
	}

	public AbstractTag setOnMouseOut(String onMouseOut) {
		this.onMouseOut = onMouseOut;
		return this;
	}

	public AbstractTag setOnMouseOver(String onMouseOver) {
		this.onMouseOver = onMouseOver;
		return this;
	}

	public AbstractTag setOnSubmit(String onSubmit) {
		this.onSubmit = onSubmit;
		return this;
	}

	public AbstractTag setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
		return this;
	}

	public void setRel(String rel) {
		this.rel = rel;
	}

	public final AbstractTag setRows(int rows) {
		this.rows = rows;
		return this;
	}

	public final AbstractTag setSize(int size) {
		this.size = size;
		return this;
	}

	public final AbstractTag setSrc(String src) {
		this.src = src;
		return this;
	}

	public AbstractTag setStyle(String style) {
		this.style = style;
		return this;
	}

	public AbstractTag setTarget(String target) {
		this.target = target;
		return this;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public AbstractTag setValue(Object obj) {

		value = EntityObject.getEqualityString(obj);
		if (value != null) {
			value = escape(value);
		}

		return this;
	}

	public AbstractTag setWidth(int width) {
		this.width = width;
		return this;
	}

	
	public final int getWidth() {
		return width;
	}

	
	public final int getHeight() {
		return height;
	}

    
    public String getName() {
        return name;
    }

    public String getAlt() {
        return alt;
    }
}
