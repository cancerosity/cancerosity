package org.sevensoft.jeezy.http.html;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;


/**
 * @author sam2 Nov 20, 2003 7:26:51 PM
 */
public class TagUtil {

	public static String generateSelectTag(Map attributes, Map options, String selectedOption) {

		StringBuilder buffer = new StringBuilder("<select");

		if (attributes != null) {
			Iterator iter = attributes.entrySet().iterator();
			while (iter.hasNext()) {

				Map.Entry entry = (Entry) iter.next();

				buffer.append(' ');
				buffer.append(entry.getKey());
				buffer.append("=\"");
				buffer.append(entry.getValue());
				buffer.append('"');
			}
		}

		buffer.append(">\n");

		if (options != null) {
			Iterator iter = options.entrySet().iterator();
			while (iter.hasNext()) {

				Map.Entry entry = (Entry) iter.next();
				String value = entry.getKey() == null ? "" : entry.getKey().toString();
				String label = entry.getValue() == null ? "" : entry.getValue().toString();

				buffer.append("<option value=\"" + value + "\"");

				if (value.equals(selectedOption))
					buffer.append(" selected=\"selected\"");

				buffer.append('>');
				buffer.append(label);
				buffer.append("</option>\n");
			}
		}

		buffer.append("</select>\n");

		return buffer.toString();
	}

	public static String generateSelectTag(Map<String, String> attributes, Collection<? extends Enum> collection, Enum selected) {

		StringBuilder sb = new StringBuilder("<select");

		for (Map.Entry<String, String> entry : attributes.entrySet()) {

			sb.append(' ');
			sb.append(entry.getKey());
			sb.append("=\"");
			sb.append(entry.getValue());
			sb.append('"');
		}

		sb.append(">\n");

		for (Enum e : collection) {

			sb.append("<option value=\"" + e.name() + "\"");
			if (selected == e)
				sb.append(" selected=\"selected\"");

			sb.append('>');
			sb.append(e.toString());
			sb.append("</option>\n");
		}

		sb.append("</select>\n");

		return sb.toString();
	}

	public static String generateTag(String name, Map attributes) {
		return generateTag(name, attributes, null);
	}

	public static String generateTag(String name, Map attributes, String body) {

		StringBuilder sb = new StringBuilder("<");
		sb.append(name);

		if (attributes != null) {
			Iterator iter = attributes.entrySet().iterator();
			while (iter.hasNext()) {

				Map.Entry entry = (Entry) iter.next();

				sb.append(' ');
				sb.append(entry.getKey());
				sb.append("=\"");
				sb.append(entry.getValue());
				sb.append('"');
			}
		}

		if (body == null)
			sb.append("/>\n");

		else {
			sb.append(">");
			sb.append(body);
			sb.append("</" + name + ">\n");
		}

		return sb.toString();
	}

}