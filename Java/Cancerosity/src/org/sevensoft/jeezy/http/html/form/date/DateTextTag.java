package org.sevensoft.jeezy.http.html.form.date;

import java.util.logging.Logger;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 26 Feb 2007 19:57:38
 *
 */
public class DateTextTag {

	private static Logger		logger	= Logger.getLogger("tags");

	private final RequestContext	context;
	private final String		name;
	private String			value;

	public DateTextTag(RequestContext context, String name) {
		this.context = context;
		this.name = name;
	}

	public DateTextTag(RequestContext context, String name, Date value) {
		this.context = context;
		this.name = name;
		if (value != null) {
			this.value = value.toString("dd/MM/yyyy");
		}
	}

	public DateTextTag(RequestContext context, String name, String value) {
		this.context = context;
		this.name = name;
		this.value = value;
	}

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    @Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		sb.append("<input size='12' type=\"text\" name=\"" + name + "\"");
		if (value != null) {
			sb.append(" value='" + value + "'");
		}
		sb.append("/>");

		return sb.toString();
	}
}
