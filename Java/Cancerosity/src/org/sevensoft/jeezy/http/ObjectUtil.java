package org.sevensoft.jeezy.http;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;

/**
 * @author sks 27-Mar-2006 23:38:48
 *
 */
public class ObjectUtil {

	/**
	 * Returns true if these objects are equal or both null.
	 * 
	 */
	public static boolean equal(Object obj1, Object obj2) {

		if (obj1 == null && obj2 == null) {
			return true;
		}

		if (obj1 == null || obj2 == null) {
			return false;
		}

		return obj1.equals(obj2);
	}

	public static String fieldsToString(Object obj) {

		StringBuilder sb = new StringBuilder(obj.getClass().getSimpleName() + ": ");

		Field[] fields = obj.getClass().getDeclaredFields();
		AccessibleObject.setAccessible(fields, true);

		for (Field field : fields) {

			if (field.getName().equals("context"))
				continue;

			if (field.isSynthetic())
				continue;

			try {

				sb.append(field.getName() + "=" + field.get(obj) + ",");
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}

		return sb.toString();
	}

}
