package org.sevensoft.jeezy.http;

import org.sevensoft.commons.smail.Email;
import org.sevensoft.commons.smail.EmailAddressException;
import org.sevensoft.commons.smail.SmtpServerException;
import org.sevensoft.commons.smail.EmailDecorator;
import org.sevensoft.commons.errors.ErrorAwareRequestWrapper;
import org.sevensoft.jeezy.db.Query;
import org.sevensoft.jeezy.http.results.*;
import org.sevensoft.jeezy.http.results.docs.Doc;
import org.sevensoft.ecreator.model.system.config.Config;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.beans.Introspector;
import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Logger;

/**
 * @author Sam 17-Jun-2003 12:29:52
 */
public abstract class HandlerServlet extends DatabaseServlet {

    private static final String Password = "mobile";

    private Map<String, String> paths;
    private boolean initialised;

    @Override
    public void destroy() {

        super.destroy();

        logger.config("Shutting down web application: " + this);

        servletContext = null;
        paths = null;
        schemaValidator = null;
        initialised = false;

        Introspector.flushCaches();

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    protected abstract void doHandlers(Map<String, String> paths);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Logger.getLogger("queries").finest("New request");
        logger.fine("[HandlerServlet] New request ds=" + ds + ", schemaval=" + schemaValidator);

        long timestamp = System.currentTimeMillis();

        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");

        final RequestContext requestContext = new RequestContext(request, response, servletContext, ds, schemaValidator);

        if ("1".equals(requestContext.getParameter("write_handlers"))) {

            writeHandlers(response);
            return;

        } else if ("1".equals(requestContext.getParameter("schema_reset")) && Password.equals(request.getParameter("p"))) {

            schemaValidator.clear();
            response.setContentType("text/html");
            response.getWriter().print("Schema validator reset");
            return;

        }

        if (!initialised || requestContext.isLocalhost()) {
            //		if (!initialised) {
            logger.fine("[HandlerServlet] calling setup");
            setup(requestContext);
            initialised = true;
        }

        logger.fine("[HandlerServlet] servletPath=" + request.getServletPath());

        try {

            Handler handler = getHandler(request.getServletPath(), requestContext);
            if (handler == null) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                return;
            }

            Config config = Config.getInstance(requestContext);
            if (config.isWwwRedirection() && request.getRequestURL().indexOf("www") == -1 && !requestContext.isLocalhost()) {

                logger.fine("[HandlerServlet] WWW page required");

                String url = request.getRequestURL().toString();
                url = url.replace("://", "://www.");
                String query = request.getQueryString();
                if (query != null) {
                    url = url + "?" + query;
                }

                logger.fine("[HandlerServlet] forwarding WWW url: " + url);

                response.sendRedirect(url);
                return;

            }

            // if non SSL and we want SSL then redirect to https on 443
            if (handler.runSecure() && !request.isSecure()) {

                logger.fine("[HandlerServlet] secure page required, forwarding to https (443)");

                String url = request.getRequestURL().toString();
                url = url.replace("http://", "https://");
                String query = request.getQueryString();
                if (query != null) {
                    url = url + "?" + query;
                }

                logger.fine("[HandlerServlet] forwarding url: " + url);

                response.sendRedirect(url);
                return;

            }

            // if SSL and we want non SSL then redirect to http on 80
            if (handler.runUnsecure() && request.isSecure()) {

                logger.fine("[HandlerServlet] unsecure page required, forwarding to http (80)");

                String url = request.getRequestURL().toString();
                url = url.replace("https://", "http://");
                String query = request.getQueryString();
                if (query != null) {
                    url = url + "?" + query;
                }

                logger.fine("[HandlerServlet] forwarding url: " + url);

                response.sendRedirect(url);
                return;

            }

            logger.fine("[HandlerServlet] populating handler");
            handler.populate();
            logger.fine("[HandlerServlet] handler populated, fields=" + ObjectUtil.fieldsToString(handler));

            logger.fine("[HandlerServlet] calling init");
            Object result = handler.init();

            if (result == null) {

                String action = requestContext.getParameter("_a");

                if (action == null) {
                    result = handler.main();
                } else {
                    result = handler.getClass().getMethod(action).invoke(handler);
                }
            }

            processResult(request, response, result, handler);

        } catch (final RuntimeException e) {
            e.printStackTrace();
            throw new ServletException(e);

        } catch (InstantiationException e) {
            throw new ServletException(e);

        } catch (InvocationTargetException e) {
            throw new ServletException(e);

        } catch (NoSuchMethodException e) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);

        } catch (ClassNotFoundException e) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);

        } catch (IllegalAccessException e) {
            throw new ServletException(e);

        } catch (NoSuchFieldException e) {
            throw new ServletException(e);

        } catch (ServletException e) {

            try {

                Email msg = new Email();
                StringBuilder sb = new StringBuilder();
                sb.append(request.getRequestURL());
                sb.append("\n\n");
                sb.append(e.toString());

                msg.setTo("sks@7soft.co.uk");
                msg.addCc("trudenka@gmail.com");
                msg.setFrom(Config.getInstance(requestContext).getServerEmail());
                msg.setSubject("Servlet exception");
                msg.setBody(sb);

                new EmailDecorator(requestContext, msg).send(Config.getInstance(requestContext).getSmtpHostname());

            } catch (EmailAddressException e1) {
                e1.printStackTrace();
            } catch (SmtpServerException e1) {
                e1.printStackTrace();
            }

            throw e;

        } finally {

            requestContext.end();
        }

        String time = "Request took: " + (System.currentTimeMillis() - timestamp) + "ms";
        logger.fine(time);
    }

    protected void emptyDatabase(RequestContext context) throws ServletException {

        Query q = new Query(context, "show tables");
        for (String string : q.getStrings())
            new Query(context, "drop table `" + string + "`").run();

        schemaValidator.clear();
        logger.finest("Schema reset");
    }

    protected List<String> getClassNames() {

        List<String> classNames = new ArrayList<String>();

        classNames.addAll(getClassNamesFromDirectory(new File(servletContext.getRealPath("WEB-INF/classes/"))));
        classNames.addAll(getClassNamesFromJars(new File(servletContext.getRealPath("WEB-INF/lib/"))));

        return classNames;
    }

    private List<String> getClassNamesFromDirectory(File file) {

        List<String> list = new ArrayList();
        if (file.exists())
            getClassNamesFromDirectory(file, file.getAbsolutePath().length(), list);

        return list;
    }

    private List<String> getClassNamesFromDirectory(File file, int i, List<String> list) {

        if (file.isDirectory())
            for (File f : file.listFiles())
                getClassNamesFromDirectory(f, i, list);
        else
            list.add(file.getPath().substring(i).replaceAll("(\\\\|/)", "."));

        return list;
    }

    private List<String> getClassNamesFromJars(File libDirectory) {

        List<String> list = new ArrayList();

        if (libDirectory.exists() && libDirectory.isDirectory()) {

            // list jars
            for (File jarFile : libDirectory.listFiles(new FilenameFilter() {

                public boolean accept(File dir, String name) {
                    return name.toLowerCase().endsWith(".jar");
                }
            })) {

                if (jarFile.isFile())
                    try {

                        JarFile jar = new JarFile(jarFile);
                        Enumeration<JarEntry> entries = jar.entries();
                        while (entries.hasMoreElements()) {

                            JarEntry e = entries.nextElement();
                            String name = e.getName().replaceAll("/", ".");
                            list.add(name);
                        }

                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
            }
        }

        return list;
    }

    private Handler getHandler(String path, RequestContext handlerContext) throws ServletException, IllegalArgumentException, SecurityException,
            InstantiationException, InvocationTargetException, NoSuchMethodException, ClassNotFoundException {

        if (path == null) {
            return null;
        }

        if (path.startsWith("/")) {
            path = path.substring(1);
        }

        try {

            String className = paths.get(path.toLowerCase());
            if (className == null) {
                return null;
            }

            Class handlerClass = Class.forName(className);
            Constructor constructor = handlerClass.getConstructor(RequestContext.class);

            return (Handler) constructor.newInstance(handlerContext);

        } catch (NoSuchMethodException e) {
            return null;

        } catch (ClassNotFoundException e) {
            return null;

        } catch (IllegalAccessException e) {
            throw new RuntimeException("Illegal access to handler constructor");
        }
    }

    public synchronized void init(ServletConfig config) throws ServletException {
        super.init(config);

        this.paths = new HashMap();
        doHandlers(paths);
    }

    private void processResult(HttpServletRequest request, HttpServletResponse response, Object obj, Handler handler) throws ServletException, IOException {

        if (obj instanceof Reader) {

            response.setContentType("text/plain");

            BufferedReader reader = null;

            try {

                reader = new BufferedReader((Reader) obj);

                String string = null;
                while ((string = reader.readLine()) != null) {
                    response.getWriter().print(string);
                }

            } catch (IOException e) {
                e.printStackTrace();

            } finally {

                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

        } else if (obj instanceof Doc) {

            try {

                StringBuilder sb = ((Doc) obj).output();

                response.setContentType("text/html");
                response.getWriter().print(sb);

                return;

            } catch (InstantiationException e) {
                throw new ServletException(e);

            } catch (IllegalAccessException e) {
                throw new ServletException(e);

            } catch (ClassNotFoundException e) {
                throw new ServletException(e);

            }

        } else if (obj instanceof RequestForward) {

            request.getRequestDispatcher(obj.toString()).forward(request, response);

        } else if (obj instanceof String) {

            response.setContentType("text/plain");
            response.getWriter().print(obj.toString());

        } else if (obj instanceof StringResult) {

            StringResult result = (StringResult) obj;
            response.setContentType(result.getContentType());
            response.getWriter().print(result.getContent());

        } else if (obj instanceof StreamResult) {

            StreamResult result = (StreamResult) obj;
            BufferedInputStream bis = null;
            ServletOutputStream bos = null;

            try {

                bis = new BufferedInputStream(result.getStream());
                bos = response.getOutputStream();

                if (result.getType() == StreamResult.Type.Attachment) {

                    response.setContentType("application/octet-stream");
                    response.setHeader("Content-Disposition", "attachment; filename=\"" + result.getFilename() + "\"");
                    response.setHeader("Cache-Control", "private");

                } else {

                    response.setHeader("Content-Disposition", "inline; filename=\"" + result.getFilename() + "\"");
                    response.setContentType(result.getContentType());
                }

                byte[] buf = new byte[32 * 1024];
                int length = 0;
                int count = 0;
                while ((count = bis.read(buf)) >= 0) {
                    bos.write(buf, 0, count);
                    length = length + count;
                }

                bos.flush();
                response.setContentLength(length);

                bos.close();

            } catch (IOException e) {
                throw e;

            } finally {

                try {
                    if (bis != null) {
                        bis.close();
                    }
                } catch (IOException e) {
                    throw e;
                }

                try {
                    if (bos != null) {
                        bos.close();
                    }
                } catch (IOException e) {
                    throw e;
                }

                result.close();
            }

        } else if (obj instanceof HttpCode) {
            HttpCode h = (HttpCode) obj;
            response.sendError(h.getResponseCode(), h.getMessage());

        } else if (obj instanceof ExternalRedirect) {
            response.sendRedirect(obj.toString());

        } else if (obj instanceof Integer) {

            int code = (Integer) obj;
            if (code == HttpServletResponse.SC_OK) {
                return;
            } else {
                new ErrorAwareRequestWrapper(response).sendError(code);
            }

        } else if (obj instanceof XmlResult) {

            XmlResult result = (XmlResult) obj;

            if (result.getMethod() == XmlResult.Attachment) {

                response.setContentType("application/octet-stream");
                response.setHeader("Content-Disposition", "attachment; filename=\"" + result.getFilename() + "\"");
                response.setHeader("Cache-Control", "private");

            } else {

                response.setHeader("Content-Disposition", "inline; filename=\"" + result.getFilename() + "\"");
                response.setContentType("application/xml");
                response.setHeader("Cache-Control", "no-cache");
            }

            response.getWriter().print(obj);

        } else {
            throw new ServletException("no compatible output");
        }
    }

    protected abstract void setup(RequestContext context);

    private void writeHandlers(HttpServletResponse response) throws ServletException, IOException {

        try {

            Map<String, String> params = new TreeMap<String, String>();

            for (String name : getClassNames()) {

                if (name.startsWith(".")) {
                    name = name.substring(1);
                }

                if (name.contains("$")) {
                    continue;
                }

                if (!name.endsWith(".class")) {
                    continue;
                }

                Class clazz = Class.forName(name.replace(".class", ""));
                if (Modifier.isAbstract(clazz.getModifiers())) {
                    continue;
                }

                if (!Handler.class.isAssignableFrom(clazz)) {
                    continue;
                }

                Path path = (Path) clazz.getAnnotation(Path.class);

                if (path != null) {

                    String[] values = path.value();
                    if (values != null) {

                        for (String value : values) {
                            String existingClassName = params.put(value.trim().toLowerCase(), clazz.getName());
                            if (existingClassName != null) {
                                throw new ServletException("Duplicate path " + existingClassName + " and " + clazz.getName());
                            }
                        }
                    }
                }
            }

            response.getWriter().println("package org.sevensoft.ecreator.iface;\n");
            response.getWriter().println("import java.util.Map;\n");
            response.getWriter().println("public class Paths {\n");
            response.getWriter().println("\tpublic static void setPaths(Map<String, String> paths) {\n");

            for (Map.Entry<String, String> entry : params.entrySet()) {

                response.getWriter().println("\t\tpaths.put(\"" + entry.getKey() + "\", \"" + entry.getValue() + "\");");

            }

            response.getWriter().println("}}");

        } catch (ClassNotFoundException e) {
            throw new ServletException(e);
        }

    }
}
