package org.sevensoft.jeezy.http;

import org.sevensoft.ecreator.util.log.MDC;

import javax.servlet.ServletException;
import javax.servlet.ServletConfig;

/**
 * @author RoshkovskayaTI
 * @version: $id$
 */
public class MyLoggerServlet extends DatabaseServlet {

    private static final String QUARTZ_CONTEXT_KEY = "org.quartz.impl.StdSchedulerFactory.KEY";

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);

        MDC.domain = config.getInitParameter("webapp");
        if (config.getServletContext().getAttribute(QUARTZ_CONTEXT_KEY) != null) {
            logger.info("Quartz Started!!!");
        } else {
            logger.info(" Quartz Failed!!!");
        }
    }
}
