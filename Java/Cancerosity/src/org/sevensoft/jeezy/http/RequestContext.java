package org.sevensoft.jeezy.http;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadBase;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.sevensoft.commons.collections.maps.MultiValueMap;
import org.sevensoft.commons.simpleio.SimpleFile;
import org.sevensoft.commons.superstrings.RandomHelper;
import org.sevensoft.commons.superstrings.StringHelper;
import org.sevensoft.commons.validators.RequiredValidator;
import org.sevensoft.commons.validators.Validator;
import org.sevensoft.ecreator.model.system.config.ResourcesUtils;
import org.sevensoft.ecreator.model.system.users.User;
import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.db.SchemaValidator;

import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Logger;

/**
 * @author sks 18-May-2005 23:32:10
 */
public class RequestContext {

    private static RequestContext ourInstance;

    public static RequestContext getInstance() {
        return ourInstance;
    }

	public enum MessageType {
		Warning, Info, Popup, Error
	}

	private static final Logger					logger	= Logger.getLogger("jeezy");

	/**
	 * Use this count to generate unique incremental values
	 */
	private int								count		= 0;
	private final ServletContext					servletContext;
	private final Map<String, String>				errors;
	private final MultiValueMap<MessageType, String>	messages;
	private final HttpServletRequest				request;
	private final HttpServletResponse				response;
	private final Map<String, EntityObject>			entityObjectCache;
	private String							sessionId;
	private final SchemaValidator					schemaValidator;
	private MultiValueMap<String, String>			parameters;

	/**
	 * A list of the files we have created for uploaded files
	 */
	private List<File>						temporaryUploadedFiles;

	private boolean							cacheDisabled;

	private final DataSource					ds;

	public RequestContext(HttpServletRequest request, HttpServletResponse response, ServletContext context, DataSource ds, SchemaValidator schemaValidator) {

		logger.finest("[request context] creating new request context");

		this.request = request;
		this.response = response;
		this.servletContext = context;
		this.schemaValidator = schemaValidator;
		this.ds = ds;

		this.entityObjectCache = new HashMap();

		this.errors = new HashMap();
		this.messages = new MultiValueMap(new HashMap());

		request.setAttribute("__errors", errors);
		request.setAttribute("__messages", messages);

		initParameters();
		initSession();

        ourInstance = this;
	}

	final void addError(Exception e) {
		messages.put(MessageType.Error, e.getMessage());
	}

	public final void addError(String message) {
		messages.put(MessageType.Error, message);
	}

	public void addHeader(String key, String value) {
		response.setHeader(key, value);
	}

	final void addMessage(String message) {
		messages.put(MessageType.Info, message);
	}

	public void addParameter(String name, boolean value) {
		addParameter(name, String.valueOf(value));
	}

	public void addParameter(String name, double value) {
		addParameter(name, String.valueOf(value));
	}

	public void addParameter(String name, float value) {
		addParameter(name, String.valueOf(value));
	}

	public void addParameter(String name, int value) {
		addParameter(name, String.valueOf(value));
	}

	public void addParameter(String name, long value) {
		addParameter(name, String.valueOf(value));
	}

	public void addParameter(String name, String value) {
		parameters.put(name, value);
	}

	public void addParameters(String name, List<String> values) {
		parameters.putAll(name, values);
	}

	public void addParameters(String name, String[] values) {
		parameters.putAll(name, values);
	}

	final void addPopup(String message) {
		messages.put(MessageType.Popup, message);
	}

	final void addWarning(String message) {
		messages.put(MessageType.Warning, message);
	}

	/**
     *
     */
	public void clearCache() {
		entityObjectCache.clear();
	}

	final void clearErrors() {
		errors.clear();
	}

	final void clearMessages() {
		messages.clear();
	}

	public void clearParameters() {
		parameters.clear();
	}

	public boolean containsAttribute(String key) {
		return getAttribute(key) != null;
	}

	public boolean containsServletAttribute(String string) {
		return servletContext.getAttribute(string) != null;
	}

	public File createPublicTempFile(String prefix, String ext) throws IOException {

		if (ext == null) {
			ext = "tmp";
		}

		for (int n = 0; n < 10000; n++) {
			File file = ResourcesUtils.getRealTmp(prefix + "_" + n + "." + ext);
			if (!file.exists()) {
				file.createNewFile();
				file.deleteOnExit();
				return file;
			}
		}

		return null;
	}

	public void disableCache() {
		cacheDisabled = true;
		clearCache();
	}

	void end() {

		if (temporaryUploadedFiles != null) {

			for (File file : temporaryUploadedFiles) {
				file.delete();
			}
		}
	}

	public String[] getAcceptedEncodings() {
		String types = getHeader("Accept-Encoding");
		if (types != null)
			return types.split(",");
		else
			return null;
	}

	public String[] getAcceptedMimeTypes() {

		String types = getHeader("Accept");
		if (types != null)
			return types.split(",");
		else
			return null;
	}

	public final Object getAttribute(String key) {
		return request.getAttribute(key);
	}

	public Connection getConnection() throws SQLException {

		try {
			return ds.getConnection();

		} catch (SQLException e) {

			e.printStackTrace();
			throw new SQLException("Error with connection pool: " + e);
		}
	}

	public String getConnectionType() {
		return getHeader("Connection");
	}

	public String getContextPath() {
		return request.getContextPath();
	}

	public Cookie getCookie(String name) {

		Cookie[] cookies = request.getCookies();
		if (cookies != null)
			for (Cookie cookie : cookies)
				if (cookie.getName().equals(name))
					return cookie;

		return null;
	}

	public String getCookieValue(String name) {
		Cookie cookie = getCookie(name);
		return cookie == null ? null : cookie.getValue();
	}

	public int getCount() {
		return count++;
	}

	public EntityObject getEntityObject(String key, boolean cacheOverride) {

		if (cacheDisabled && cacheOverride == false) {
			return null;
		}

		EntityObject obj = entityObjectCache.get(key);
		return obj;
	}

	public String getError(String key) {
		return errors.get(key);
	}

	/**
     *
     */
	public Map<String, String> getErrors() {
		return errors;
	}

	/**
	 * Returns the full url string as a get request
	 * <p/>
	 * Domain / Servlet / Path / Query
	 * <p/>
	 * The query will only include GET parameters not POST
	 */
	public final String getGetRequest() {

		StringBuffer sb = request.getRequestURL();
		String queryString = request.getQueryString();
		if (queryString != null) {
			sb.append("?");
			sb.append(queryString);
		}
		return sb.toString();
	}

	public String getHeader(String name) {
		return request.getHeader(name);
	}

	public String getKeepAliveTime() {
		return getHeader("Keep-Alive");
	}

	public Map<MessageType, String> getMessages() {
		return messages;
	}

	public String getParameter(String arg0) {
		return parameters.get(arg0);
	}

	public Enumeration getParameterNames() {
		return Collections.enumeration(parameters.keySet());
	}

	public MultiValueMap<String, String> getParameters() {
		return parameters;
	}

	public String[] getParameterValues(String arg0) {
		List<String> values = parameters.getAll(arg0);
		return values == null ? null : values.toArray(new String[values.size()]);
	}

	public Set<String> getParamNames() {
		return parameters.keySet();
	}

	public List<String> getParamValues(String key) {
		return parameters.getAll(key);
	}

	public String getQueryString() {
		return request.getQueryString();
	}

	public File getRealFile(String string) {
		return new File(getRealPath(string));
	}

	public final String getRealPath(String file) {
		return servletContext.getRealPath(file);
	}

	public String getReferrer() {
		return getHeader("referer");
	}

	final String getRemoteHostname() {
		return request.getRemoteAddr();
	}

	public final String getRemoteIp() {
		return request.getRemoteHost();
	}

	public HttpServletRequest getRequest() {
		return request;
	}

	/**
	 * Returns the the url after the domain name, eg /e7/admin-items-types.do
	 * <p/>
	 * Servlet / Path
	 */
	public final String getRequestURI() {
		return request.getRequestURI();
	}

	/**
	 * Returns http://127.0.0.1/e7/admin-items-types.do
	 * <p/>
	 * Returns domain / servlet / path
	 */
	public final String getRequestURL() {
		return request.getRequestURL().toString();
	}

	public HttpServletResponse getResponse() {
		return response;
	}

	public String getRoot() {
		return getRealPath(".");
	}

	public SchemaValidator getSchemaValidator() {
		return schemaValidator;
	}

	final String getServerHostname() {
		return request.getServerName();
	}

	public final String getServerIp() {
		return request.getLocalAddr();
	}

	public Object getServletAttribute(String key) {
		return servletContext.getAttribute(key);
	}

	/**
     * Returns the path of the request, eg category.do
     * Path
	 */
	public String getServletPath() {
		return request.getServletPath();
	}

	public Object getSessionAttribute(String key) {
		return request.getSession().getAttribute(key);
	}

	public String getSessionId() {
		return sessionId;
	}

	public String getUserAgent() {
		return getHeader("User-Agent");
	}

	public final boolean hasErrors() {
		return errors.size() > 0;
	}

	public boolean hasMessage() {
		return hasMessages();
	}

	public boolean hasMessages() {
		return messages.size() > 0;
	}

	public boolean hasParameter(String key) {
		return parameters.containsKey(key);
	}

    @SuppressWarnings("static-access")
	void initParameters() {

		logger.finest("[RequestContext] init params");

		// create a multimap for our parameters
		parameters = new MultiValueMap(new LinkedHashMap());

		List<FileItem> items = null;
		if (FileUploadBase.isMultipartContent(request)) {

			// Create a factory for disk-based file items
			DiskFileItemFactory factory = new DiskFileItemFactory();

			// Create a new file upload handler
			ServletFileUpload upload = new ServletFileUpload(factory);
			// upload.setHeaderEncoding("utf-8");

			// in bytes (100mb max upload)
			upload.setSizeMax(100000000);

			try {

				items = upload.parseRequest(request);
				if (items.isEmpty()) {
					items = null;
				}

			} catch (FileUploadException e) {
				e.printStackTrace();
			}

			logger.finest("[RequestContext] items from multi parser=" + items);
		}

		/*
           * If the items are empty, then either our request is empty, or resin has nabbled all the inputs first
           * Either way we will process them as normal if empty.
		 */
		if (items == null) {

			Map<String, String[]> map = request.getParameterMap();
			if (map != null)
				for (Map.Entry<String, String[]> entry : map.entrySet()) {

					String name = entry.getKey();
					String[] values = entry.getValue();

					if (values != null) {
						for (String value : values) {
							parameters.put(name, value);
						}
					}
				}

		} else {

			for (FileItem fileItem : items) {

				if (fileItem.isFormField()) {

					try {

						logger.finest("[request context] param value=" + fileItem.getString("utf-8") + ", name=" + fileItem.getFieldName());
						parameters.put(fileItem.getFieldName(), fileItem.getString("utf-8"));

					} catch (UnsupportedEncodingException e) {
						throw new RuntimeException(e);
					}

				} else {

					/*
					 * Using resin syntax here for file uploads.
					 * 
                          * param.filename = name of file on client machine (IE uploads full path - we should strip back to file only)
                          * param = path to file on server
                          * param.content-type = content type of uploaded file
					 * 
					 * and my own extra ones
					 * 
					 * param.size = size of file in bytes
					 */

					try {

						// file to write to, use same extension as the original file
						File file = File.createTempFile("jeezy_upload_", "." + SimpleFile.getExtension(fileItem.getName()));
						file.deleteOnExit();
						fileItem.write(file);

						if (temporaryUploadedFiles == null) {
							temporaryUploadedFiles = new ArrayList();
						}

						temporaryUploadedFiles.add(file);

						String fn = fileItem.getName();
						if (fn != null) {
							fn = fn.replaceAll(".*/", "");
							fn = fn.replaceAll(".*\\\\", "");
							fn = fn.replaceAll("\\s", "_");
						}

						parameters.put(fileItem.getFieldName() + ".filename", fn);
						parameters.put(fileItem.getFieldName() + ".content-type", fileItem.getContentType());
						parameters.put(fileItem.getFieldName() + ".size", String.valueOf(fileItem.getSize()));
						parameters.put(fileItem.getFieldName(), file.getAbsolutePath());

					} catch (Exception e) {
						e.printStackTrace();
					}

				}
			}

		}

		/*
		 * I want to cycle through all parameters, and any that begin with _ want to be concatenated and put back in another param that doesn't
		 * begin with _
		 * 
		 * eg, _date=10, _date=09, _date=1979 should be concat to date=10091979
           *
		 */
		{
			MultiValueMap<String, String> parametersToAdd = new MultiValueMap(new LinkedHashMap());
			for (String name : parameters.keySet()) {

				if (name.startsWith("_") && !name.equals("_a")) {

					List<String> values = parameters.list(name);
					if (values != null) {

						logger.finest("combining parameters=" + name + " values=" + values);

						// combine all values together and then split up on the pipe
						String[] newValues = StringHelper.implode(values, "", false).split("\\|");
						logger.finest("new parameter values=" + Arrays.toString(newValues) + " new name=" + name.substring(1));
						parametersToAdd.putAll(name.substring(1), newValues);
					}
				}
			}

			/*
			 * Add the parameters
			 */
			parameters.putAll(parametersToAdd);
		}

        //done for PayPal Pro integration to be able to add number of params to returnUrl
        {
            MultiValueMap<String, String> parametersToAdd = new MultiValueMap(new LinkedHashMap());
            if (parameters.containsKey("_a")  && parameters.get("_a").contains("$session")) {
                String[] params = parameters.get("_a").split("\\$");
                for (String param : params) {
                    if (param.split("=").length == 1) {
                        parameters.remove("_a");
                        parametersToAdd.put("_a", param);
                    } else if (param.split("=").length == 2) {
                        parametersToAdd.put(param.split("=")[0], param.split("=")[1]);
                    }
                }
            }

            parameters.putAll(parametersToAdd);
        }

		/*
		 * Combine parameters that begin with a number in the order they are entered here.
		 * 
		 * First of all build a multi value map with all the values put together minus their numbers
		 */
		{
			MultiValueMap<String, String> parametersToAdd = new MultiValueMap(new LinkedHashMap());

            List<String> list = new ArrayList<String>(parameters.keySet());
            Collections.sort(list);

//			for (String name : parameters.keySet()) {
            for (String name : list) {

				if (Character.isDigit(name.charAt(0))) {

					List<String> values = parameters.list(name);
					if (values != null) {

						logger.finest("[RequestContext] combining parameters=" + name + " values=" + values);

						while (Character.isDigit(name.charAt(0))) {
							name = name.substring(1);
						}

						String value = StringHelper.implode(values, "", false);
						parametersToAdd.put(name, value);
					}
				}
			}

			/*
                * Add the parameters
                * (Remember don't do this inline because you will get an iteration exception
			 */
			parameters.putAll(parametersToAdd);
		}

		logger.fine("[RequestContext] final parameters=" + parameters);
	}

	/*
      * Get session id from my own rolled cookie becauase I've been having
      * trouble with container sessions deleting themselves after so long
      * I prefer the cookie to be perm.
	 */
	private void initSession() {
        rewriteCookieToHeader();

		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("permsession")) {
                    sessionId = cookie.getValue();
                    return;
                }
            }
        }

        sessionId = RandomHelper.getRandomString(24);
        Cookie cookie = new Cookie("permsession", sessionId);
        cookie.setMaxAge(Integer.MAX_VALUE);

		// add cookie to response so it is sent back to the browser
		response.addCookie(cookie);
	}

    private void rewriteCookieToHeader() {
        if (response.containsHeader("SET-COOKIE")) {
            String sessionid = request.getSession().getId();
            String contextPath = request.getContextPath();
            String secure = "";
            if (request.isSecure()) {
                secure = "; Secure";
            }
            response.setHeader("SET-COOKIE", "JSESSIONID=" + sessionid + "; Path=" + contextPath + "; HttpOnly" + secure);
        }
    }

	public boolean isLocalhost() {

        boolean localhost = getRemoteIp().startsWith("127.0.0") || getRemoteIp().startsWith("192.168.")
                || getRemoteIp().startsWith("10.0.") || getRemoteIp().startsWith("212.98")
                || getRemoteIp().equals("81.134.18.187") || getRemoteIp().equals("81.149.134.48")
                || getRemoteIp().equals("81.6.244.229") || getRemoteIp().equals("91.84.190.45")
                || getRemoteIp().equals("213.152.38.29") || getRemoteIp().equals("82.133.95.232")
                || getRemoteIp().equals("81.149.26.62");
		logger.fine("[RequestContext] is localhost=" + localhost + ", ip=" + getRemoteIp());

        // check super ips from file based on server
		Set<String> supermenIps = (Set<String>) getServletAttribute(User.SuperIpsKey);
		if (supermenIps != null) {
			for (String ip : supermenIps) {
				if (getRemoteIp().startsWith(ip)) {
					return true;
				}
			}
		}
		return localhost;
	}

	final boolean isValidated() {
		return errors.size() == 0;
	}

	public void print(Object o) throws IOException {
		response.getWriter().print(o);
	}

	public final void putEntityObject(String key, EntityObject obj, boolean ignoreDisabledFlag) {

		if (cacheDisabled && ignoreDisabledFlag == false) {
			return;
		}

		entityObjectCache.put(key, obj);
	}

	public final void removeAttribute(String key) {
		request.removeAttribute(key);
	}

	public void removeCachedObj(EntityObject obj) {
		if (cacheDisabled)
			return;
		entityObjectCache.remove(EntityObject.getCacheKey(obj));
	}

	final void removeCookie(String key) {

		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals(key)) {
					cookie.setMaxAge(0);
					return;
				}
			}
		}
	}

	final public void removeParameter(String name) {
		parameters.remove(name);
	}

	public final void setAttribute(String key, Object obj) {
		request.setAttribute(key, obj);
	}

	final void setCookie(String key, long value) {
		setCookie(key, String.valueOf(value), false, Integer.MAX_VALUE);
	}

	public final void setCookie(String key, String value) {
		setCookie(key, value, false, Integer.MAX_VALUE);
	}

	/**
	 * Sets the maximum age of the cookie in seconds.
	 * <p/>
	 * A positive value indicates that the cookie will expire after that many seconds have passed. Note that the value is the maximum age when the
	 * cookie will expire, not the cookie's current age.
	 * <p/>
	 * A negative value means that the cookie is not stored persistently and will be deleted when the Web browser exits. A zero value causes the
	 * cookie to be deleted.
	 * 
	 * @param key
	 * @param value
	 * @param secure
	 * @param age
	 */
	public final void setCookie(String key, String value, boolean secure, int age) {

		Cookie cookie = new Cookie(key, value);
		cookie.setMaxAge(age);
		cookie.setSecure(secure);

		response.addCookie(cookie);
	}

	final void setError(String key, Exception e) {
		setError(key, e.getMessage());
	}

	public final void setError(String key, String error) {
		errors.put(key, error);
	}

	/**
	 * @param key
	 * @param value
	 */
	public void setSessionAttribute(String key, Object value) {
		request.getSession().setAttribute(key, value);
	}

	/**
	 * @param validator
	 * @param string
	 * @param obj
	 */
	public void test(RequiredValidator validator, String string, EntityObject obj) {
		test(validator, string + "_" + obj.getIdString());
	}

	/**
	 * @param validator
	 * @param name
	 */
	public void test(Validator validator, String name) {

		String value = getParameter(name);
		String error = validator.validate(value);

		if (error != null) {
			setError(name, error);
		}
	}

	public String toString() {

		StringBuilder sb = new StringBuilder();

		sb.append("---begin headers---\n\n");

		Enumeration e = request.getHeaderNames();
		while (e.hasMoreElements()) {

			String name = e.nextElement().toString();
			String value = getHeader(name);

			sb.append(name);
			sb.append("=");
			sb.append(value);

			sb.append("\n");
		}

		sb.append("\n---end  headers---\n");

		sb.append("\nauthType=");
		sb.append(request.getAuthType());

		sb.append("\ncontextPath=");
		sb.append(request.getContextPath());

		sb.append("\ncontentType=");
		sb.append(request.getContentType());

		sb.append("\nmethod=");
		sb.append(request.getMethod());

		sb.append("\npathInfo=");
		sb.append(request.getPathInfo());

		sb.append("\npathTranslated=");
		sb.append(request.getPathTranslated());

		sb.append("\nprotocol=");
		sb.append(request.getProtocol());

		sb.append("\nqueryString=");
		sb.append(request.getQueryString());

		sb.append("\nremoteHost=");
		sb.append(request.getRemoteHost());

		sb.append("\nrequestedSessionId=");
		sb.append(request.getRequestedSessionId());

		sb.append("\nrequestURI=");
		sb.append(request.getRequestURI());

		sb.append("\nrequestURL=");
		sb.append(request.getRequestURL());

		sb.append("\nscheme=");
		sb.append(request.getScheme());

		sb.append("\nserverName=");
		sb.append(request.getServerName());

		sb.append("\nserverPort=");
		sb.append(request.getServerPort());

		sb.append("\nservletPath=");
		sb.append(request.getServletPath());

		sb.append("\nparameters=");
		sb.append(parameters);

		return sb.toString();
	}

	/**
	 * @return
	 */
	public HttpSession getSession() {
		return request.getSession();
	}
}
