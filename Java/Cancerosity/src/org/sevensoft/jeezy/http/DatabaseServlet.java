package org.sevensoft.jeezy.http;


import org.sevensoft.jeezy.db.SchemaValidator;

import javax.mail.MessagingException;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.sql.DataSource;

import java.sql.SQLException;
import java.util.logging.Logger;

/**
 * @author sks 19 Dec 2006 15:00:33
 */
public class DatabaseServlet extends HttpServlet {

    protected final static Logger logger = Logger.getLogger("jeezy");
  
    protected DataSource ds;
    protected SchemaValidator schemaValidator;
    protected ServletContext servletContext;

    private void database(ServletConfig config) throws SQLException, NamingException, ServletException {

        InitialContext ic = new InitialContext();
        ds = (DataSource) ic.lookup("java:comp/env/jdbc/mysql");
        if (ds == null) {
            throw new ServletException("Datasource is null");
        }

        logger.fine("Datasource=" + ds);
        logger.config("Datasource=" + ds);

        schemaValidator = new SchemaValidator(ds);
        logger.config("Schema validator created: " + schemaValidator);
    }

    public void destroy() {
        super.destroy();
    }

    @Override
    public void init(ServletConfig config) throws ServletException {

        super.init(config);
        this.servletContext = config.getServletContext();

        try {
            database(config);
        } catch (SQLException e) {
            throw new ServletException(e);
        } catch (NamingException e) {
            throw new ServletException(e);
        }
    }
}
