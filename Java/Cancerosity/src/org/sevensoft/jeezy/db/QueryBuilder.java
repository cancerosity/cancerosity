package org.sevensoft.jeezy.db;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 10-Jan-2005 15:55:41
 */
public class QueryBuilder {

	private List<String>						clauses, orders, groups, selects;
	private boolean							debug;
	private List<Object>						params;
	private int								start, limit;
	private Map<String, Class<? extends EntityObject>>	tables;
	private ArrayList<String>					havings;
	private RequestContext						context;

	public QueryBuilder(RequestContext context) {

		this.context = context;

		selects = new ArrayList<String>();
		orders = new ArrayList<String>();
		groups = new ArrayList<String>();
		clauses = new ArrayList<String>();
		havings = new ArrayList<String>();
		params = new ArrayList<Object>();
		tables = new LinkedHashMap<String, Class<? extends EntityObject>>();
	}

	public void clause(String clause) {
		clause(clause, (Object[]) null);
	}

	public void clause(String clause, Collection<Object> col) {

		clauses.add(clause);
		if (col != null)
			params.addAll(col);
	}

	public void clause(String clause, Object... objs) {

		clauses.add(clause);
		if (objs != null)
			params.addAll(Arrays.asList(objs));
	}

	public void debug() {
		debug = true;
	}

	/**
	 * @return
	 */
	public List<Row> execute() {
		return toQuery().execute();
	}

	/**
	 * @param name
	 * @return
	 */
	public <E extends EntityObject> List<E> execute(Class<E> clazz) {
		return toQuery().execute(clazz);
	}

	/**
	 * @param clazz
	 * @param i
	 * @return
	 */
	public <E extends EntityObject> List<E> execute(Class<E> clazz, int i) {
		return toQuery().execute(clazz, i);
	}

	/**
	 * @param name
	 * @param start2
	 * @param limit2
	 * @return
	 */
	public <E extends EntityObject> List<E> execute(Class<E> clazz, int start, int limit) {
		return toQuery().execute(clazz, start, limit);
	}

	public void from(String alias, Class clazz) {
		tables.put(alias, clazz);
	}

	/**
	 * @param name
	 * @return
	 */
	public <E extends EntityObject> E get(Class<E> clazz) {
		return toQuery().get(clazz);
	}

	public <K extends Object, V extends Object> Map getMap(Class<?> c1, Class<?> c2, Map map) {
		return toQuery().getMap(c1, c2, map);
	}

	public void group(String string) {
		groups.add(string);
	}

	/**
	 * @param string
	 */
	public void having(String string, Object... objects) {
		havings.add(string);
		params.addAll(Arrays.asList(objects));
	}

	public void limit(int limit) {
		this.start = 0;
		this.limit = limit;
	}

	public void limit(int start, int limit) {
		this.start = start;
		this.limit = limit;
	}

	public void order(String string) {
		orders.add(string);
	}

	/**
	 * @param value
	 */
	public void parameter(Object obj) {
		params.add(obj);
	}

	public void select(String string) {
		selects.add(string);
	}

	/**
	 * Create aliases for all columns of this entity object class using the prefix parameter.
	 * 
	 * Eg,
	 * 
	 * prefix.id, prefix.name, prefix.height, etc
	 * 
	 */
	public void select(String prefix, Class<? extends EntityObject> clazz) {

		/*
		 * Create the column for the special id class
		 */
		select(prefix + ".id");

		for (Field field : EntityObject.getPersistableFields(clazz)) {
			select(prefix + "." + field.getName() + " as `" + prefix + "." + field.getName() + "`");
		}
	}

	public void setDebug(boolean b) {
		this.debug = b;
	}

	public Query toQuery() {

		Query q = new Query(context, toString());
		q.setDebug(debug);

		for (Map.Entry<String, Class<? extends EntityObject>> entry : tables.entrySet()) {
			q.setTable(entry.getValue());
		}

		for (Object obj : params)
			if (obj != null)
				q.setParameter(obj);

		return q;
	}

	public String toString() {

		StringBuilder s = new StringBuilder();

		s.append("SELECT ");
		if (selects.size() > 0) {
			for (String select : selects) {
				s.append(select);
				s.append(", ");
			}
			s.delete(s.length() - 2, s.length());
		}

		s.append(" FROM ");
		for (Map.Entry<String, Class<? extends EntityObject>> entry : tables.entrySet()) {
			s.append(entry.getKey());
			s.append(" ");
		}
		s.delete(s.length() - 1, s.length());

		if (clauses.size() > 0) {
			s.append(" WHERE ");
			Iterator<String> iter = clauses.iterator();
			while (iter.hasNext()) {

				String clause = iter.next();
				s.append(clause);
				if (iter.hasNext())
					if (!clause.equals("(") && !clause.equals(")"))
						s.append(" AND ");
			}
		}

		Iterator<String> iter = groups.iterator();
		if (iter.hasNext())
			s.append(" GROUP BY ");
		while (iter.hasNext()) {
			s.append(iter.next());
			if (iter.hasNext())
				s.append(", ");
		}

		iter = havings.iterator();
		if (iter.hasNext())
			s.append(" HAVING ");
		while (iter.hasNext()) {
			s.append(iter.next());
			if (iter.hasNext())
				s.append(", ");
		}

		if (orders.size() > 0) {
			s.append(" ORDER BY ");
			for (String order : orders) {

				s.append(order);
				s.append(", ");
			}
			s.delete(s.length() - 2, s.length());
		}

		if (limit > 0) {
			s.append(" LIMIT ");
			s.append(start);
			s.append(" ,");
			s.append(limit);
		}

		return s.toString();
	}
}
