package org.sevensoft.jeezy.db;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 14-Dec-2004 17:47:51
 */
public class Row {

	private List<Object>		list;
	private final RequestContext	context;

	Row(RequestContext context, ResultSet rs) throws SQLException {

		this.context = context;

		list = new ArrayList<Object>();
		ResultSetMetaData meta = rs.getMetaData();
		for (int n = 1; n <= meta.getColumnCount(); n++) {
			list.add(rs.getObject(n));
		}
	}

	public boolean getBoolean(int i) {

		Object obj = list.get(i);
		if (obj instanceof Boolean)
			return ((Boolean) obj).booleanValue();
		else
			return obj.toString().equals("1");
	}

	public byte getByte(int i) {

		Object obj = list.get(i);

		if (obj == null)
			return 0;

		if (obj instanceof Integer)
			return ((Integer) obj).byteValue();

		if (obj instanceof BigInteger)
			return ((BigInteger) obj).byteValue();

		else if (obj instanceof Float)
			return ((Float) obj).byteValue();

		else if (obj instanceof Byte)
			return ((Byte) obj).byteValue();

		else if (obj instanceof Short)
			return ((Short) obj).byteValue();

		else if (obj instanceof Double)
			return ((Double) obj).byteValue();

		else if (obj instanceof Long)
			return ((Long) obj).byteValue();

		else if (obj instanceof BigDecimal)
			return ((BigDecimal) obj).byteValue();

		else
			throw new RuntimeException("Unhandled object type " + obj.getClass());

	}

	/**
	 * @param i
	 * @return
	 */
	public Date getDate(int i) {

		long timestamp = getLong(i);
		return timestamp == 0 ? null : new Date(timestamp);
	}

	public DateTime getDateTime(int i) {
		return new DateTime(getLong(i));
	}

	public double getDouble(int i) {

		Object obj = list.get(i);

		if (obj == null)
			return 0;

		if (obj instanceof Integer)
			return ((Integer) obj).doubleValue();

		if (obj instanceof BigInteger)
			return ((BigInteger) obj).doubleValue();

		else if (obj instanceof Float)
			return ((Float) obj).doubleValue();

		else if (obj instanceof Byte)
			return ((Byte) obj).doubleValue();

		else if (obj instanceof Short)
			return ((Short) obj).doubleValue();

		else if (obj instanceof Double)
			return ((Double) obj).doubleValue();

		else if (obj instanceof Long)
			return ((Long) obj).doubleValue();

		else if (obj instanceof BigDecimal)
			return ((BigDecimal) obj).doubleValue();

		else if (obj instanceof String)
			return Double.parseDouble(obj.toString());

		else
			throw new RuntimeException("Unhandled object type " + obj.getClass());

	}

	public <E extends Enum<E>> E getEnum(int i, Class<E> clazz) {
		try {
			return Enum.valueOf(clazz, getString(i));
		} catch (RuntimeException e) {
			return null;
		}
	}

	public int getInt(int i) {

		Object obj = list.get(i);

		if (obj == null) {
			return 0;
		}

		if (obj instanceof Integer) {
			return ((Integer) obj).intValue();
		}

		if (obj instanceof BigInteger) {
			return ((BigInteger) obj).intValue();
		}

		else if (obj instanceof Float) {
			return ((Float) obj).intValue();
		}

		else if (obj instanceof Byte) {
			return ((Byte) obj).intValue();
		}

		else if (obj instanceof Short) {
			return ((Short) obj).intValue();
		}

		else if (obj instanceof Double) {
			return ((Double) obj).intValue();
		}

		else if (obj instanceof Long) {
			return ((Long) obj).intValue();
		}

		else if (obj instanceof BigDecimal) {
			return ((BigDecimal) obj).intValue();
		}

		else if (obj instanceof String) {
			try {
				return Integer.parseInt((String) obj);
			} catch (NumberFormatException e) {
				return 0;
			}
		}

		else {
			throw new RuntimeException("Unhandled object type " + obj.getClass());
		}
	}

	/**
	 * @param i
	 * @return
	 */
	public Integer getInteger(int i) {
		return new Integer(getInt(i));
	}

	public long getLong(int i) {

		Object obj = list.get(i);

		if (obj == null)
			return 0;

		if (obj instanceof Integer)
			return ((Integer) obj).longValue();

		if (obj instanceof BigInteger)
			return ((BigInteger) obj).longValue();

		else if (obj instanceof Float)
			return ((Float) obj).longValue();

		else if (obj instanceof Byte)
			return ((Byte) obj).longValue();

		else if (obj instanceof Short)
			return ((Short) obj).longValue();

		else if (obj instanceof Double)
			return ((Double) obj).longValue();

		else if (obj instanceof Long)
			return ((Long) obj).longValue();

		else if (obj instanceof BigDecimal)
			return ((BigDecimal) obj).longValue();

		else if (obj instanceof String)
			try {
				return Long.parseLong(obj.toString());
			} catch (NumberFormatException e) {
				return 0;
			}
		else
			throw new RuntimeException("Unhandled object type " + obj.getClass());
	}

	public Money getMoney(int i) {
		return new Money(getInt(i));
	}

	public <E extends EntityObject> E getObject(int i, Class<E> clazz) {
		return EntityObject.getInstance(context, clazz, getInt(i));
	}

	public int getShort(int i) {

		Object obj = list.get(i);

		if (obj == null) {
			return 0;
		}

		if (obj instanceof Integer) {
			return ((Integer) obj).shortValue();
		}

		if (obj instanceof BigInteger) {
			return ((BigInteger) obj).shortValue();
		}

		else if (obj instanceof Float) {
			return ((Float) obj).shortValue();
		}

		else if (obj instanceof Byte) {
			return ((Byte) obj).shortValue();
		}

		else if (obj instanceof Short) {
			return ((Short) obj).shortValue();
		}

		else if (obj instanceof Double) {
			return ((Double) obj).shortValue();
		}

		else if (obj instanceof Long) {
			return ((Long) obj).shortValue();
		}

		else if (obj instanceof BigDecimal) {
			return ((BigDecimal) obj).shortValue();
		}

		else {
			throw new RuntimeException("Unhandled object type " + obj.getClass());
		}

	}

	public String getString(int i) {

		Object obj = list.get(i);
		return obj == null ? null : obj.toString();
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();
		for (int n = 0; n < list.size(); n++) {
			sb.append(n);
			sb.append("=");
			sb.append(list.get(n));
		}

		return sb.toString();
	}

	// /**
	// * @param name
	// * @return
	// */
	// public <E extends EntityObject> E getObject(Class<E> clazz) {
	//		
	// E obj = null;
	// if (Modifier.isAbstract(clazz.getModifiers())) {
	//
	// String type = rs.getString("type");
	// if (type == null)
	// throw new RuntimeException("No implementation type stored with data");
	//
	// Class<E>[] imps = getImplementations(clazz);
	// if (imps == null)
	// throw new RuntimeException("No implementation subclasses defined in annotation");
	//
	// for (Class<E> imp : imps)
	// if (imp.getSimpleName().equals(type)) {
	// obj = imp.newInstance();
	// break;
	// }
	//
	// } else
	// obj = clazz.newInstance();
	//
	// obj.populate(rs);
	// return obj;
	//		
	//		
	// }

}
