package org.sevensoft.jeezy.db;

import org.sevensoft.commons.collections.CollectionsUtil;
import org.sevensoft.commons.collections.Predicate;
import org.sevensoft.commons.collections.maps.LinkedMultiMap;
import org.sevensoft.commons.collections.maps.MultiHashMap;
import org.sevensoft.commons.gaia.Country;
import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.commons.samdate.Period;
import org.sevensoft.commons.samdate.Time;
import org.sevensoft.commons.skint.Amount;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.jeezy.db.annotations.Singleton;
import org.sevensoft.jeezy.db.annotations.StartId;
import org.sevensoft.jeezy.db.annotations.Table;
import org.sevensoft.jeezy.db.exceptions.ConnectionUnavailableException;
import org.sevensoft.jeezy.db.positionable.Positionable;
import org.sevensoft.jeezy.http.RequestContext;
import org.sevensoft.jeezy.http.util.Selectable;
import org.sevensoft.ecreator.util.log.MDC;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ByteArrayInputStream;
import java.lang.reflect.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.*;
import java.util.*;
import java.util.logging.Logger;

/**
 * @author sks 04-Jun-2004 09:31:59
 */
public abstract class EntityObject implements Cloneable {

    private static final Map<String, List<Field>> fieldsCache = new HashMap();

    protected final static Logger logger = Logger.getLogger("ecreator");
    //private final static Logger				eologger	= Logger.getLogger("jeezy");

    private static final Class[] ClassArgs = new Class[]{RequestContext.class};

    public static void clearFieldsCache() {
        fieldsCache.clear();
    }

    /**
     * Helper method to exec runtime command
     */
    public static void exec(String command) throws IOException {

        Process process = Runtime.getRuntime().exec(command);
        execOutput(process);
    }

    public static void exec(String[] command) throws IOException {

        Process process = Runtime.getRuntime().exec(command);
        execOutput(process);
    }

    private static void execOutput(Process process) throws IOException {

        BufferedReader reader;
        String string;

        reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        while ((string = reader.readLine()) != null) {
            System.out.print(string);
        }
        reader.close();

        reader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
        while ((string = reader.readLine()) != null) {
            System.out.print(string);
        }
        reader.close();
    }

    /**
     * Returns the cache key for this singleton
     */
    public static String getCacheKey(Class<? extends EntityObject> clazz) {
        return EntityObject.getTableName(clazz) + ".singleton";
    }

    /**
     * Returns the cache key for this class plus id
     */
    public static String getCacheKey(Class<? extends EntityObject> clas, int id) {
        return EntityObject.getTableName(clas) + "." + id;
    }

    public static String getCacheKey(EntityObject obj) {
        return obj.getTableName() + "." + obj.getId();
    }

    public static String getEqualityString(Object obj) {

        if (obj == null)
            return null;

        if (obj instanceof Country)
            return ((Country) obj).getIsoAlpha2();

        if (obj instanceof Class)
            return ((Class) obj).getName();

        if (obj instanceof Enum)
            return ((Enum) obj).name();

        if (obj instanceof EntityObject)
            return ((EntityObject) obj).getIdString();

        if (obj instanceof Date)
            return ((Date) obj).getTimestampString();

        if (obj instanceof DateTime)
            return ((DateTime) obj).getTimestampString();

        if (obj instanceof Time)
            return String.valueOf(((Time) obj).getTimestamp());

        if (obj instanceof Selectable) {
            return ((Selectable) obj).getValue();
        }

        return obj.toString();
    }

    public static <E extends EntityObject> E getInstance(RequestContext context, Class<E> clazz, int id) {

        //eologger.fine("[EntityObject] requesting instance of clazz=" + clazz + ", id=" + id);

        if (clazz.getAnnotation(Singleton.class) != null) {
            return getSingleton(context, clazz);
        }

        context.getSchemaValidator().validate(clazz, context);

        if (id == 0) {
            return null;
        }

        /*
           * Lookup object from cache first
           */
        E obj = null;
        obj = (E) context.getEntityObject(getCacheKey(clazz, id), false);

        if (obj != null) {

            /*
                * Ensure the object is populated - as a blank obj may have been put in the cache by a key field populator
                */
            obj.pop();

            //eologger.fine("[EntityObject] obj from cache=" + obj);
            return obj;
        }

        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {

            conn = context.getConnection();
            if (conn == null) {
                throw new ConnectionUnavailableException();
            }

            /*
                * get two objects either side, as very very often we are getting objects in a sequence, and this will cut down database times massively.
                */

            String string = "SELECT * FROM " + getTableName(clazz) + " WHERE id>=? and id<=?";

            stmt = conn.prepareStatement(string);

            stmt.setLong(1, id > 5 ? id - 5 : 1);
            stmt.setLong(2, id + 5);

            rs = stmt.executeQuery();
            while (rs.next()) {

                E e = instantiate(clazz, rs, context, null);
                if (e.getId() == id) {
                    obj = e;
                }
            }

            return obj;

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("SQL Error retreiving entity object\n" + stmt, e);

        } catch (NullPointerException e) {
            throw new RuntimeException(e);

        } catch (IllegalArgumentException e) {
            throw new RuntimeException(e);

        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);

        } catch (SecurityException e) {
            throw new RuntimeException("bug, constructor cannot be accessed in entity object", e);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);

        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);

        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);

        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);

        } finally {

            try {
                if (rs != null) {
                    rs.close();
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }

            try {

                if (conn != null) {
                    conn.close();
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static <E extends EntityObject> E getInstance(RequestContext context, Class<E> clazz, String string) {

        if (string == null || string.length() == 0 || string.equals("0")) {
            return null;
        }

        try {
            return getInstance(context, clazz, Integer.parseInt(string.trim()));
        } catch (NumberFormatException e) {
            return null;
        }
    }

    static List<Field> getPersistableFields(Class clazz) {

        List<Field> fields = fieldsCache.get(clazz.getName());
        if (fields == null) {

            fields = new ArrayList();

            while (!EntityObject.class.equals(clazz)) {

                Field[] fs = clazz.getDeclaredFields();
                AccessibleObject.setAccessible(fs, true);

                fields.addAll(Arrays.asList(fs));

                clazz = clazz.getSuperclass();
            }

            CollectionsUtil.filter(fields, new Predicate<Field>() {

                public boolean accept(Field e) {

                    int modifiers = e.getModifiers();

                    if (Modifier.isStatic(modifiers) || Modifier.isTransient(modifiers)) {
                        return false;
                    }

                    if (e.isSynthetic()) {
                        return false;
                    }

                    if (e.getType().equals(Logger.class)) {
                        return false;
                    }

                    return true;
                }
            });

            fieldsCache.put(clazz.getName(), fields);
        }

        return fields;
    }

    public static <T extends EntityObject> T getSingleton(RequestContext context, Class<T> clazz) {

        context.getSchemaValidator().validate(clazz, context);

        /*
           * Check cache first
           */
        T obj = (T) context.getEntityObject(getCacheKey(clazz), true);

        if (obj != null) {

            /*
                * Ensure the object is populated - as a blank obj may have been put in the cache by a key field populator
                */
            obj.pop();
            return obj;
        }

        if (clazz.getAnnotation(Singleton.class) == null) {
            throw new RuntimeException("Attempt to get non-singleton as singleton!");
        }

        Query q = new Query(context, "select * from #");
        q.setTable(clazz);
        obj = q.get(clazz);

        if (obj == null) {

            try {

                Constructor c = clazz.getDeclaredConstructor(RequestContext.class);
                c.setAccessible(true);
                obj = (T) c.newInstance(new Object[]{context});

                obj.singletonInit(context);
                obj.save();

            } catch (InstantiationException e) {
                throw new RuntimeException(e);

            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);

            } catch (IllegalArgumentException e) {
                throw new RuntimeException(e);

            } catch (InvocationTargetException e) {
                throw new RuntimeException(e);

            } catch (SecurityException e) {
                throw new RuntimeException(e);

            } catch (NoSuchMethodException e) {
                throw new RuntimeException(e);
            }

        }

        context.putEntityObject(getCacheKey(clazz), obj, true);
        return obj;
    }

    static int getStartId(Class clazz) {

        StartId startId = (StartId) clazz.getAnnotation(StartId.class);
        return startId == null ? 0 : startId.value();
    }

    protected static String getTableName(Class clazz) {

        Table table = (Table) clazz.getAnnotation(Table.class);
        if (table == null)
            throw new RuntimeException("No table name annotation set " + clazz.getName());

        return table.value().toLowerCase();

    }

    /**
     * Creates a new instnace of the object and populates with the data in the results set.
     */
    static <E extends EntityObject> E instantiate(Class<E> clazz, ResultSet rs, RequestContext context, String alias) throws IllegalAccessException,
            ClassNotFoundException, SQLException, IllegalArgumentException, SecurityException, NoSuchFieldException, NoSuchMethodException,
            InvocationTargetException {

        try {

            /*
                * Get id column
                */

            int id;
            if (alias == null) {
                id = rs.getInt("id");
            } else {
                id = rs.getInt(alias + ".id");
            }

            /*
                * If cache is present then check for existance of this object.
                * While this will not save us any database lookups, as the data has already been retreved, this will stop
                * two instances of the same object being present in memory.
                */

            E obj = (E) context.getEntityObject(getCacheKey(clazz, id), false);

            if (obj != null) {

                /*
                     * Ensure the object is populated - as a blank obj may have been put in the cache by a key field populator
                     */
                obj.populate(rs, alias);
                return obj;
            }

            Constructor<E> c = clazz.getDeclaredConstructor(RequestContext.class);
            c.setAccessible(true);

            obj = c.newInstance(new Object[]{context});
            obj.populated = false;
            obj.populate(rs, alias);

            /*
                * Put instantiated object in entity cache
                */
            context.putEntityObject(obj.getCacheKey(), obj, false);
            //eologger.fine("[EntityObject] putting in cache, obj=" + obj);

            return obj;

        } catch (InstantiationException e1) {
            throw new RuntimeException("Cannot instantiate " + clazz + ", probably no zero args constructor");
        }

    }

    public static <E> E objectFromString(RequestContext context, E e, String string) {

        if (e instanceof Country) {
            return (E) Country.getInstance(string);
        }

        if (e instanceof Enum) {
            return (E) Enum.valueOf((Class<? extends Enum>) e.getClass(), string);
        }

        if (e instanceof EntityObject) {
            return (E) EntityObject.getInstance(context, (Class<? extends EntityObject>) e.getClass(), string);
        }

        throw new RuntimeException("Class not recognised " + e.getClass());
    }

    private transient int id;
    private transient boolean populated;
    protected transient RequestContext context;

    private EntityObject() {
        throw new UnsupportedOperationException();
    }

    protected EntityObject(RequestContext context) {
        this.context = context;
        this.populated = true;
    }

    @Override
    protected EntityObject clone() throws CloneNotSupportedException {

        EntityObject obj = (EntityObject) super.clone();

        obj.populated = true;
        obj.id = 0;
        obj.save();

        return obj;

    }

    public int compareTo(EntityObject obj) {
        if (id < obj.id) {
            return -1;
        } else if (id > obj.id) {
            return 1;
        } else {
            return 0;
        }
    }

    public synchronized boolean delete() {

        Connection conn = null;
        PreparedStatement stmt = null;

        try {

            conn = context.getConnection();
            if (conn == null) {
                throw new ConnectionUnavailableException();
            }

            stmt = conn.prepareStatement(" DELETE FROM " + getTableName(getClass()) + " WHERE id=? ");
            stmt.setLong(1, id);

            stmt.execute();
            id = 0;

            return true;

        } catch (SQLException e) {
            throw new RuntimeException(e);

        } finally {

            try {
                if (stmt != null) {
                    stmt.close();
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }

            try {

                if (conn != null) {
                    conn.close();
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    public final boolean equals(Object obj) {

        if (obj == null) {
            return false;
        }

        if (obj.getClass().equals(getClass())) {
            return ((EntityObject) obj).id == id;
        }

        return false;
    }

    protected final void finalize() throws Throwable {
        super.finalize();
    }

    /**
     * Returns the string used as the key for the entity object cache
     */
    private String getCacheKey() {
        return getCacheKey(getClass(), id);
    }

    public String getFullId() {
        return getTableName() + "@" + getIdString();
    }

    public final int getId() {
        return id;
    }

    public final String getIdString() {
        return String.valueOf(getId());
    }

    /**
     * Returns the id along with class instance information
     */
    public String getInstanceId() {
        return getClass().getName() + ";" + getId();
    }

    public final int getStartId() {
        return EntityObject.getStartId(getClass());
    }

    public final String getTableName() {
        return EntityObject.getTableName(getClass());
    }

    public final int hashCode() {
        return id;
    }


/*

    private synchronized final void insert(Connection conn) {

        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {

            List<Field> fields = EntityObject.getPersistableFields(getClass());

            StringBuilder sb = new StringBuilder("INSERT INTO ");
            sb.append(getTableName(getClass()));
            sb.append(" (id");

            for (Field field : fields) {
                sb.append(",`");
                sb.append(field.getName().toLowerCase());
                sb.append("`");
            }
            sb.append(") values (?");

            for (int n = 0; n < fields.size(); n++) {
                sb.append(",?");
            }

            sb.append(")");

            stmt = conn.prepareStatement(sb.toString());

            stmt.setInt(1, getMaxId(stmt.getConnection()));

            int n = 2;
            for (Field field : getPersistableFields(getClass())) {
                prepareField(stmt, n, field);
                n++;
            }

            stmt.execute();
            rs = stmt.getGeneratedKeys();

            if (rs.next()) {

                id = rs.getInt(1);

                if (id < getStartId()) {

                    PreparedStatement stmt2 = null;

                    try {

                        stmt2 = conn.prepareStatement("update `" + getTableName() + "` set id=? where id=?");
                        stmt2.setInt(1, getStartId());
                        stmt2.setInt(2, id);
                        stmt2.execute();

                        id = getStartId();

                    } catch (SQLException e) {
                        e.printStackTrace();

                    } finally {

                        try {

                            if (stmt2 != null) {
                                stmt2.close();
                            }

                        } catch (RuntimeException e) {
                            e.printStackTrace();
                        }

                    }
                }


*/
/*
                     * Put newly created entity object in entity cache if cache is present
                     */
/*

                if (context != null) {
                    context.putEntityObject(getCacheKey(), this, false);
                    //eologger.fine("[EntityObject] putting new object in cache, obj=" + this);
                }

            } else {
                //throw new RuntimeException("Object not saved!");
            }

        } catch (NullPointerException e) {
            throw new RuntimeException(e);

        } catch (SQLException e) {
            throw new RuntimeException("SQL exception in class: " + getClass() + "\n" + e);

        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);

        } finally {

            try {
                if (rs != null) {
                    rs.close();
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
*/


    private synchronized final void insert(Connection conn) {

        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {

            List<Field> fields = EntityObject.getPersistableFields(getClass());

            StringBuilder sb = new StringBuilder("INSERT INTO ");
            sb.append(getTableName(getClass()));
            sb.append(" (");

            for (int n = 1; n <= fields.size(); n++) {
                sb.append("`");
                sb.append(fields.get(n - 1).getName().toLowerCase());
                sb.append("`");
                if (n < fields.size()) {
                    sb.append(",");
                }
            }

            sb.append(") values (");

            for (int n = 1; n <= fields.size(); n++) {
                sb.append("?");
                if (n < fields.size()) {
                    sb.append(",");
                }
            }

            sb.append(")");

            stmt = conn.prepareStatement(sb.toString());

            int n = 1;
            for (Field field : getPersistableFields(getClass())) {
                prepareField(stmt, n, field);
                n++;
            }

            stmt.execute();
            rs = stmt.getGeneratedKeys();

            if (rs.next()) {

                id = rs.getInt(1);

                if (id < getStartId()) {

                    PreparedStatement stmt2 = null;

                    try {

                        stmt2 = conn.prepareStatement("update `" + getTableName() + "` set id=? where id=?");
                        stmt2.setInt(1, getStartId());
                        stmt2.setInt(2, id);
                        stmt2.execute();

                        id = getStartId();

                    } catch (SQLException e) {
                        e.printStackTrace();

                    } finally {

                        try {

                            if (stmt2 != null) {
                                stmt2.close();
                            }

                        } catch (RuntimeException e) {
                            e.printStackTrace();
                        }

                    }
                }
                /*
                * Put newly created entity object in entity cache if cache is present
                */
                if (context != null) {
                    context.putEntityObject(getCacheKey(), this, false);
                    //eologger.fine("[EntityObject] putting new object in cache, obj=" + this);
                }

            } else {
                //throw new RuntimeException("Object not saved!");
            }

        } catch (NullPointerException e) {
            throw new RuntimeException(e);

        } catch (SQLException e) {
            throw new RuntimeException(MDC.domain + " SQL exception in class: " + getClass() + "\n" + e);

        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);

        } finally {

            try {
                if (rs != null) {
                    rs.close();
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public final boolean isPersisted() {
        return getId() > 0;
    }

    public <E extends EntityObject> E pop() {

        if (populated) {
            return (E) this;
        }

        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        context.getSchemaValidator().validate(getClass(), context);

        try {

            // get connection from data source
            conn = context.getConnection();
            if (conn == null) {
                throw new ConnectionUnavailableException();
            }

            stmt = conn.prepareStatement("SELECT * FROM " + getTableName(getClass()) + " WHERE id>=? and id<=?");

            stmt.setLong(1, id > 5 ? id - 5 : 1);
            stmt.setLong(2, id + 5);

            rs = stmt.executeQuery();

            while (rs.next()) {

                if (rs.getInt("id") == id) {

                    populate(rs, null);

                } else {

                    instantiate(getClass(), rs, context, null);
                }
            }

            //			if (!populated) {
            //				throw new RuntimeException("Database error: Non existant row: " + id + " for class " + getClass());
            //			}

            return (E) this;

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);

        } catch (NullPointerException e) {
            e.printStackTrace();
            throw new RuntimeException(e);

        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            throw new RuntimeException(e);

        } catch (IllegalAccessException e) {
            e.printStackTrace();
            throw new RuntimeException(e);

        } catch (SecurityException e) {
            e.printStackTrace();
            throw new RuntimeException(e);

        } catch (NoSuchFieldException e) {
            e.printStackTrace();
            throw new RuntimeException(e);

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException(e);

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            throw new RuntimeException(e);

        } catch (InvocationTargetException e) {
            e.printStackTrace();
            throw new RuntimeException(e);

        } catch (InstantiationException e) {
            e.printStackTrace();
            throw new RuntimeException(e);

        } finally {

            try {
                if (rs != null) {
                    rs.close();
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }

            try {
                if (conn != null) {
                    conn.close();
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    void populate(ResultSet rs, String prefix) throws IllegalArgumentException, SQLException, IllegalAccessException, SecurityException,
            NoSuchFieldException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException {

        if (populated)
            return;

        if (prefix == null) {
            id = rs.getInt("id");
        } else {
            id = rs.getInt(prefix + ".id");
        }

        for (Field field : getPersistableFields(getClass())) {

            Class<?> type = field.getType();
            String col;
            if (prefix == null) {
                col = field.getName();
            } else {
                col = prefix + "." + field.getName();
            }

            if (type.isArray()) {
                populateArray(field, rs, col, type);
            } else

            if (Collection.class.isAssignableFrom(type)) {
                populateCollectionField(field, rs, col);

            } else if (Map.class.isAssignableFrom(type)) {
                populateMapField(field, rs, col);

            } else if (EntityObject.class.isAssignableFrom(type)) {
                populateKeyField(field, rs, col);

            } else {
                populateDataField(field, rs, col);

            }
        }

        populated = true;
    }

    private final void populateCollectionField(Field field, ResultSet rs, String col) throws IllegalArgumentException, IllegalAccessException, SQLException {

        String value = rs.getString(col);

        if (value == null) {
            return;
        }

        ParameterizedType type;
        try {
            type = (ParameterizedType) field.getGenericType();
        } catch (RuntimeException e1) {
            throw new RuntimeException("Field does not declare generic parameters for field: " + field);
        }
        Class rawType = (Class) type.getRawType();
        Class parameterType = (Class) type.getActualTypeArguments()[0];

        Collection<Object> collection;
        if (ArrayList.class.equals(rawType)) {
            collection = new ArrayList<Object>();
        } else if (LinkedList.class.equals(rawType)) {
            collection = new LinkedList<Object>();
        } else if (List.class.isAssignableFrom(rawType)) {
            collection = new ArrayList<Object>();
        } else if (LinkedHashSet.class.equals(rawType)) {
            collection = new LinkedHashSet<Object>();
        } else if (HashSet.class.equals(rawType)) {
            collection = new HashSet<Object>();
        } else if (TreeSet.class.equals(rawType)) {
            collection = new TreeSet<Object>();
        } else if (SortedSet.class.isAssignableFrom(rawType)) {
            collection = new TreeSet<Object>();
        } else if (Set.class.isAssignableFrom(rawType)) {
            collection = new LinkedHashSet<Object>();
        } else {
            throw new RuntimeException("Unsupported collection type " + rawType + " in " + this);
        }

        field.set(this, collection);

        if (value.length() == 0) {
            return;
        }

        for (String string : value.split("\n")) {

            if (Country.class.equals(parameterType)) {
                collection.add(Country.getInstance(string));
            } else if (DateTime.class.equals(parameterType)) {
                collection.add(new DateTime(Long.parseLong(string)));
            } else if (Double.class.equals(parameterType)) {
                collection.add(new Double(string));
            } else if (parameterType.isEnum()) {
                try {
                    collection.add(Enum.valueOf(parameterType, string));
                } catch (RuntimeException e) {
                    // e.printStackTrace();
                }
            } else if (Integer.class.equals(parameterType)) {
                collection.add(new Integer(string));
            } else if (Long.class.equals(parameterType)) {
                collection.add(new Long(string));
            } else if (Money.class.equals(parameterType)) {
                collection.add(new Money(Integer.parseInt(string)));
            } else if (String.class.equals(parameterType)) {
                collection.add(string);
            } else if (Time.class.equals(parameterType)) {
                collection.add(new Time(Long.parseLong(string)));
            } else {
                throw new RuntimeException("Unsupported collection parameterized type" + parameterType + " in collection field of " + this);
            }
        }

    }

    final void populateArray(Field field, ResultSet rs, String col, Class clazz) throws IllegalArgumentException, IllegalAccessException, SQLException {

        Class componentType = clazz.getComponentType();

        if (Byte.TYPE.equals(componentType)) {
            byte[] data = new byte[0];

            Blob blob = rs.getBlob(col);
            if (blob != null) {
                data = new byte[(int) blob.length()];

                try {
                    blob.getBinaryStream().read(data);
                } catch (IOException e) {
                    logger.warning("[" + EntityObject.class + "] " + e);
                }
            }

            logger.fine("byte[] item=" + data);

            field.set(this, data);

        } else {
            throw new RuntimeException("Unknown type when populating data field, field=" + col + ", class=" + componentType + "[]");
        }
    }

    private final void populateDataField(Field field, ResultSet rs, String column) throws IllegalArgumentException, IllegalAccessException, SQLException,
            SecurityException, NoSuchFieldException, ClassNotFoundException {

        Class type = field.getType();

        if (type.equals(Boolean.TYPE)) {
            try {
                field.setBoolean(this, rs.getByte(column) == 1 ? true : false);
            } catch (IllegalArgumentException e1) {
                e1.printStackTrace();
            } catch (IllegalAccessException e1) {
                e1.printStackTrace();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        } else if (Amount.class.equals(type)) {

            String amount = rs.getString(column);
            field.set(this, amount == null ? null : Amount.parse(amount));
        } else if (type.equals(Boolean.class)) {
            field.set(this, new Boolean(rs.getByte(column) == 1 ? true : false));

        } else if (type.equals(Byte.TYPE)) {
            field.setByte(this, rs.getByte(column));
        } else if (type.equals(Byte.class)) {
            field.set(this, new Byte(rs.getByte(column)));
        } else if (Country.class.equals(type)) {
            field.set(this, Country.getInstance(rs.getString(column)));
        } else if (type.equals(Date.class)) {
            long timestamp = rs.getLong(column);
            if (timestamp > 0) {
                field.set(this, new Date(timestamp));
            }

        } else if (type.equals(DateTime.class)) {
            long timestamp = rs.getLong(column);
            if (timestamp > 0) {
                field.set(this, new DateTime(timestamp));
            }

        } else if (type.equals(Double.TYPE)) {
            field.setDouble(this, rs.getDouble(column));
        } else if (type.equals(Double.class)) {
            field.set(this, new Double(rs.getDouble(column)));
        } else if (Enum.class.isAssignableFrom(type)) {
            String string = rs.getString(column);
            if (string != null) {
                try {
                    Enum e = Enum.valueOf((Class<? extends Enum>) type, string);
                    field.set(this, e);
                } catch (RuntimeException e) {
                    // e1.printStackTrace();
                }
            }

        } else if (type.equals(Float.TYPE)) {
            field.setFloat(this, rs.getFloat(column));
        } else if (Float.class.equals(type)) {
            field.set(this, new Float(rs.getFloat(column)));
        } else if (type.equals(Integer.TYPE)) {
            field.setInt(this, rs.getInt(column));
        } else if (type.equals(Integer.class)) {
            field.set(this, new Integer(rs.getInt(column)));
        } else if (Money.class.equals(type)) {
            field.set(this, new Money(rs.getInt(column)));
        } else if (Long.TYPE.equals(type)) {
            field.setLong(this, rs.getLong(column));
        } else if (type.equals(Long.class)) {
            field.set(this, new Long(rs.getLong(column)));
        } else if (type.equals(Period.class)) {
            String string = rs.getString(column);
            if (string != null && new Period(string).isCorrect()) {
                field.set(this, new Period(string));
            } else {
                field.set(this, null);
            }
        } else if (type.equals(Short.TYPE)) {
            field.setShort(this, rs.getShort(column));
        } else if (type.equals(Short.class)) {
            field.set(this, new Short(rs.getShort(column)));
        } else if (String.class.equals(type)) {
            field.set(this, rs.getString(column));
        } else if (Time.class.equals(type)) {
            field.set(this, new Time(rs.getLong(column)));
        } else if (URL.class.equals(type)) {
            String string = rs.getString(column);
            if (string != null) {
                try {
                    field.set(this, new URL(string));
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }

        } else {
            throw new RuntimeException("Unknown type when populating data field, field=" + column + ", class=" + type);
        }
    }

    private final void populateKeyField(Field field, ResultSet rs, String col) throws SQLException, IllegalAccessException, SecurityException,
            NoSuchMethodException, IllegalArgumentException, InvocationTargetException {

        //eologger.fine("[EntityObject] populate keyfield, field=" + field + ", col=" + col);

        int keyId;
        try {
            keyId = rs.getInt(col);
        } catch (NumberFormatException e) {
            keyId = 0;
        } catch (SQLException e) {
            keyId = 0;
        }

        if (keyId == 0) {
            return;
        }

        final String cacheKey = EntityObject.getCacheKey((Class<? extends EntityObject>) field.getType(), keyId);
        //eologger.fine("[EntityObject] cachekey=" + cacheKey);

        EntityObject keyObj = context.getEntityObject(cacheKey, false);

        if (keyObj == null) {

            Constructor c = field.getType().getDeclaredConstructor(RequestContext.class);
            c.setAccessible(true);

            try {
                keyObj = (EntityObject) c.newInstance(new Object[]{context});
            } catch (InstantiationException e) {
                throw new RuntimeException("Cannot instantiate " + field.getType() + ", probably no zero args constructor");
            }

            keyObj.id = keyId;
            keyObj.populated = false;

            context.putEntityObject(cacheKey, keyObj, false);
            //eologger.fine("[EntityObject] caching keyfield=" + keyObj);
        }

        field.set(this, keyObj);
    }

    private final void populateMapField(Field field, ResultSet rs, String col) throws SQLException, IllegalArgumentException, IllegalAccessException {

        String value = rs.getString(col);

        if (value == null) {
            return;
        }

        ParameterizedType type = (ParameterizedType) field.getGenericType();
        Class rawType = (Class) type.getRawType();
        Type[] types = type.getActualTypeArguments();

        Map map;
        if (LinkedHashMap.class.equals(rawType)) {
            map = new LinkedHashMap();
        } else if (TreeMap.class.equals(rawType)) {
            map = new TreeMap();
        } else if (LinkedMultiMap.class.equals(rawType)) {
            map = new LinkedMultiMap();
        } else if (MultiHashMap.class.equals(rawType)) {
            map = new MultiHashMap();
        } else {
            map = new HashMap();
        }

        if (value.length() == 0) {
            return;
        }

        for (String string : value.split("\n")) {

            String[] entry = string.split(";");
            if (entry.length == 2) {

                Object o1, o2;

                if (types[0].equals(Double.class)) {
                    o1 = new Double(entry[0]);
                } else if (Enum.class.isAssignableFrom((Class) types[0])) {
                    o1 = Enum.valueOf((Class) types[0], entry[0]);
                } else if (types[0].equals(Integer.class)) {
                    o1 = new Integer(entry[0]);
                } else if (types[0].equals(Long.class)) {
                    o1 = new Long(entry[0]);
                } else if (types[0].equals(Date.class)) {
                    o1 = new Date(Long.parseLong(entry[0]));
                }

                // default case covers strings
                else {
                    o1 = entry[0];
                }

                if (types[1].equals(Double.class)) {
                    o2 = new Double(entry[1]);
                } else if (types[1].equals(Integer.class)) {
                    o2 = new Integer(entry[1]);
                } else if (types[1].equals(Long.class)) {
                    o2 = new Long(entry[1]);
                } else if (types[1].equals(Money.class)) {

                    if (entry[1] == null || entry[1].trim().length() == 0 || entry[1].equals("null")) {
                        o2 = null;
                    } else {
                        try {
                            o2 = new Money(entry[1]);
                        } catch (RuntimeException e) {
                            throw new RuntimeException(entry[1]);
                        }
                    }

                } else if (types[1].equals(Date.class)) {
                    o2 = new Date(Long.parseLong(entry[1]));
                } else {
                    o2 = entry[1];
                }

                map.put(o1, o2);
            }
        }

        field.set(this, map);
    }

    private synchronized int getMaxId(Connection conn) {
        int result = 0;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = conn.prepareStatement("SELECT MAX(id) AS id FROM " + getTableName(getClass()));
            rs = stmt.executeQuery();
            if (rs.first()) {
                result = rs.getInt("id") + 1;
            }
        } catch (SQLException exc) {
            logger.fine(exc.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    protected void prepareField(PreparedStatement stmt, int n, Field field) throws IllegalArgumentException, SQLException, IllegalAccessException {

        Class type = field.getType();

        if (type.isArray()) {
            Class componentType = type.getComponentType();
            if (Byte.TYPE.equals(componentType)) {
                byte[] value = (byte[]) field.get(this);
                if (value == null) {
                    value = new byte[0];
                }
                stmt.setBinaryStream(n, new ByteArrayInputStream(value), value.length);
            }

        } else
        // boolean primitive and Boolean wrapper
        if (type.equals(Boolean.TYPE))
            stmt.setInt(n, field.getBoolean(this) == true ? 1 : 0);

        else if (type.equals(Amount.class)) {

            Amount amount = (Amount) field.get(this);
            stmt.setString(n, amount == null ? null : amount.toString());
        } else if (type.equals(Boolean.class)) {
            stmt.setInt(n, ((Boolean) field.get(this)).booleanValue() == true ? 1 : 0);

            // byte primitive and Byte wrapper
        } else if (type.equals(Byte.TYPE)) {
            stmt.setByte(n, field.getByte(this));

        } else if (type.equals(Byte.class))
            stmt.setByte(n, ((Byte) field.get(this)).byteValue());

        else if (type.equals(Class.class))
            stmt.setString(n, ((Class) field.get(this)).getSimpleName());

            // Country class
        else if (type.equals(Country.class)) {
            Country c = (Country) field.get(this);
            stmt.setString(n, c == null ? null : c.getIsoAlpha2());
        }

        // Date class
        else if (type.equals(Date.class)) {
            Date date = (Date) field.get(this);
            if (date == null)
                stmt.setNull(n, Types.NULL);
            else
                stmt.setLong(n, date.getTimestamp());
        }

        // DateTime class
        else if (type.equals(DateTime.class)) {
            DateTime date = (DateTime) field.get(this);
            if (date == null)
                stmt.setNull(n, Types.NULL);
            else
                stmt.setLong(n, date.getTimestamp());
        }

        // double primitive and Double wrapper
        else if (type.equals(Double.TYPE)) {
            double d = field.getDouble(this);
            if (Double.isNaN(d))
                stmt.setDouble(n, 0);
            else
                stmt.setDouble(n, d);

        } else if (type.equals(Double.class)) {
            Double d = (Double) field.get(this);
            if (d.isNaN())
                stmt.setDouble(n, 0);
            else
                stmt.setDouble(n, d.doubleValue());

        } else if (Enum.class.isAssignableFrom(type)) {

            Enum e = (Enum) field.get(this);
            stmt.setString(n, e == null ? null : e.name());
        }

        // float primitive and Float wrapper
        else if (type.equals(Float.TYPE))
            stmt.setFloat(n, field.getFloat(this));

        else if (type.equals(Float.class))
            stmt.setFloat(n, ((Float) field.get(this)).floatValue());

            // int primitive and Integer wrapper
        else if (type.equals(Integer.TYPE))
            stmt.setInt(n, field.getInt(this));

        else if (type.equals(Integer.class))
            stmt.setInt(n, ((Integer) field.get(this)).intValue());

            // Long primitive and long wrapper
        else if (type.equals(Long.TYPE))
            stmt.setLong(n, field.getLong(this));

        else if (type.equals(Long.class))
            stmt.setLong(n, ((Long) field.get(this)).longValue());

        else if (type.equals(Money.class)) {
            Money money = (Money) field.get(this);
            stmt.setInt(n, money == null ? 0 : money.getAmount());
        } else if (type.equals(Period.class)) {
            Period period = (Period) field.get(this);
            stmt.setString(n, period == null ? null : period.toString());
        }

        // Long primitive and long wrapper
        else if (type.equals(Short.TYPE))
            stmt.setShort(n, field.getShort(this));

        else if (type.equals(Short.class))
            stmt.setShort(n, ((Short) field.get(this)).shortValue());

            // String class
        else if (type.equals(String.class))
            stmt.setString(n, (String) field.get(this));

        else if (Time.class.equals(type)) {
            Time time = (Time) field.get(this);
            stmt.setLong(n, time == null ? 0 : time.getTimestamp());

            // key fields
        } else if (EntityObject.class.isAssignableFrom(type)) {

            EntityObject d = (EntityObject) field.get(this);
            stmt.setLong(n, d == null ? 0 : d.getId());

            // collection fields
        } else if (Collection.class.isAssignableFrom(type)) {

            Collection collection = (Collection) field.get(this);
            if (collection == null) {
                stmt.setNull(n, Types.NULL);

            } else {

                StringBuilder sb2 = new StringBuilder();
                Iterator<Object> iter2 = collection.iterator();
                while (iter2.hasNext()) {

                    Object colObj = iter2.next();

                    if (colObj instanceof Country)
                        sb2.append(((Country) colObj).getIsoAlpha2());

                    else if (colObj instanceof Date)
                        sb2.append(((Date) colObj).getTimestamp());

                    else if (colObj instanceof DateTime)
                        sb2.append(((DateTime) colObj).getTimestamp());

                    else if (colObj instanceof Money)
                        sb2.append(((Money) colObj).getAmount());

                    else if (colObj instanceof Enum)
                        sb2.append(((Enum) colObj).name());

                    else if (colObj instanceof Time)
                        sb2.append(((Time) colObj).getTimestamp());

                    else if (colObj != null)
                        sb2.append(colObj.toString());

                    if (iter2.hasNext())
                        sb2.append("\n");
                }

                stmt.setString(n, sb2.toString());
            }

        } else if (Map.class.isAssignableFrom(type)) {

            Map map = (Map) field.get(this);
            if (map == null)
                stmt.setNull(n, Types.NULL);

            else {

                StringBuilder sb2 = new StringBuilder();
                Iterator<Map.Entry> iter2 = map.entrySet().iterator();
                while (iter2.hasNext()) {

                    Map.Entry entry = iter2.next();

                    final Object key = entry.getKey();
                    final Object value = entry.getValue();

                    if (key != null)
                        sb2.append(key.toString().replaceAll(";\\n", ""));
                    sb2.append(";");
                    if (value != null)
                        sb2.append(value.toString().replaceAll(";\\n", ""));

                    if (iter2.hasNext())
                        sb2.append("\n");
                }

                stmt.setString(n, sb2.toString());
            }
        }
    }

    protected int prepareStatement(PreparedStatement stmt, int n) throws IllegalArgumentException, IllegalAccessException, SQLException {

        //eologger.finest("[EntityObject] preparing statement on " + this);

        for (Field field : getPersistableFields(getClass())) {

            prepareField(stmt, n, field);

            n++;
        }

        return n;
    }

    /*
      * This method will save the object to the database, creating a new row if needed, and updating all columns to the current values of this
      * object.
      */
    public synchronized void save() {

        context.getSchemaValidator().validate(getClass(), context);

        Connection conn = null;

        try {

            conn = context.getConnection();
            if (conn == null) {
                throw new ConnectionUnavailableException();
            }

            if (id == 0) {

                insert(conn);

            } else {

                pop();
                update(conn);
            }

            if (this instanceof Positionable) {
                Positionable p = (Positionable) this;
                if (p.getPosition() == 0) {
                    p.setPosition(getId());
                    save();
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);

        } finally {

            try {
                if (conn != null) {
                    conn.close();
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    protected final void save(String... fieldNames) {

        context.getSchemaValidator().validate(getClass(), context);

        Connection conn = null;
        PreparedStatement stmt = null;

        try {

            conn = context.getConnection();
            if (conn == null)
                throw new ConnectionUnavailableException();

            if (id == 0) {

                insert(conn);

            } else {

                StringBuilder sb = new StringBuilder(" UPDATE ");
                sb.append(getTableName(getClass()));
                sb.append(" SET ");

                for (int n = 0; n < fieldNames.length; n++) {

                    if (n > 0) {
                        sb.append(",");
                    }

                    sb.append(" `");
                    sb.append(fieldNames[n]);
                    sb.append("`=? ");
                }

                sb.append(" WHERE id=? ");

                stmt = conn.prepareStatement(sb.toString());

                // populate fields
                int n = 1;
                for (String fieldName : fieldNames) {

                    List<Field> fields = getPersistableFields(getClass());
                    for (Field field : fields) {

                        if (field.getName().equals(fieldName)) {

                            field.setAccessible(true);
                            prepareField(stmt, n, field);
                            n++;
                            break;
                        }
                    }
                }

                stmt.setInt(n, id);
                stmt.execute();

            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);

        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            throw new RuntimeException(e);

        } catch (IllegalAccessException e) {
            e.printStackTrace();
            throw new RuntimeException(e);

        } catch (SecurityException e) {
            e.printStackTrace();
            throw new RuntimeException(e);

        } finally {

            try {
                if (stmt != null)
                    stmt.close();

            } catch (SQLException e) {
                e.printStackTrace();
            }

            try {
                if (conn != null)
                    conn.close();

            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
    }

    protected void schemaInit(RequestContext context) {
    }

    protected void singletonInit(RequestContext context) {
    }

    public String toString() {
        return getClass().getSimpleName() + "@" + super.hashCode() + " id=" + id + " pop=" + populated;
    }

    private synchronized final void update(Connection conn) {

        //eologger.finest("[EntityObject] calling update on " + this);

        PreparedStatement stmt = null;

        try {

            StringBuilder sb = new StringBuilder("UPDATE ");
            sb.append(getTableName(getClass()));
            sb.append(" SET ");

            Iterator<Field> iter = EntityObject.getPersistableFields(getClass()).iterator();
            while (iter.hasNext()) {

                Field field = iter.next();

                sb.append("`");
                sb.append(field.getName());
                sb.append("`=?");
                if (iter.hasNext())
                    sb.append(", ");
            }

            sb.append(" WHERE id=? ");

            //eologger.finest("query=" + sb);

            stmt = conn.prepareStatement(sb.toString());
            int n = 1;

            for (Field field : getPersistableFields(getClass())) {
                prepareField(stmt, n, field);
                n++;
            }

            stmt.setInt(n, id);
            stmt.execute();

        } catch (NullPointerException e) {
            throw new RuntimeException(e);

        } catch (SQLException e) {
            throw new RuntimeException(e);

        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);

        } finally {

            try {
                if (stmt != null)
                    stmt.close();

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}