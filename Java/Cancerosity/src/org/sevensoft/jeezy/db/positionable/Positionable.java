package org.sevensoft.jeezy.db.positionable;

/**
 * @author sks 31-Aug-2005 17:13:36
 * 
 */
public interface Positionable {

	public int getPosition();

	public void setPosition(int position);
}
