package org.sevensoft.jeezy.db.positionable;

import java.util.Comparator;

/**
 * @author sks 15 Aug 2006 11:58:18
 *
 */
public class ReorderingPositionComparator implements Comparator<Positionable> {

	private boolean	reorder;

	public int compare(Positionable o1, Positionable o2) {

		if (o1.getPosition() < o2.getPosition())
			return -1;

		if (o1.getPosition() > o2.getPosition())
			return 1;

		/* 
		 * if we are here then we have two positions that are equal. 
		 * We want to flag the system to re-order all elements inside here afterwards 
		 * with a new position, each one unique, but in the same order as this comparator has returned
		 */
		reorder = true;

		return 0;
	}

	public boolean isReorder() {
		return reorder;
	}
}
