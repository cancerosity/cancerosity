package org.sevensoft.jeezy.db.positionable;

import org.sevensoft.jeezy.db.EntityObject;
import org.sevensoft.jeezy.http.Handler;
import org.sevensoft.jeezy.http.html.ImageTag;
import org.sevensoft.jeezy.http.html.links.LinkTag;
import org.sevensoft.jeezy.http.util.Link;

/**
 * @author sks 1 Aug 2006 11:31:01
 *
 */
public class PositionRender {

	/**
	 * Class used for requests to re-order elements
	 */
	private Class<? extends Handler>	clazz;
	private int					n;
	private final String			param;

	public PositionRender(Class<? extends Handler> clazz, String param) {
		this.clazz = clazz;
		this.param = param;
	}

	public PositionRender(String param) {
		this.param = param;
	}

	public String render() {
		n++;
		return String.valueOf(n);
	}

	public String render(Class<? extends Handler> clazz, EntityObject p) {
		this.clazz = clazz;
		return render(p);
	}

	public String render(EntityObject obj) {
		return render(obj.getIdString());
	}

	public String render(EntityObject obj, Object... params) {
		return render(obj.getIdString(), params);
	}

	public String render(String value) {
		return render(value, (Object[]) null);
	}

	public String render(String value, Object... params) {

		n++;

		StringBuilder sb = new StringBuilder();

		sb.append(n);
		sb.append(". ");

		Link link = new Link(clazz, "moveUp", params);
		link.setParameter(param, value);

		sb.append(new LinkTag(link, new ImageTag("files/graphics/admin/arrowup.gif")));
		sb.append(" ");

		link = new Link(clazz, "moveDown", params);
		link.setParameter(param, value);

		sb.append(new LinkTag(link, new ImageTag("files/graphics/admin/arrowdown.gif")));

		return sb.toString();
	}

}