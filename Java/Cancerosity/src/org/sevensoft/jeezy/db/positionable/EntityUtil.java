package org.sevensoft.jeezy.db.positionable;

import java.util.Collections;
import java.util.List;

import org.sevensoft.jeezy.db.EntityObject;

/**
 * @author sks 12-Oct-2005 10:36:50
 * 
 */
public class EntityUtil {

	private static void move(Positionable obj, List<? extends Positionable> list) {

		// get position of this item
		int index = list.indexOf(obj);

		// if this item is the first one in the list or not in list at all then exit
		if (index < 1)
			return;

		// get the item before this one so we can swap pos
		Positionable swap = list.get(index - 1);

		final int p1 = obj.getPosition();
		final int p2 = swap.getPosition();

		// swap positions
		obj.setPosition(p2);
		((EntityObject) obj).save();

		swap.setPosition(p1);
		((EntityObject) swap).save();
	}

	public static void moveDown(Positionable obj, List<? extends Positionable> list) {

		if (list.size() < 2)
			return;

		final ReorderingPositionComparator comparator = new ReorderingPositionComparator();
		Collections.sort(list, comparator);

		if (comparator.isReorder())
			reorder(list);

		Collections.reverse(list);

		move(obj, list);
	}

	public static void moveUp(Positionable obj, List<? extends Positionable> list) {

		if (list.size() < 2)
			return;

		final ReorderingPositionComparator comparator = new ReorderingPositionComparator();
		Collections.sort(list, comparator);

		if (comparator.isReorder())
			reorder(list);

		move(obj, list);
	}

	private static void reorder(List<? extends Positionable> list) {

		int n = 1;
		for (Positionable e : list) {

			e.setPosition(n);
			((EntityObject) e).save();

			n++;
		}
	}
}
