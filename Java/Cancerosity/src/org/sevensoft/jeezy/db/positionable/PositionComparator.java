package org.sevensoft.jeezy.db.positionable;

import java.util.Comparator;

/**
 * @author sks 15 Aug 2006 11:56:20
 *
 */
public class PositionComparator implements Comparator<Positionable> {

	public static final PositionComparator	instance	= new PositionComparator();

	public int compare(Positionable o1, Positionable o2) {

		if (o1.getPosition() < o2.getPosition())
			return -1;

		if (o1.getPosition() > o2.getPosition())
			return 1;

		return 0;
	}
}
