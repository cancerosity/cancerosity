package org.sevensoft.jeezy.db;

import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Logger;

import org.sevensoft.commons.collections.maps.LinkedMultiMap;
import org.sevensoft.commons.collections.maps.MultiHashMap;
import org.sevensoft.commons.gaia.Country;
import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.commons.samdate.Time;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.jeezy.db.exceptions.ConnectionUnavailableException;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sam
 */
public class Query {

	private static Logger		logger	= Logger.getLogger("ecreator");

	protected boolean			debug		= false, ordered = false;

	protected List<Object>		parameters;

	protected String			query;
	private List<String>		tables;

	private final RequestContext	context;

	public Query(RequestContext context, CharSequence sb) {

		this.context = context;
		this.query = sb.toString();
		this.parameters = new ArrayList();
		this.tables = new ArrayList();
	}

	public Query(RequestContext context, Class<? extends EntityObject> clazz) {
		this(context, "select * from #");
		setTable(clazz);
	}

	public void clearTables() {
		tables.clear();
	}

	@SuppressWarnings("null")
	public List<Row> execute() {

		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {

			conn = context.getConnection();
			if (conn == null) {
				throw new ConnectionUnavailableException();
			}

			stmt = prepareStatement(conn);

			populateStatement(stmt);

			logger.fine(stmt.toString());

			List<Row> results = new ArrayList<Row>();
			rs = stmt.executeQuery();
			while (rs.next()) {

				Row row = new Row(context, rs);
				results.add(row);
			}

			return results;

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(stmt.toString(), e);

		} finally {

			try {
				if (rs != null)
					rs.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}

			try {
				if (stmt != null)
					stmt.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public <E extends EntityObject> List<E> execute(Class<E> clazz) {

		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {

			conn = context.getConnection();
			if (conn == null) {
				throw new ConnectionUnavailableException();
			}

			stmt = prepareStatement(conn);
			populateStatement(stmt);

			logger.fine(stmt.toString());

			List<E> results = new ArrayList();

			rs = stmt.executeQuery();
			while (rs.next()) {
				results.add(EntityObject.instantiate(clazz, rs, context, null));
			}

			return results;

		} catch (SQLException e) {
			logger.warning("SQLException: Statement: "+stmt+ "\n"+e);
            e.printStackTrace();

			throw new RuntimeException(e);

		} catch (IllegalAccessException e) {
            logger.warning("SQLException: Statement: "+stmt+ "\n"+e);
			e.printStackTrace();
			throw new RuntimeException("BUG: Class does not have public zero arg constructor", e);

		} catch (Exception e) {
            logger.warning("SQLException: Statement: "+stmt+ "\n"+e);
			e.printStackTrace();
			throw new RuntimeException(e);

		} finally {

			try {

				if (rs != null) {
					rs.close();
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}

			try {
				if (stmt != null) {
					stmt.close();
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}

			try {

				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
	}

	public <E extends EntityObject> List<E> execute(Class<E> clazz, int limit) {
		return execute(clazz, 0, limit);
	}

	public <E extends EntityObject> List<E> execute(Class<E> clazz, int start, int limit) {
		setLimit(start, limit);
		return execute(clazz);
	}

	public List<Row> execute(int limit) {
		return execute(0, limit);
	}

	public List<Row> execute(int start, int limit) {
		setLimit(start, limit);
		return execute();
	}

	@SuppressWarnings("null")
	public Row get() {

		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {

			conn = context.getConnection();
			if (conn == null)
				throw new ConnectionUnavailableException();

			stmt = prepareStatement(conn);
			populateStatement(stmt);

			rs = stmt.executeQuery();
			if (rs.next())
				return new Row(context, rs);
			else
				return null;

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(stmt.toString(), e);

		} finally {

			try {
				if (rs != null)
					rs.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}

			try {
				if (stmt != null)
					stmt.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}

			try {
				conn.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
	}

	public <E extends EntityObject> E get(Class<E> clazz) {
		return retrieve(clazz);
	}

	@SuppressWarnings("null")
	public boolean getBoolean() {

		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {

			conn = context.getConnection();
			if (conn == null)
				throw new ConnectionUnavailableException();

			stmt = prepareStatement(conn);
			populateStatement(stmt);

			rs = stmt.executeQuery();
			if (rs.next())
				return rs.getInt(1) == 1;
			else
				return false;

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(stmt.toString(), e);

		} finally {

			try {
				if (rs != null)
					rs.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}

			try {
				if (stmt != null)
					stmt.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}

			try {
				conn.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
	}

	@SuppressWarnings("null")
	public byte getByte() {

		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {

			conn = context.getConnection();
			if (conn == null)
				throw new ConnectionUnavailableException();

			stmt = prepareStatement(conn);
			populateStatement(stmt);

			rs = stmt.executeQuery();
			if (rs.next()) {
				return rs.getByte(1);
			} else {
				return 0;
			}

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(stmt.toString(), e);

		} finally {

			try {
				if (rs != null)
					rs.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}

			try {
				if (stmt != null)
					stmt.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}

			try {
				conn.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
	}

	@SuppressWarnings("null")
	public Date getDate() {

		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {

			conn = context.getConnection();
			if (conn == null)
				throw new ConnectionUnavailableException();

			stmt = prepareStatement(conn);
			populateStatement(stmt);

			rs = stmt.executeQuery();
			if (rs.next())
				return new Date(rs.getLong(1));
			else
				return null;

		} catch (SQLException e) {
			e.printStackTrace();

			throw new RuntimeException(stmt.toString(), e);

		} finally {

			try {
				if (rs != null)
					rs.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}

			try {
				if (stmt != null)
					stmt.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}

			try {
				conn.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
	}

	@SuppressWarnings("null")
	public List<Date> getDates() {

		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {

			conn = context.getConnection();
			if (conn == null)
				throw new ConnectionUnavailableException();

			stmt = prepareStatement(conn);
			populateStatement(stmt);

			rs = stmt.executeQuery();
			List<Date> list = new ArrayList<Date>();
			while (rs.next()) {
				list.add(new Date(rs.getLong(1)));
			}
			return list;

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(stmt.toString(), e);

		} finally {

			try {
				if (rs != null)
					rs.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}

			try {
				if (stmt != null)
					stmt.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}

			try {
				conn.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
	}

	/**
	 * @return
	 */
	@SuppressWarnings("null")
	public DateTime getDateTime() {

		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {

			conn = context.getConnection();
			if (conn == null)
				throw new ConnectionUnavailableException();

			stmt = prepareStatement(conn);
			populateStatement(stmt);

			rs = stmt.executeQuery();
			if (rs.next())
				return new DateTime(rs.getLong(1));
			else
				return null;

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(stmt.toString(), e);

		} finally {

			try {
				if (rs != null)
					rs.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}

			try {
				if (stmt != null)
					stmt.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}

			try {
				conn.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
	}

	/**
	 * @return
	 */
	@SuppressWarnings("null")
	public List<DateTime> getDateTimes() {

		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {

			conn = context.getConnection();
			if (conn == null)
				throw new ConnectionUnavailableException();

			stmt = prepareStatement(conn);
			populateStatement(stmt);

			rs = stmt.executeQuery();
			List<DateTime> list = new ArrayList<DateTime>();
			while (rs.next()) {
				list.add(new DateTime(rs.getLong(1)));
			}
			return list;

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(stmt.toString(), e);

		} finally {

			try {
				if (rs != null)
					rs.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}

			try {
				if (stmt != null)
					stmt.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}

			try {
				conn.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
	}

	@SuppressWarnings( { "null", "null" })
	public double getDouble() {

		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {

			conn = context.getConnection();
			if (conn == null)
				throw new ConnectionUnavailableException();

			stmt = prepareStatement(conn);
			populateStatement(stmt);

			rs = stmt.executeQuery();
			if (rs.next())
				return rs.getDouble(1);
			else
				return 0;

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(stmt.toString(), e);

		} finally {

			try {
				if (rs != null)
					rs.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}

			try {
				if (stmt != null)
					stmt.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}

			try {
				conn.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
	}

	/**
	 * @param name
	 * @return
	 */
	public <E extends Enum> E getEnum(Class<E> clazz) {

		String string = getString();
		if (string == null)
			return null;

		return (E) Enum.valueOf(clazz, string);
	}

	/**
	 * @param name
	 * @return
	 */
	@SuppressWarnings("null")
	public <E extends Enum> List<E> getEnums(Class<E> clazz) {

		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {

			conn = context.getConnection();
			if (conn == null)
				throw new RuntimeException();

			stmt = prepareStatement(conn);
			if (stmt == null)
				throw new RuntimeException();

			populateStatement(stmt);

			rs = stmt.executeQuery();
			List<E> list = new ArrayList<E>();
			while (rs.next()) {
				try {
					E e = (E) Enum.valueOf(clazz, rs.getString(1));
					list.add(e);
				} catch (RuntimeException e) {
					e.printStackTrace();
				}
			}
			return list;

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(stmt.toString(), e);

		} finally {

			try {
				if (rs != null)
					rs.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}

			try {
				if (stmt != null)
					stmt.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}

			try {
				conn.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
	}

	public int getInt() {

		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {

			conn = context.getConnection();
			if (conn == null) {
				throw new ConnectionUnavailableException();
			}

			stmt = prepareStatement(conn);
			populateStatement(stmt);

			logger.fine(stmt.toString());

			rs = stmt.executeQuery();
			if (rs.next()) {
				return rs.getInt(1);
			} else {
				return 0;
			}

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);

		} finally {

			try {
				if (rs != null)
					rs.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}

			try {
				if (stmt != null)
					stmt.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
	}

	/**
	 * 
	 */
	public List<Integer> getInts() {

		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {

			conn = context.getConnection();
			if (conn == null) {
				throw new ConnectionUnavailableException();
			}

			stmt = prepareStatement(conn);
			populateStatement(stmt);

			rs = stmt.executeQuery();

			List<Integer> list = new ArrayList();
			while (rs.next()) {
				list.add(rs.getInt(1));
			}

			return list;

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);

		} finally {

			try {
				if (rs != null) {
					rs.close();
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}

			try {
				if (stmt != null) {
					stmt.close();
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
	}

	/**
	 * @param name
	 * @param name2
	 * @param i
	 * @return
	 */
	public Map<String, String> getLinkedHashMap(Class c1, Class c2, int i) {
		setLimit(i);
		return getLinkedHashMap(c1, c2);
	}

	public Map getLinkedHashMap(Class<? extends Object> c1, Class<? extends Object> c2) {
		return getMap(c1, c2, new LinkedHashMap());
	}

	/**
	 * @param name
	 * @param name2
	 * @return
	 */
	public LinkedMultiMap getLinkedMultiMap(Class<? extends Object> c1, Class<? extends Object> c2) {
		return (LinkedMultiMap) getMap(c1, c2, new LinkedMultiMap());
	}

	@SuppressWarnings("null")
	public long getLong() {

		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {

			conn = context.getConnection();
			if (conn == null) {
				throw new ConnectionUnavailableException();
			}

			stmt = prepareStatement(conn);
			populateStatement(stmt);

			rs = stmt.executeQuery();
			if (rs.next()) {
				return rs.getLong(1);
			} else {
				return 0;
			}

		} catch (SQLException e) {
			e.printStackTrace();

			throw new RuntimeException(stmt.toString(), e);

		} finally {

			try {
				if (rs != null) {
					rs.close();
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}

			try {
				if (stmt != null) {
					stmt.close();
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
	}

	public <K extends Object, V extends Object> Map<K, V> getMap(Class<K> c1, Class<V> c2) {
		return getMap(c1, c2, new HashMap());
	}

	public <K extends Object, V extends Object> Map<K, V> getMap(Class<K> c1, Class<V> c2, Map<K, V> map) {

		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {

			conn = context.getConnection();
			if (conn == null) {
				throw new ConnectionUnavailableException();
			}

			stmt = prepareStatement(conn);
			populateStatement(stmt);

			rs = stmt.executeQuery();
			while (rs.next()) {
				K k = (K) getObject(rs, 1, c1);
				V v = (V) getObject(rs, 2, c2);
				if (k != null) {
					map.put(k, v);
				}
			}
			return map;

		} catch (SQLException e) {
			e.printStackTrace();

			throw new RuntimeException(e);

		} finally {

			try {
				if (rs != null) {
					rs.close();
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}

			try {
				if (stmt != null) {
					stmt.close();
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
	}

	public <K extends Object, V extends Object> Map<K, V> getMap(Class<K> c1, String prefix1, Class<V> c2, String prefix2, Map<K, V> map) {

		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {

			conn = context.getConnection();
			if (conn == null) {
				throw new ConnectionUnavailableException();
			}

			stmt = prepareStatement(conn);
			populateStatement(stmt);

			rs = stmt.executeQuery();
			while (rs.next()) {
				K k = (K) getObject(rs, prefix1, c1);
				V v = (V) getObject(rs, prefix2, c2);
				map.put(k, v);
			}
			return map;

		} catch (Exception e) {
			e.printStackTrace();

			throw new RuntimeException(e);

		} finally {

			try {
				if (rs != null) {
					rs.close();
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}

			try {
				if (stmt != null) {
					stmt.close();
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}

	}

	@SuppressWarnings("null")
	public Money getMoney() {

		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {

			conn = context.getConnection();
			if (conn == null)
				throw new ConnectionUnavailableException();

			stmt = prepareStatement(conn);
			populateStatement(stmt);

			rs = stmt.executeQuery();
			if (rs.next())
				return new Money(rs.getInt(1));
			else
				return null;

		} catch (SQLException e) {
			e.printStackTrace();

			throw new RuntimeException(stmt.toString(), e);

		} finally {

			try {
				if (rs != null)
					rs.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}

			try {
				if (stmt != null)
					stmt.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}

			try {
				if (conn != null)
					conn.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
	}

	/**
	 * @return
	 */
	public List<Money> getMonies() {

		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {

			conn = context.getConnection();
			if (conn == null) {
				throw new RuntimeException();
			}

			stmt = prepareStatement(conn);
			if (stmt == null) {
				throw new RuntimeException();
			}

			populateStatement(stmt);

			rs = stmt.executeQuery();
			List<Money> list = new ArrayList();
			while (rs.next()) {
				list.add(new Money(rs.getInt(1)));
			}
			return list;

		} catch (SQLException e) {
			e.printStackTrace();

			throw new RuntimeException(stmt.toString(), e);

		} finally {

			try {
				if (rs != null) {
					rs.close();
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}

			try {
				if (stmt != null) {
					stmt.close();
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}

			try {
				conn.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
	}

	public <K extends Object, V extends Object> MultiHashMap<K, V> getMultiMap(Class<K> c1, Class<V> c2) {
		return (MultiHashMap<K, V>) getMap(c1, c2, new MultiHashMap<K, V>());
	}

	private Object getObject(ResultSet rs, int i, Class<? extends Object> clazz) throws SQLException {

		if (clazz.equals(Money.class)) {
			return new Money(rs.getLong(i));

		} else if (clazz.equals(Date.class)) {
			return new Date(rs.getLong(i));

		} else if (clazz.equals(String.class)) {
			return rs.getString(i);

		} else if (clazz.equals(Integer.class)) {
			return rs.getInt(i);

		} else if (clazz.equals(Double.class)) {
			return rs.getDouble(i);

		} else if (Enum.class.isAssignableFrom(clazz)) {
			return Enum.valueOf((Class<? extends Enum>) clazz, rs.getString(i));

		} else if (EntityObject.class.isAssignableFrom(clazz)) {
			return EntityObject.getInstance(context, (Class<? extends EntityObject>) clazz, rs.getString(i));

		} else if (clazz.equals(Long.class)) {
			return rs.getLong(i);

		} else if (clazz.equals(Integer.class)) {
			return rs.getInt(i);
		}

		return null;
	}

	private Object getObject(ResultSet rs, String alias, Class clazz) throws SQLException, IllegalArgumentException, SecurityException,
			InstantiationException, IllegalAccessException, ClassNotFoundException, NoSuchFieldException, NoSuchMethodException,
			InvocationTargetException {

		if (clazz.equals(Money.class)) {
			return new Money(rs.getLong(alias));
		}

		else if (clazz.equals(Date.class)) {
			return new Date(rs.getLong(alias));
		}

		else if (clazz.equals(String.class)) {
			return rs.getString(alias);
		}

		else if (Enum.class.isAssignableFrom(clazz)) {
			return Enum.valueOf((Class<? extends Enum>) clazz, rs.getString(alias));
		}

		else if (EntityObject.class.isAssignableFrom(clazz)) {
			return EntityObject.instantiate(clazz, rs, context, alias);
		}

		else if (clazz.equals(Long.class)) {
			return rs.getLong(alias);
		}

		else if (clazz.equals(Integer.class)) {
			return rs.getInt(alias);
		}

		return null;
	}

	/**
	 * @return
	 */
	@SuppressWarnings("null")
	public short getShort() {

		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {

			conn = context.getConnection();
			if (conn == null) {
				throw new ConnectionUnavailableException();
			}

			stmt = prepareStatement(conn);
			populateStatement(stmt);

			rs = stmt.executeQuery();
			if (rs.next()) {
				return rs.getShort(1);
			} else {
				return 0;
			}

		} catch (SQLException e) {
			e.printStackTrace();

			throw new RuntimeException(stmt.toString(), e);

		} finally {

			try {
				if (rs != null) {
					rs.close();
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}

			try {
				if (stmt != null) {
					stmt.close();
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}

			try {
				if (conn != null) {
					conn.close();
				}

			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
	}

	public Map<String, Integer> getSortedMap(Class<String> c1, Class<Integer> c2, Comparator<String> c) {
		return getMap(c1, c2, new TreeMap(c));
	}

	@SuppressWarnings("null")
	public String getString() {

		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {

			conn = context.getConnection();
			if (conn == null) {
				throw new ConnectionUnavailableException();
			}

			stmt = prepareStatement(conn);
			populateStatement(stmt);

			rs = stmt.executeQuery();
			if (rs.next())
				return rs.getString(1);
			else
				return null;

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(stmt.toString(), e);

		} finally {

			try {
				if (rs != null) {
					rs.close();
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}

			try {
				if (stmt != null) {
					stmt.close();
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}

			try {
				if (conn != null) {
					conn.close();
				}

			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
	}

	public String[] getStringArray() {
		return getStrings().toArray(new String[0]);
	}

	public List<String> getStrings() {

		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {

			conn = context.getConnection();
			if (conn == null) {
				throw new ConnectionUnavailableException();
			}

			stmt = prepareStatement(conn);
			populateStatement(stmt);

			rs = stmt.executeQuery();

			List<String> list = new ArrayList();
			while (rs.next()) {
				list.add(rs.getString(1));
			}
			return list;

		} catch (SQLException e) {
			e.printStackTrace();

			throw new RuntimeException(stmt.toString(), e);

		} finally {

			try {
				if (rs != null) {
					rs.close();
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}

			try {
				if (stmt != null) {
					stmt.close();
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}

			try {
				if (conn != null) {
					conn.close();
				}

			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
	}

	public List<String> getStrings(int limit) {
		setLimit(limit);
		return getStrings();
	}

	public Set<String> getStringsSet() {

		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {

			conn = context.getConnection();
			if (conn == null) {
				throw new ConnectionUnavailableException();
			}

			stmt = prepareStatement(conn);
			populateStatement(stmt);

			rs = stmt.executeQuery();

			Set<String> set = new HashSet();
			while (rs.next()) {
				set.add(rs.getString(1));
			}
			return set;

		} catch (SQLException e) {
			e.printStackTrace();

			throw new RuntimeException(stmt.toString(), e);

		} finally {

			try {
				if (rs != null) {
					rs.close();
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}

			try {
				if (stmt != null) {
					stmt.close();
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}

			try {
				if (conn != null) {
					conn.close();
				}

			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
	}

	public Map getTreeMap(Class<? extends Object> c1, Class<? extends Object> c2) {
		return getMap(c1, c2, new TreeMap());
	}

	public Map getTreeMap(Class<? extends Object> c1, Class<? extends Object> c2, Comparator comparator) {
		return getMap(c1, c2, new TreeMap(comparator));
	}

	public void order(String order) {
		if (ordered) {
			query = query + " , " + order;
		} else {
			query = query + " order by " + order;
			ordered = true;
		}
	}

	protected void populateStatement(PreparedStatement stmt) throws SQLException {

		for (int n = 0; n < parameters.size(); n++) {

			Object obj = parameters.get(n);

			if (obj == null) {
				stmt.setNull(n + 1, Types.NULL);
			}

			else if (obj instanceof Byte) {
				stmt.setByte(n + 1, (Byte) obj);
			}

			else if (obj instanceof Character) {
				stmt.setString(n + 1, ((Character) obj).toString());
			}

			else if (obj instanceof Boolean) {
				stmt.setInt(n + 1, ((Boolean) obj).booleanValue() ? 1 : 0);
			}

			else if (obj instanceof Class) {
				stmt.setString(n + 1, ((Class) obj).getSimpleName());
			}

			else if (obj instanceof Country)
				stmt.setString(n + 1, ((Country) obj).getIsoAlpha2());

			else if (obj instanceof Date)
				stmt.setLong(n + 1, ((Date) obj).getTimestamp());

			else if (obj instanceof DateTime)
				stmt.setLong(n + 1, ((DateTime) obj).getTimestamp());

			else if (obj instanceof Double)
				stmt.setDouble(n + 1, ((Double) obj).doubleValue());

			else if (obj instanceof EntityObject)
				stmt.setLong(n + 1, ((EntityObject) obj).getId());

			else if (obj instanceof Enum)
				stmt.setString(n + 1, ((Enum) obj).name());

			else if (obj instanceof Float)
				stmt.setFloat(n + 1, ((Float) obj).floatValue());

			else if (obj instanceof Integer)
				stmt.setInt(n + 1, ((Integer) obj).intValue());

			else if (obj instanceof Long)
				stmt.setLong(n + 1, (Long) obj);

			else if (obj instanceof Money)
				stmt.setInt(n + 1, ((Money) obj).getAmount());

			else if (obj instanceof String)
				stmt.setString(n + 1, (String) obj);

			else if (obj instanceof Time)
				stmt.setLong(n + 1, ((Time) obj).getTimestamp());

			else
				throw new RuntimeException("Unknown parameter type" + obj.getClass());
		}

	}

	private PreparedStatement prepareStatement(Connection conn) throws SQLException {

		String string = query;

		for (String table : tables) {
			string = string.replaceFirst("\\#", table);
		}

		return conn.prepareStatement(string);
	}

	public void reset() {
		parameters.clear();
	}

	@SuppressWarnings("null")
	public <E extends EntityObject> E retrieve(Class<E> clazz) {

		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {

			conn = context.getConnection();
			stmt = prepareStatement(conn);

			populateStatement(stmt);

			rs = stmt.executeQuery();
			if (rs.next()) {

				E obj = EntityObject.instantiate(clazz, rs, context, null);
				return obj;
			}

			return null;

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);

		} catch (IllegalAccessException e) {
			e.printStackTrace();
			throw new RuntimeException("BUG: Class does not have public zero arg constructor", e);

		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			throw new RuntimeException(stmt.toString(), e);

		} catch (SecurityException e) {
			e.printStackTrace();
			throw new RuntimeException(stmt.toString(), e);

		} catch (NoSuchFieldException e) {
			e.printStackTrace();
			throw new RuntimeException(stmt.toString(), e);

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(stmt.toString(), e);

		} catch (NoSuchMethodException e) {
			e.printStackTrace();
			throw new RuntimeException(stmt.toString(), e);

		} catch (InvocationTargetException e) {
			e.printStackTrace();
			throw new RuntimeException(stmt.toString(), e);

		} finally {

			try {
				if (rs != null) {
					rs.close();
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}

			try {
				if (stmt != null) {
					stmt.close();
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}

			try {
				if (conn != null) {
					conn.close();
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@SuppressWarnings("null")
	public int run() {

		Connection conn = null;
		PreparedStatement stmt = null;

		try {

			conn = context.getConnection();
			if (conn == null) {
				throw new ConnectionUnavailableException();
			}

			stmt = prepareStatement(conn);
			populateStatement(stmt);
			stmt.execute();

			return stmt.getUpdateCount();

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(stmt.toString(), e);

		} finally {

			try {
				if (stmt != null) {
					stmt.close();
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}

			try {
				if (conn != null) {
					conn.close();
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	public void setDebug() {
		this.debug = true;
	}

	public void setDebug(boolean debug) {
		this.debug = debug;
	}

	public void setLimit(int limit) {
		if (limit > 0) {
			query = query + " limit " + limit;
		}
	}

	public Query setLimit(int start, int limit) {

		if (limit > 0) {

			if (start < 0) {
				start = 0;
			}

			// remove existing limit tag
			query = query.replaceAll(" limit\\s\\d+,\\d+", "");
			query = query + " limit " + start + "," + limit;
		}

		return this;
	}

	public Query setParam(Object obj) {
		parameters.add(obj);
		return this;
	}

	public Query setParameter(Enum e) {
		parameters.add(e);
		return this;
	}

	public Query setParameter(Object obj) {
		parameters.add(obj);
		return this;
	}

	public void setParameters(Collection c) {
		for (Object obj : c) {
			setParameter(obj);
		}
	}

	public void setParameters(Object... obj) {
		for (Object o : obj) {
			setParam(o);
		}
	}

	public void setParams(Object... obj) {
		for (Object o : obj) {
			setParam(o);
		}
	}

	public <E extends EntityObject> Query setTable(Class<E> clazz) {

		context.getSchemaValidator().validate(clazz, context);
		tables.add(EntityObject.getTableName(clazz));

		return this;
	}

}