package org.sevensoft.jeezy.db;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.sql.DataSource;

import org.sevensoft.commons.gaia.Country;
import org.sevensoft.commons.samdate.Date;
import org.sevensoft.commons.samdate.DateTime;
import org.sevensoft.commons.samdate.Period;
import org.sevensoft.commons.samdate.Time;
import org.sevensoft.commons.skint.Amount;
import org.sevensoft.commons.skint.Money;
import org.sevensoft.jeezy.db.annotations.Default;
import org.sevensoft.jeezy.db.annotations.Index;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author Sam 12-May-2003 14:32:58
 */
public class SchemaValidator {

	//	private static //logger		//logger	= //logger.get//logger("schemaval");

	private final boolean		debug		= false;
	private final Set<String>	checked	= new HashSet();
	private DataSource		ds;

	public SchemaValidator(DataSource ds) {

		this.ds = ds;
		if (ds == null) {
			throw new RuntimeException("DataSource pool is null");
		}
	}

	private void checkFields(Class clazz, Connection conn, String tableName, List<String> columns) throws SQLException {

		// Check for fields
		for (Field field : EntityObject.getPersistableFields(clazz)) {

			final String fieldName = field.getName().toLowerCase();
			if (!columns.contains(fieldName)) {
				createColumn(conn, tableName, field);
			}

			// check this column has an index if present
			Index index = field.getAnnotation(Index.class);
			if (index != null) {

				String indexName = fieldName + "_index";

				// check if this index already exists
				List<String> indexes = getIndexes(conn, tableName);
				if (!indexes.contains(indexName)) {
					createIndex(conn, field, tableName, fieldName, indexName, index);
				}
			}

		}
	}

	public synchronized void clear() {

		checked.clear();
		//logger.config("[SchemaVal] clearing schema validator checked.size()=" + checked.size());
	}

	private void createColumn(Connection conn, String tableName, Field field) throws SQLException {

		//logger.config("[SchemaVal] create column:" + field);

		String defaultValue = null;
		Default d = field.getAnnotation(Default.class);
		if (d != null)
			defaultValue = d.value();

		String columnName = field.getName().toLowerCase();
		Statement stmt = null;
		PreparedStatement stmt2 = null;

		try {

			stmt = conn.createStatement();
			stmt.execute("ALTER TABLE `" + tableName + "` ADD `" + columnName + "` " + getColumnType(field));

			if (defaultValue != null) {

				String string = "UPDATE `" + tableName + "` SET `" + columnName + "`=?";

				stmt2 = conn.prepareStatement(string);
				stmt2.setString(1, defaultValue);

				stmt2.execute();

			}

		} catch (SQLException e) {
			throw new SQLException("Unable to create column " + columnName + " in table " + tableName + "\nSQL=" + stmt + "\n" + e);

		} finally {

			if (stmt != null)
				try {
					stmt.close();

				} catch (SQLException e) {
					e.printStackTrace();
				}

			if (stmt2 != null)
				try {
					stmt2.close();

				} catch (SQLException e) {
					e.printStackTrace();
				}

		}
	}

	private void createIndex(Connection conn, Field field, String tableName, String fieldName, String indexName, Index index) throws SQLException {

		//logger.config("[SchemaVal] creating index=" + index);
		StringBuilder sb = new StringBuilder();
		Statement stmt = null;

		try {

			sb = new StringBuilder("CREATE ");
			sb.append(" INDEX ");
			sb.append(indexName);
			sb.append(" ON ");
			sb.append("`");
			sb.append(tableName);
			sb.append("` (");
			sb.append(fieldName);

			// if this field is text then limit to 20 characters
			if (field.getType().equals(String.class)) {
				sb.append("(20)");
			}

			sb.append(")");

			stmt = conn.createStatement();
			stmt.execute(sb.toString());

		} catch (SQLException e) {
			throw new SQLException("Unable to create index on  " + fieldName + " in table " + tableName + "\nIndex=" + index + "\nSQL=" + sb.toString());

		} finally {

			if (stmt != null)
				try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
	}

	private void createTable(Connection conn, Class<? extends EntityObject> clazz, String tableName) throws SQLException {

		//logger.config("[SchemaVal] create table: " + tableName);

		StringBuilder sb = new StringBuilder("CREATE TABLE `");
		sb.append(tableName);
		sb.append("`(id INT UNSIGNED NOT NULL AUTO_INCREMENT,");

		for (Field field : EntityObject.getPersistableFields(clazz)) {

			sb.append("`");
			sb.append(field.getName().toLowerCase());
			sb.append("` ");
			sb.append(getColumnType(field));
			sb.append(", ");

		}

		sb.append("PRIMARY KEY(id), UNIQUE(id), INDEX(id)) ");
        sb.append("ENGINE= MyISAM DEFAULT CHARSET= latin1");

		//logger.finest("[create table] " + sb);

		Statement stmt = null;

		try {

			stmt = conn.createStatement();
			stmt.execute(sb.toString());

		} catch (SQLException e) {
			throw new SQLException("Unable to create table: " + tableName + "\n" + e);

		} finally {

			if (stmt != null)
				try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
	}

	private List<String> getColumns(Connection conn, String tableName) throws SQLException {

		List<String> list = new ArrayList<String>();
		Statement stmt = null;
		ResultSet rs = null;

		try {

			stmt = conn.createStatement();
			rs = stmt.executeQuery("DESCRIBE `" + tableName + "`");
			while (rs.next()) {
				list.add(rs.getString(1).toLowerCase());
			}

		} catch (SQLException e) {
			throw e;

		} finally {

			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}

			if (stmt != null)
				try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}

		}

		return list;
	}

	private Object getColumnType(Field field) {

		Class type = field.getType();

		if (Boolean.class.equals(type) || Boolean.TYPE.equals(type))
			return "TINYINT(1) UNSIGNED DEFAULT '0' NOT NULL";

		else if (Amount.class.equals(type))
			return "TEXT";

		else if (Byte.class.equals(type) || Byte.TYPE.equals(type))
			return "TINYINT DEFAULT '0' NOT NULL";

		else if (Collection.class.isAssignableFrom(type))
			return "TEXT";

		else if (Country.class.equals(type))
			return "VARCHAR(3)";

		else if (Date.class.equals(type))
			return "BIGINT";

		else if (DateTime.class.equals(type))
			return "BIGINT";

		else if (Double.class.equals(type) || Double.TYPE.equals(type))
			return "DOUBLE DEFAULT '0' NOT NULL";

		else if (Enum.class.isAssignableFrom(type))
			return "VARCHAR(64)";

		else if (Integer.class.equals(type) || Integer.TYPE.equals(type))
			return "INT DEFAULT '0' NOT NULL";

		else if (EntityObject.class.isAssignableFrom(type))
			return "INT UNSIGNED DEFAULT '0' NOT NULL";

		else if (Float.class.equals(type) || Float.TYPE.equals(type))
			return "FLOAT DEFAULT '0' NOT NULL";

		else if (Map.class.isAssignableFrom(type))
			return "TEXT";

		else if (Money.class.equals(type))
			return "BIGINT DEFAULT '0' NOT NULL";

		else if (Long.class.equals(type) || Long.TYPE.equals(type))
			return "BIGINT DEFAULT '0' NOT NULL";

		else if (Period.class.equals(type))
			return "VARCHAR(12)";

		else if (Short.class.equals(type) || Short.TYPE.equals(type))
			return "SMALLINT DEFAULT '0' NOT NULL";

		else if (String.class.equals(type))
			return "TEXT";

		else if (Time.class.equals(type))
			return "BIGINT DEFAULT '0' NOT NULL";

		throw new RuntimeException("Unsupported field type: " + type + " in class " + field + " " + field.getDeclaringClass());
	}

	private List<String> getIndexes(Connection conn, String tableName) throws SQLException {

		List<String> list = new ArrayList<String>();
		Statement stmt = null;
		ResultSet rs = null;

		try {

			stmt = conn.createStatement();
			rs = stmt.executeQuery("SHOW INDEX FROM `" + tableName + "`");
			while (rs.next()) {
				list.add(rs.getString("Key_name").toLowerCase());
			}

		} catch (RuntimeException e) {
			throw e;

		} catch (SQLException e) {
			throw e;

		} finally {

			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}

			if (stmt != null)
				try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}

		}

		return list;

	}

	private List<String> getTables(Connection conn) throws SQLException {

		List<String> list = new ArrayList<String>();
		Statement stmt = null;
		ResultSet rs = null;

		try {

			stmt = conn.createStatement();
			rs = stmt.executeQuery(" SHOW TABLES ");
			while (rs.next()) {
				list.add(rs.getString(1).toLowerCase());
			}

		} catch (SQLException e) {
			throw e;

		} finally {

			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}

			if (stmt != null)
				try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}

		}

		return list;
	}

	public synchronized void validate(Class<? extends EntityObject> clazz, RequestContext context) {

		if (checked.contains(clazz.getName())) {
			return;
		}

		//logger.config("[SchemaVal] Validating class: " + clazz);

		Connection conn = null;

		try {

			conn = ds.getConnection();
			if (conn == null)
				throw new RuntimeException("No connection received from datasource=" + ds);

			List<String> tables = getTables(conn);
			//logger.config("[SchemaVal] tables=" + tables);

			// get table name
			String tableName = EntityObject.getTableName(clazz);
			//logger.config("[SchemaVal] tableName=" + tableName);

			// check to see if we need to create the table
			if (!tables.contains(tableName)) {
				createTable(conn, clazz, tableName);
			}

			// get all current column names
			List columns = getColumns(conn, tableName);
			//logger.config("[SchemaVal] columns=" + columns);

			checkFields(clazz, conn, tableName, columns);

			checked.add(clazz.getName());

			// schema init
			long time = System.currentTimeMillis();

			Constructor<? extends EntityObject> c = clazz.getDeclaredConstructor(RequestContext.class);
			c.setAccessible(true);

			EntityObject obj = c.newInstance(new Object[] { context });
			obj.schemaInit(context);

			time = System.currentTimeMillis() - time;
			//logger.config("[SchemaVal] Schema init for " + clazz + " took: " + time);

		} catch (InstantiationException e) {
			throw new RuntimeException("Error instantiating class: " + clazz);

		} catch (SQLException e) {
			//logger.config("[SchemaVal] " + e);
			throw new RuntimeException(e);

		} catch (IllegalArgumentException e) {
			throw new RuntimeException(clazz.getName() + " " + e);

		} catch (IllegalAccessException e) {
			throw new RuntimeException(clazz.getName() + " " + e);

		} catch (InvocationTargetException e) {
			throw new RuntimeException(clazz.getName() + " " + e);

		} catch (SecurityException e) {
			throw new RuntimeException(clazz.getName() + " " + e);

		} catch (NoSuchMethodException e) {
			throw new RuntimeException(clazz.getName() + " " + e);

		} finally {

			try {
				if (conn != null)
					conn.close();

			} catch (SQLException e) {
				e.printStackTrace();

			}
		}

	}

}