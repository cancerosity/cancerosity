package org.sevensoft.jeezy.db;

import java.util.List;

import org.sevensoft.commons.skint.Money;
import org.sevensoft.jeezy.db.positionable.Positionable;
import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 14 Aug 2006 17:07:48
 *
 */
public class SimpleQuery {

	public static <E extends EntityObject> int count(RequestContext context, Class<E> clazz) {
		return new Query(context, "select count(*) from #").setTable(clazz).getInt();
	}

	public static <E extends EntityObject> int count(RequestContext context, Class<E> clazz, Object... params) {

		if (params.length % 2 == 1) {
			throw new RuntimeException("Unequal number of params: " + params);
		}

		StringQueryBuilder b = new StringQueryBuilder(context);
		b.append("select count(*) from # where ");
		b.addTable(clazz);

		for (int n = 0; n < params.length; n = n + 2) {
			if (params[n] != null && params[n + 1] != null) {
				if (n > 0) {
					b.append(" and ");
				}
				b.append("`" + params[n] + "`=?", params[n + 1]);
			}
		}

		return b.toQuery().getInt();
	}

	public static void delete(RequestContext context, Class<? extends EntityObject> clazz) {
		new Query(context, "truncate table `#`").setTable(clazz).run();
	}

	public static void delete(RequestContext context, Class<? extends EntityObject> clazz, Object... params) {

		if (params.length % 2 == 1) {
			throw new RuntimeException("Unequal number of params: " + params);
		}

		StringQueryBuilder b = new StringQueryBuilder(context);
		b.append("delete from # where ");
		b.addTable(clazz);

		for (int n = 0; n < params.length; n = n + 2) {
			if (params[n] != null && params[n + 1] != null) {
				if (n > 0) {
					b.append(" and ");
				}
				b.append("`" + params[n] + "`=?", params[n + 1]);
			}
		}

		b.toQuery().run();
	}

	public static void empty(RequestContext context, Class<? extends EntityObject> clazz) {
		delete(context, clazz);
	}

	public static void emptyDb(RequestContext context) {

		Query q = new Query(context, "show tables");
		List<String> tables = q.getStrings();

		for (String string : tables) {
			new Query(context, "drop table `" + string + "`; ").run();
		}

		context.getSchemaValidator().clear();
		context.clearCache();

		EntityObject.clearFieldsCache();
	}

	public static <E extends EntityObject> List<E> execute(RequestContext context, Class<E> clazz) {

		Query q;
		if (Positionable.class.isAssignableFrom(clazz)) {
			q = new Query(context, "select * from # order by position ");
		} else {
			q = new Query(context, "select * from # order by id ");
		}

		q.setTable(clazz);
		return q.execute(clazz);
	}

	public static <E extends EntityObject> List<E> execute(RequestContext context, Class<E> clazz, Object... params) {

		if (params.length % 2 == 1) {
			throw new RuntimeException("Unequal number of params: " + params);
		}

		QueryBuilder b = new QueryBuilder(context);
		b.select("*");
		b.from("#", clazz);

		for (int n = 0; n < params.length; n = n + 2) {
			if (params[n] != null && params[n + 1] != null) {
				b.clause("`" + params[n] + "`=?", params[n + 1]);
			}
		}

		if (Positionable.class.isAssignableFrom(clazz)) {
			b.order(" position ");
		}

		return b.toQuery().execute(clazz);
	}

	public static <E extends EntityObject> List<E> execute(RequestContext context, Class<E> clazz, String order) {
		Query q = new Query(context, "select * from # order by " + order);
		q.setTable(clazz);
		return q.execute(clazz);
	}

	public static <E extends EntityObject> boolean exists(RequestContext context, Class<E> clazz, Object... params) {

		QueryBuilder b = new QueryBuilder(context);
		b.select("count(*)");
		b.from("#", clazz);

		if (params.length % 2 == 1) {
			throw new RuntimeException("Unequal number of params: " + params);
		}

		for (int n = 0; n < params.length; n = n + 2) {
			if (params[n] != null && params[n + 1] != null) {
				b.clause(params[n] + "=?", params[n + 1]);
			}
		}

		return b.toQuery().getInt() > 0;
	}

	public static <E extends EntityObject> E get(RequestContext context, Class<E> clazz, Object... params) {

		if (params.length % 2 == 1) {
			throw new RuntimeException("Unequal number of params: " + params);
		}

		QueryBuilder b = new QueryBuilder(context);
		b.select("*");
		b.from("#", clazz);

		for (int n = 0; n < params.length; n = n + 2) {
			if (params[n] != null && params[n + 1] != null) {
				b.clause("`" + params[n] + "`=?", params[n + 1]);
			}
		}

		return b.toQuery().get(clazz);
	}

	public static <E extends EntityObject> Money getMoney(RequestContext context, Class<E> clazz, String field, Object... params) {

		QueryBuilder b = new QueryBuilder(context);
		b.select("select " + field);
		b.from("#", clazz);

		if (params.length % 2 == 1) {
			throw new RuntimeException("Unequal number of params: " + params);
		}

		for (int n = 0; n < params.length; n = n + 2) {
			if (params[n] != null && params[n + 1] != null) {
				b.clause(params[n] + "=?", params[n + 1]);
			}
		}

		return b.toQuery().getMoney();
	}

}
