package org.sevensoft.jeezy.db.exceptions;

/**
 * @author sks 09-Jul-2004 23:41:36
 */
public class SchemaValidationException extends RuntimeException {

	/**
	 *  
	 */
	public SchemaValidationException() {
		super();
	}

	/**
	 * @param message
	 */
	public SchemaValidationException(String message) {
		super(message);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public SchemaValidationException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param cause
	 */
	public SchemaValidationException(Throwable cause) {
		super(cause);
	}

}