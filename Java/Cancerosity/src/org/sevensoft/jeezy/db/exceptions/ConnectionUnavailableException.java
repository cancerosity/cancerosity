package org.sevensoft.jeezy.db.exceptions;


/**
 * @author sks
 * 09-Jul-2004 22:56:23
 */
public class ConnectionUnavailableException extends RuntimeException {

	/**
	 * 
	 */
	public ConnectionUnavailableException() {
		super();
	}

	/**
	 * @param message
	 */
	public ConnectionUnavailableException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public ConnectionUnavailableException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public ConnectionUnavailableException(String message, Throwable cause) {
		super(message, cause);
	}

}
