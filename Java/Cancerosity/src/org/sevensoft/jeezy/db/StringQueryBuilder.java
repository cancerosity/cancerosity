package org.sevensoft.jeezy.db;

import java.util.ArrayList;
import java.util.List;

import org.sevensoft.jeezy.http.RequestContext;

/**
 * @author sks 31 Aug 2006 23:16:14
 *
 */
public class StringQueryBuilder {

	private List<Object>					params;
	private List<Class<? extends EntityObject>>	tables;
	private StringBuilder					sb;
	private final RequestContext				context;
	private boolean						debug;

	public StringQueryBuilder(RequestContext context) {
		this.context = context;
		params = new ArrayList();
		tables = new ArrayList();
		sb = new StringBuilder();
	}

	public void addParameter(Object p) {
		params.add(p);
	}

	public void addTable(Class<? extends EntityObject> c) {
		tables.add(c);
	}

	public void append(String string) {
		sb.append(string);
	}

	public void append(String string, Object... objs) {
		sb.append(string);
		for (Object obj : objs)
			params.add(obj);
	}

	public void debug() {
		this.debug = true;
	}

	public Query toQuery() {

		Query q = new Query(context, sb);

		for (Class table : tables) {
			q.setTable(table);
		}

		for (Object o : params) {
			q.setParam(o);
		}

		if (debug) {
			q.setDebug();
		}

		return q;
	}

	@Override
	public String toString() {
		return sb.toString();
	}
}
