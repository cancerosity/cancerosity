/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.socialserver.foler;

import java.io.File;

import com.socialserver.aws.util.AWSConfig;

/**
 *
 * @author Bing
 */
public class FolderManipulation {

    public String createDirectory(String baseDirectory, String anotherDirectory) {
        try {
            String OS = System.getProperty("os.name").toLowerCase();
            File userDirectory = null;
            if (OS.indexOf("win") >= 0 && !AWSConfig.USE_S3 ) {
                System.out.println("Step 1");
                userDirectory = new File(new File(baseDirectory).getCanonicalPath() + "//" + anotherDirectory);
            } else {
                System.out.println("Step 2");
                userDirectory = new File(new File(baseDirectory).getCanonicalPath().replaceAll("bin", "webapps") + "//" + anotherDirectory);
            }
            if (!userDirectory.exists()) {
                userDirectory.mkdir();
                System.out.println(userDirectory + "  directory is created");
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Folder Creation:   " + e);
        }
        return anotherDirectory;
    }
}
