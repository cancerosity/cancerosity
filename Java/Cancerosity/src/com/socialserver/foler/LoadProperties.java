/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.socialserver.foler;

/**
 *
 * @author Bing
 */
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class LoadProperties {

    public String loadPropertiesFile(String confFile) {
        Properties prop = new Properties();
        try {
            //load a properties file
            prop.load(new FileInputStream(confFile));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        //get the property value and print it out
        return prop.getProperty("BaseFolder");
    }
}
