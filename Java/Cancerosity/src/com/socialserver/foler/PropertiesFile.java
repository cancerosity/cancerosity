/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.socialserver.foler;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

/**
 *
 * @author Bing.sharma
 */

//  Properties file is always used to store the configuration data or settings.
public class PropertiesFile {
    
    public void createPropFile(String servletContextPath, String confFile){
        Properties prop = new Properties();
    	try {
    		//set the properties value
    		prop.setProperty("BaseFolder", servletContextPath+"/"+"MediaFiles");
 
    		//save properties to project root folder
    		prop.store(new FileOutputStream(confFile), null);
 
    	} catch (IOException ex) {
    		ex.printStackTrace();
        }
    }

}
