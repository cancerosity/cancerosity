/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.socialserver.foler;

import java.io.File;

/**
 *
 * @author Bing
 */
public class DisplayDirectoryAndFile{ 
	 
	public void displayIt(File node){ 
		System.out.println(node.getAbsoluteFile()); 
		if(node.isDirectory()){
			String[] subNote = node.list();
			for(String filename : subNote){
				displayIt(new File(node, filename));
			}
		}
 
	}
}