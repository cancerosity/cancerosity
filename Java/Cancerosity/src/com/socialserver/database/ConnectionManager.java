package com.socialserver.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;

import org.apache.catalina.Host;
import org.apache.tomcat.jni.OS;

/**
 * 
 * This class manage the connection to database
 * 
 */
public class ConnectionManager {

	private static String mysql_host = "ec2-23-20-216-216.compute-1.amazonaws.com";
	private static String mysql_database = "socialserver";
	private static String mysql_driverUrl = "jdbc:mysql://" + mysql_host
			+ ":3306/" + mysql_database;
	private static String mysql_username = "mysqluser";
	private static String mysql_pass = "830102";

	public static void main(String[] args) {
		System.out.println(getMysqlConnection());
	}

	static {
		try {
			String OS = System.getProperty("os.name").toLowerCase();
			if (OS.indexOf("win") == -1) {
				mysql_host = "localhost";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Get mysql connection
	 * 
	 * @return
	 */
	public static Connection getMysqlConnection() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			return DriverManager.getConnection(mysql_driverUrl, mysql_username,
					mysql_pass);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	/**
	 * Release the resource hold by JVM
	 * 
	 * @param rs
	 * @param stmt
	 * @param con
	 */
	public static void freeResource(ResultSet rs, Statement stmt,
			Connection conn) {
		try {
			if (rs != null) {
				rs.close();
			}
			rs = null;
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		try {
			if (stmt != null) {
				stmt.close();
			}
			stmt = null;
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		try {
			if (conn != null) {
				conn.close();
			}
			conn = null;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	/**
	 * 
	 * @return
	 */
	public static String testConnection() {
		Connection conn = null;
		try {
			conn = getMysqlConnection();
			return "<font color=blue>Database :</font> " + mysql_host + ",<br><font color=blue>Status</font>: "
					+ ((conn != null) ?  " good " : " bad ");

		} catch (Exception ex) {
			ex.printStackTrace();
			return ex.getMessage();
		} finally {
			freeResource(null, null, conn);
		}

	}
}
