 
package com.socialserver.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * 
 * @author Bing
 * 
 */
public class SqlBeanUpdated {

	private String result;

	HashMap<String, String> profile = new LinkedHashMap<String, String>();

	/**
	 * 
	 * @param sql
	 * @return
	 * @throws SQLException
	 */
	public String getdetails(String sql) {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			conn = ConnectionManager.getMysqlConnection();
			stmt = conn.prepareStatement(sql);
			rs = stmt.executeQuery();
			String username = "";
			String picture = "";
			String user_id;
			boolean video_bool;
			boolean image_bool;
			boolean story_bool;
			String qry_video = "";

			if (rs.next()) {
				// user_id = rs.getString(1);
				username = rs.getString(1);

				video_bool = getVideo(username);
				image_bool = getImage(username);
				story_bool = getStory(username);
				if (story_bool || image_bool || video_bool) {
					try {
						map_sort();
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					result = username;
				}

			} else {
				result = ",";
			}
			// result = username+","+picture;
			setResult(result);

		} catch (Exception ex) {
			Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null,
					ex);
			ex.printStackTrace();
			return result;
		} finally {
			ConnectionManager.freeResource(rs, stmt, conn);
		}
		return result;
	}

	public void setResult(String s) {
		result = s;
	}

	public String getResult() {
		return result;
	}

	/**
	 * 
	 * @param username
	 * @return
	 * @throws SQLException
	 */
	public boolean getVideo(String username) {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		String qry_video = "select username,name_of_video,link_of_video,preview,inserted_at from video where username ='"
				+ username + "' ";

		try {

			conn = ConnectionManager.getMysqlConnection();
			stmt = conn.prepareStatement(qry_video);
			rs = stmt.executeQuery();

			String user_id_new = "";
			String name = "";
			String name_of_video = "";
			String link_of_video = "";
			String preivew = "";
			String inserted_at = "";
			String result = "";

			if (rs.next()) {
				rs.beforeFirst();
				while (rs.next()) {

					// user_id_new = local_result_video.getString(1);
					name = rs.getString(1);
					name_of_video = rs.getString(2);
					link_of_video = rs.getString(3);
					preivew = rs.getString(4);
					inserted_at = rs.getString(5);
					if (inserted_at.lastIndexOf(".") > 0) {
						inserted_at = inserted_at.substring(0,
								inserted_at.lastIndexOf("."));
					}
					// Storing each set of result in result string
					result = name + "," + name_of_video + "," + link_of_video
							+ "," + preivew + "," + inserted_at;

					// Stroring resukt string in hash map where date is key and
					// result is value
					profile.put(inserted_at, result);

				}

			} else {
				return false;
			}
		} catch (Exception ex) {
			Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null,
					ex);
			ex.printStackTrace();
			return false;
		} finally {
			ConnectionManager.freeResource(rs, stmt, conn);
		}
		return true;
	}

	/**
	 * 
	 * @param username
	 * @return
	 * @throws SQLException
	 */
	public boolean getImage(String username) {
		String qry_Image = "select username,name_of_images,link_of_images,preview,inserted_at from images where username ='"
				+ username + "' ";

		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			conn = ConnectionManager.getMysqlConnection();
			stmt = conn.prepareStatement(qry_Image);
			rs = stmt.executeQuery();

			String username_new = "";
			String name = "";
			String name_of_image = "";
			String link_of_image = "";
			String preivew = "";
			String inserted_at = "";
			String result = "";

			if (rs.next()) {
				rs.beforeFirst();
				while (rs.next()) {
					username_new = rs.getString(1);
					name_of_image = rs.getString(2);
					link_of_image = rs.getString(3);
					preivew = rs.getString(4);
					inserted_at = rs.getString(5);
					if (inserted_at.lastIndexOf(".") > 0) {
						inserted_at = inserted_at.substring(0,
								inserted_at.lastIndexOf("."));
					}
					// Storing each set of result in result string
					result = username_new + "," + name_of_image + ","
							+ link_of_image + "," + preivew + "," + inserted_at;

					// Stroring resukt string in hash map where date is key and
					// result is value
					profile.put(inserted_at, result);
				}
			} else {
				return false;
			}
		} catch (Exception ex) {
			Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null,
					ex);
			ex.printStackTrace();
			return false;
		} finally {
			ConnectionManager.freeResource(rs, stmt, conn);
		}
		return true;
	}

	/**
	 * 
	 * @param username
	 * @return
	 * @throws SQLException
	 */
	public boolean getStory(String username) {
		String qry_Story = "select username,story,inserted_at from story where username ='"
				+ username + "' order by sno desc";

		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {
			conn = ConnectionManager.getMysqlConnection();
			stmt = conn.prepareStatement(qry_Story);
			rs = stmt.executeQuery();

			String name = "";
			String story = "";
			String inserted_at = "";
			String result = "";

			if (rs.next()) {
				rs.beforeFirst();
				while (rs.next()) {

					// user_id_new = rs.getString(1);
					name = rs.getString(1);
					story = rs.getString(2);
					inserted_at = rs.getString(3);
					if (inserted_at.lastIndexOf(".") > 0) {
						inserted_at = inserted_at.substring(0,
								inserted_at.lastIndexOf("."));
					}
					// Storing each set of result in result string
					result = name + "," + story + "," + inserted_at;

					// result is value
					profile.put(inserted_at, result);
				}

			} else {
				return false;
			}
		} catch (Exception ex) {
			Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null,
					ex);
			ex.printStackTrace();
			return false;
		} finally {
			ConnectionManager.freeResource(rs, stmt, conn);
		}
		return true;

	}

	/**
	 * 
	 * @param username
	 * @return
	 * @throws SQLException
	 */
	public String getProfileData(String username) {
		String qry_profileData = "select picture,status from user where username ='"
				+ username + "'";
		String status = "", imagelink = "";
	 
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {
			conn = ConnectionManager.getMysqlConnection();
			stmt = conn.prepareStatement(qry_profileData);
			rs = stmt.executeQuery();

			if (rs.next()) {
				imagelink = rs.getString(1);
				status = rs.getString(2);
			}
		} catch (Exception ex) {
			Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null,
					ex);
			ex.printStackTrace();
		} finally {
			ConnectionManager.freeResource(rs, stmt, conn);
		}
		return imagelink + "," + status;

	}

	public void map_sort() {

		Map.Entry<String, String>[] entries = profile.entrySet().toArray(
				new Map.Entry[profile.size()]);
		Arrays.sort(entries, new Comparator<Map.Entry<String, String>>() {
			DateTimeFormatter date = DateTimeFormat
					.forPattern("yyyy-MM-dd HH:mm:ss");

			@Override
			public int compare(Map.Entry<String, String> o1,
					Map.Entry<String, String> o2) {

				return date.parseDateTime(o2.getKey()).compareTo(
						date.parseDateTime(o1.getKey()));

			}
		});

		for (Map.Entry entry : entries) {
			result = result + entry.toString() + "~";
			System.out.println(result);
		}

	}
}
