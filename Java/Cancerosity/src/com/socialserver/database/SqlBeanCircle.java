package com.socialserver.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 
 * @author Bing
 */
public class SqlBeanCircle {

	/**
	 * 
	 * @param sql
	 * @return
	 * @throws SQLException
	 */
	public String getdetails(String sql) throws SQLException {
		String result = "";
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {
			conn = ConnectionManager.getMysqlConnection();
			stmt = conn.prepareStatement(sql);
			rs = stmt.executeQuery();
			while (rs.next()) {
				result = result + rs.getString(1).toString();
				System.out.println("Result:" + result);
				result = result + "-END-";
				result = result + rs.getString(2);
				result = result + "-END-";
				result = result + rs.getString(3);
				result = result + "-END-";
				result = result + rs.getString(4);
				result = result + "-END-";
				result = result + rs.getString(5);
				result = result + "-END-";
				result = result + rs.getString(6);
				result = result + "-END-";
				result = result + rs.getString(7);
				result = result + "-END-";
				if (rs.getString(8) == null) {
					result = result + "";
				} else {
					result = result + rs.getString(8);
				}
				result = result + "-END-";
				result = result + rs.getString(9) + "-END-";
				result = result
						+ new SqlBean()
								.executeQuery("select privacy from user where username ='"
										+ rs.getString(2) + "'");
				result = result + "-END_OF_DATA-LINE-";

			}

		} catch (Exception ex) {
			Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null,
					ex);
			ex.printStackTrace();
		} finally {
			ConnectionManager.freeResource(rs, stmt, conn);
		}

		return result;
	}
}
