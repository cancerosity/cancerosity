package com.socialserver.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 
 * @author Bing
 * 
 */
public class SqlBean {

	private String result;

	/**
	 * 
	 * @param sql
	 * @return
	 */
	public int executesql(String sql) {

		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		int x = 0;
		String result = "";
		try {

			conn = ConnectionManager.getMysqlConnection();
			stmt = conn.prepareStatement(sql);

			if (sql!=null&&!sql.toLowerCase().contains("select")) {
				System.out.println("Registration:" + sql);
				x = stmt.executeUpdate(sql);
			} else {
				System.out.println(sql);
				rs = stmt.executeQuery(sql);
				// String user_id = "";
				String username = "";

				while (rs.next()) {
					// user_id = rs.getString(1);
					username = rs.getString(1);
				}
				result = username;
				setResult(result);
			}
			// System.out.println("Class Name:SqlBean and method name:executesql");
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			ConnectionManager.freeResource(rs, stmt, conn);
		}

		return x;
	}

	public String getdetails(String sql) {
		String result = "";
		try {

			SqlBeanUpdated ob = new SqlBeanUpdated();
			result = ob.getdetails(sql);

		} catch (Exception ex) {
			Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null,
					ex);
			ex.printStackTrace();
		}
		return result;
	}

	/**
	 * 
	 * @param sql
	 * @return @
	 */
	public String getdetails_register(String sql) {
		String result = "";
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {
			System.out.println("Check at the time registration:" + sql);

			conn = ConnectionManager.getMysqlConnection();
			stmt = conn.prepareStatement(sql);
			rs = stmt.executeQuery();

			String email = "";
			String username = "";

			while (rs.next()) {
				email = rs.getString(1);
				username = rs.getString(2);
			}
			result = email + "," + username;
			setResult(result);

		} catch (Exception ex) {
			Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null,
					ex);
			ex.printStackTrace();
		} finally {
			ConnectionManager.freeResource(rs, stmt, conn);
		}
		return result;
	}

	/**
	 * 
	 * @param sql
	 * @param i
	 * @return
	 * 
	 */
	public String getdetails(String sql, int i) {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {

			conn = ConnectionManager.getMysqlConnection();
			stmt = conn.prepareStatement(sql);
			rs = stmt.executeQuery();

			String[] result_set = new String[i];

			while (rs.next()) {
				result_set[0] = rs.getString(1);
				result_set[1] = rs.getString(2);
				result_set[2] = rs.getString(3);
			}
			result = result_set[0] + "," + result_set[1] + "," + result_set[2];
			setResult(result);
			// System.out.println("Class Name:SqlBean and method name:getdetails");
		} catch (Exception ex) {
			Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null,
					ex);
			ex.printStackTrace();
			return result;
		} finally {
			ConnectionManager.freeResource(rs, stmt, conn);
		}
		return result;
	}

	/**
	 * 
	 * @param sql
	 * @return
	 */
	public int content_upload(String sql) {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		int x = 0;
		try {
			System.out.println("Insert Items in DataBase:" + sql);
			conn = ConnectionManager.getMysqlConnection();
			stmt = conn.prepareStatement(sql);
			x = stmt.executeUpdate();

		} catch (Exception ex) {
			System.out.println(ex);
			Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null,
					ex);
			ex.printStackTrace();
			return 0;
		} finally {
			ConnectionManager.freeResource(rs, stmt, conn);
		}
		return x;
	}

	/**
	 * 
	 * @param sql
	 * @return
	 * 
	 */
	public int editProfile(String sql) {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		int x = 0;
		try {
			conn = ConnectionManager.getMysqlConnection();
			stmt = conn.prepareStatement(sql);
			x = stmt.executeUpdate();

			// System.out.println("Class Name:SqlBean and method name:editProfile");
		} catch (Exception ex) {
			Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null,
					ex);
			ex.printStackTrace();
			return 0;
		} finally {
			ConnectionManager.freeResource(rs, stmt, conn);
		}
		return x;
	}

	/**
	 * 
	 * @param sql
	 * @return
	 */
	public String get_story_detail(String sql) {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {

			conn = ConnectionManager.getMysqlConnection();
			stmt = conn.prepareStatement(sql);
			rs = stmt.executeQuery();

			String story = "";
			String inserted_at = "";
			String name = "";
			String location = "";
			while (rs.next()) {
				story = rs.getString(1);
				if (story == null || story.equals(""))
					story = " ";
				inserted_at = rs.getString(2);
				if (inserted_at == null || inserted_at.equals(""))
					inserted_at = "";
				name = rs.getString(3);
				if (name == null || name.equals(""))
					name = "";
				location = rs.getString(4);
				if (location == null || location.equals(""))
					location = "Location not available";
			}
			result = story + "---SeperatorInStory----" + inserted_at
					+ "---SeperatorInStory----" + name
					+ "---SeperatorInStory----" + location;
			System.out.println("Story Result:" + result);
			setResult(result);
			// System.out.println("Class Name:SqlBean and method name:get_story_detail");
		} catch (Exception ex) {
			ex.printStackTrace();
			return result;
		} finally {
			ConnectionManager.freeResource(rs, stmt, conn);
		}
		return result;
	}

	/**
	 * 
	 * @param sql
	 * @return
	 */
	public String executeQuery(String sql) {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		String count = "";
		try {
			conn = ConnectionManager.getMysqlConnection();
			stmt = conn.prepareStatement(sql);
			rs = stmt.executeQuery();

			while (rs.next()) {
				count = rs.getString(1);
			}
			// System.out.println("Class Name:SqlBean and method name:executeQuery");
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			ConnectionManager.freeResource(rs, stmt, conn);
		}
		return count;
	}

	/**
	 * 
	 * @param sql
	 * @return
	 */
	public String executeUpdate(String sql) {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		String status = "";
		try {
			conn = ConnectionManager.getMysqlConnection();
			stmt = conn.prepareStatement(sql);

			int i = stmt.executeUpdate(sql);
			if (i > 0) {
				status = "ok";
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			ConnectionManager.freeResource(rs, stmt, conn);
		}
		return status;
	}

	/**
	 * 
	 * @param sql
	 * @return
	 */
	public String searchStory(String sql) {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {

			conn = ConnectionManager.getMysqlConnection();
			stmt = conn.prepareStatement(sql);
			rs = stmt.executeQuery();

			while (rs.next()) {
				result = result + rs.getString(1).toString();
				result = result + "-END-";
				result = result + rs.getString(2);
				result = result + "-END-";
				result = result + rs.getString(3);
				result = result + "-END-";
				result = result + rs.getString(4);
				result = result + "-END-";
				result = result + rs.getString(5);
				result = result + "-END-";
				result = result + rs.getString(6);
				result = result + "-END-";
				result = result + rs.getString(7);
				result = result + "-END-";
				result = result + rs.getString(8);
				result = result + "-END_OF_DATA-LINE-";
			}
			// System.out.println("Class Name:SqlBean and method name:searchStory");
		} catch (Exception ex) {

			ex.printStackTrace();
		} finally {
			ConnectionManager.freeResource(rs, stmt, conn);
		}
		return result;
	}

	/**
	 * 
	 * @param sql
	 * @return
	 */
	public String showComment(String sql) {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		String result = "";
		try {

			conn = ConnectionManager.getMysqlConnection();
			stmt = conn.prepareStatement(sql);
			rs = stmt.executeQuery();

			while (rs.next()) {

				result = result + rs.getString(1) + "-SEPARATOR-";
				result = result + rs.getString(2) + "-SEPARATOR-";
				result = result + rs.getString(3) + "-End_of_comment-";
			}
			// System.out.println("Class Name:SqlBean and method name:showComment");
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			ConnectionManager.freeResource(rs, stmt, conn);
		}
		return result;
	}

	/**
	 * 
	 * @param sql
	 * @return
	 */
	public String searchUser(String sql) {

		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {

			conn = ConnectionManager.getMysqlConnection();
			stmt = conn.prepareStatement(sql);
			rs = stmt.executeQuery();
			while (rs.next()) {
				result = result + rs.getString(1).toString();
				result = result + ",";
				result = result + rs.getString(2);
				result = result + ",";
				result = result + rs.getString(3);
				result = result + ",";
				result = result + rs.getString(4);
				result = result + ",";
				result = result + rs.getString(5);
				result = result + ",";
				result = result + rs.getString(6);
				result = result + ",";
				result = result + rs.getString(7);
				result = result + "-END_OF_DATA-LINE-";
			}
			// System.out.println("Class Name:SqlBean and method name:searchUser");
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			ConnectionManager.freeResource(rs, stmt, conn);
		}

		return result;
	}

	/**
	 * 
	 * @param sql
	 * @return
	 */
	public String showUserDetails(String sql) {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {
			conn = ConnectionManager.getMysqlConnection();
			stmt = conn.prepareStatement(sql);
			rs = stmt.executeQuery();

			if (rs.next()) {
				result = result + rs.getString(1).toString();
				result = result + "-END_OF_DATA-LINE-";
				result = result + rs.getString(2);
				result = result + "-END_OF_DATA-LINE-";
				result = result + (rs.getString(3) + " " + rs.getString(4));
				result = result + "-END_OF_DATA-LINE-";
				result = result + rs.getString(5);
				result = result + "-END_OF_DATA-LINE-";
				result = result + rs.getString(6);
				result = result + "-END_OF_DATA-LINE-";
				result = result + rs.getString(7);
				result = result + "-END_OF_DATA-LINE-";
				result = result + rs.getString(8);
				result = result + "-END_OF_DATA-LINE-";
				result = result + rs.getString(9);
				result = result + "-END_OF_DATA-LINE-";
				result = result + rs.getString(10);
				result = result + "-END_OF_DATA-LINE-";
				result = result + rs.getString(11);
				result = result + "-END_OF_DATA-LINE-";
				result = result + rs.getString(12);
				result = result + "-END_OF_DATA-LINE-";
				result = result + rs.getString(13);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			ConnectionManager.freeResource(rs, stmt, conn);
		}
		return result;
	}

	/**
	 * 
	 * @param sql
	 * @return
	 */
	public String followingUser(String sql) {

		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		String follName = "";
		try {

			conn = ConnectionManager.getMysqlConnection();
			stmt = conn.prepareStatement(sql);
			rs = stmt.executeQuery();

			while (rs.next()) {
				follName = follName + rs.getString(1) + ",";
			}
			System.out.println("Class Name:SqlBean and method name:searchUser");
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			ConnectionManager.freeResource(rs, stmt, conn);
		}
		return follName;
	}

	public void setResult(String s) {

		result = s;
	}

	public String getResult() {
		return result;
	}

	public void close() {
		// DO nothing

	}

	/**
	 * 
	 * @param sql
	 * @return
	 */
	public String userInfoDetail(String sql) {

		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {

			conn = ConnectionManager.getMysqlConnection();
			stmt = conn.prepareStatement(sql);
			rs = stmt.executeQuery();

			if (rs.next()) {
				result = result + rs.getString(1).toString();
				result = result + "-END_OF_DATA-LINE-";
				result = result + rs.getString(2);
				result = result + "-END_OF_DATA-LINE-";
				result = result + rs.getString(3);
				result = result + "-END_OF_DATA-LINE-";
				result = result + rs.getString(4);
				result = result + "-END_OF_DATA-LINE-";
				result = result + rs.getString(5);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			ConnectionManager.freeResource(rs, stmt, conn);
		}
		return result;
	}

	public String followRequestName(String sql) {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		String requestName = "";
		try {

			conn = ConnectionManager.getMysqlConnection();
			stmt = conn.prepareStatement(sql);
			rs = stmt.executeQuery();

			if (rs.next()) {
				requestName = requestName + rs.getString(1).toString();
				requestName = requestName + "-END_OF_DATA-LINE-";
				requestName = requestName + rs.getString(2);
				requestName = requestName + "-END_OF_DATA-LINE-";
				requestName = requestName + rs.getString(3);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			ConnectionManager.freeResource(rs, stmt, conn);
		}
		return requestName;
	}

	/**
	 * 
	 * @param sql
	 * @return
	 */
	public String showLatestComment(String sql) {
		String comment = "";
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {

			conn = ConnectionManager.getMysqlConnection();
			stmt = conn.prepareStatement(sql);
			rs = stmt.executeQuery();

			while (rs.next()) {
				comment = comment + "@";
				comment = comment + rs.getString(1).toString();
				comment = comment + " ";
				comment = comment + rs.getString(2);
				comment = comment + "-END_OF_DATA-LINE-";

			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			ConnectionManager.freeResource(rs, stmt, conn);
		}
		return comment;
	}

	/**
	 * 
	 * @param sql
	 * @return
	 */
	public String showNews(String sql) {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		String news = "";
		try {
			conn = ConnectionManager.getMysqlConnection();
			stmt = conn.prepareStatement(sql);
			rs = stmt.executeQuery();

			while (rs.next()) {
				news = news + rs.getString(1).toString();
				news = news + "-END-";
				news = news + rs.getString(2);
				news = news + "-END-";
				news = news + rs.getString(3);
				news = news + "-END-";
				news = news + rs.getString(4);
				news = news + "-END-";
				news = news + rs.getString(5);
				news = news + "-END_OF_DATA-LINE-";
			}
			// System.out.println("Class Name:SqlBean and method name:searchStory");
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			ConnectionManager.freeResource(rs, stmt, conn);
		}
		return news;
	}

	/**
	 * 
	 * @param sql
	 * @return
	 */
	public String newsEmailNotification(String sql) {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		String news = "";
		try {
			conn = ConnectionManager.getMysqlConnection();
			stmt = conn.prepareStatement(sql);
			rs = stmt.executeQuery();

			while (rs.next()) {
				news = news + rs.getString(1).toString();
				news = news + "-END-";
				news = news + rs.getString(2);
				news = news + "-END-";
				news = news + rs.getString(3);
				news = news + "-END-";
				news = news + rs.getString(4);
				news = news + "-END-";
				news = news + rs.getString(5);
				news = news + "-END_OF_DATA-LINE-";
			}
			// System.out.println("Class Name:SqlBean and method name:searchStory");
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			ConnectionManager.freeResource(rs, stmt, conn);
		}
		return news;
	}

	/**
	 * 
	 * @param sql
	 * @return
	 */
	public String followingName(String sql) {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		String follow = "";
		try {
			conn = ConnectionManager.getMysqlConnection();
			stmt = conn.prepareStatement(sql);
			rs = stmt.executeQuery();

			while (rs.next()) {
				follow = follow + rs.getString(1).toString();
				follow = follow + "-END-";
				follow = follow + rs.getString(2);
				follow = follow + "-END-";
				follow = follow + rs.getString(3);
				follow = follow + "-END_OF_DATA-LINE-";
			}
			// System.out.println("Class Name:SqlBean and method name:searchStory");
		} catch (Exception ex) {
			ex.printStackTrace();

		}

		finally {
			ConnectionManager.freeResource(rs, stmt, conn);
		}
		return follow;
	}

	/**
	 * 
	 * @param sql
	 * @return
	 */
	public String showFbPosting(String sql) {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		 
		String result = "";
		try {

			conn = ConnectionManager.getMysqlConnection();
			stmt = conn.prepareStatement(sql);
			rs = stmt.executeQuery();

			if (rs.next()) {
				result = result + rs.getString(1).toString();
				result = result + "-END-";
				result = result + rs.getString(2);
			}
			// System.out.println("Class Name:SqlBean and method name:searchStory");
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			ConnectionManager.freeResource(rs, stmt, conn);
		}
		return result;
	}
}
