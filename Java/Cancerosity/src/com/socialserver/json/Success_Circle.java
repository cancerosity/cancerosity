/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.socialserver.json;

import java.io.StringWriter;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.socialserver.controller.DateTimeComparison;
import com.socialserver.database.SqlBean;
import com.socialserver.database.SqlBeanUpdated;

/**
 *
 * @author Bing
 */
public class Success_Circle {

    public Object jsonPrint(String value) {
        /*
         Stories:
         -Message
         -Like
         Persons(comma seperated)
         -Post Time
         -Comments
         Persons
         -Message
         -Time
         -Likes
         -Persons(comma seperated)
         */
        System.out.println("Success_Circleeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee:" + value);
        JSONObject obj = new JSONObject();
        DateTimeComparison dateobj = new DateTimeComparison();

        // HttpSession session=request.getSession();
        // RegistrationFormResultBean result_bean=(RegistrationFormResultBean)session.getAttribute("result");
        //String value = result_bean.getValue();
        String sno = "", username = "", content = "", inserted_at = "", like = "", dislike = "", share = "", comment = "", profile_image = "";
        String likename = "", qry = "", likeprint = "", dislikeprint = "", dislikename = "",content_type="";
        String type = "", commentby = "", comment_value = "", followingNames = "", followerNames = "", strqry1 = "", strqry2 = "";
        int Counter = 0, followingCount = 0, followerCount = 0;
        String elapsed = "",location="",commentData="",comment0="",comment1="",comment2="";
        String[] profileData = null;

        // String [] retvalue = value.matches();
        if (value.indexOf("-END_OF_DATA-LINE-") < 1) {
            String[] retvalue = value.split(",");
            //id = retvalue[0];
            username = retvalue[0];
            //id = retvalue[2];

        } else {
            String[] retvalue = value.split("-END_OF_DATA-LINE-");
            String strValue = "";
            String[] innerValues;
            String[] commentDataValue;
            Vector<String> list = new Vector<String>();
            for (int i = 0; i < retvalue.length; i++) {
                list.add(retvalue[i]);
            }
            Iterator itr = list.iterator();
            while (itr.hasNext()) {
                elapsed = "";
                strValue = (String) itr.next();//strValue contains the id,name,filename,url, urlpre,time for 
                JSONObject objImg = new JSONObject();
                
                innerValues = strValue.split("-END-");
                System.out.println("((((((((((((((((((((((((((((((((((((((((((((((((((((((("+innerValues.length);
                //JSONArray objlist = new JSONArray();
                sno = innerValues[0];
                username = innerValues[1];
                content = innerValues[2];
                inserted_at = innerValues[3];
                String arr[] = innerValues[4].split("_");
                String arr1[] = innerValues[5].split("_");
                JSONObject cmntobj = new JSONObject();
                try {

                    elapsed = dateobj.showtime(inserted_at);
                    likeprint = "";
                    dislikeprint = "";
                    //String cmnt_arr[] = innerValues[7].split("_");

                    SqlBean db = new SqlBean();
                    for (int i = 1; i < arr.length; i++) {
                        qry = "select username from user where sno = '" + arr[i] + "'";
                        likename = db.executeQuery(qry);
                        likeprint = likeprint + "," + likename;
                    }
                    for (int j = 1; j < arr1.length; j++) {
                        qry = "select username from user where sno = '" + arr1[j] + "'";
                        dislikename = db.executeQuery(qry);
                        dislikeprint = dislikeprint + "," + dislikename;
                    }

                    String file = content.substring(content.lastIndexOf('.') + 1);
                    if ((file.equals("png")) || (file.equals("jpg")) || (file.equals("jpeg")) || (file.equals("gif")) || (file.equals("webp")) || (file.equals("bmp"))) {
                        type = "images";
                        content_type="image";
                    } else if ((file.equals("mov")) || (file.equals("mp4")) || (file.equals("m4v")) || (file.equals("3gp")) || (file.equals("m4a")) || (file.equals("aac")) || (file.equals("webm")) || (file.equals("mkv"))) {
                        type = "video";
                        content_type="video";
                    } else {
                        type = "story";
                        content_type="story";
                    }
                    qry = "select count(*) from comment,user where comment.altsno = '" + sno + "' and comment.type= '" + type + "' and user.username = comment.username order by inserted_at";
                    comment_value = db.executeQuery(qry);
                    commentData="";
                    String comment_qry="select comment.username,comment.comment from comment,user where comment.altsno = '" + sno + "' and comment.type= '" + type + "' and user.username = comment.username order by inserted_at desc limit 4";
                    System.out.println("comment_qry"+comment_qry);
                    commentData=db.showLatestComment(comment_qry);
                    commentDataValue=commentData.split("-END_OF_DATA-LINE-");
                    
                    
                    for(int j=0;j<commentDataValue.length;j++){
                        
                        cmntobj.put(("comment" + j).toString(),commentDataValue[j]);
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
                like = Integer.toString((arr.length) - 1);

                dislike = Integer.toString((arr1.length) - 1);
                //share=innerValues[6];
                location = innerValues[7];
                profile_image = innerValues[8];
                /*String commentArr[] =  comment_value.split("-End_of_comment-");
                 JSONArray listComment_value = new JSONArray();
                 for(int var=0;var<commentArr.length;var++){
                 listComment_value.add(commentArr[var]);
                 }*/



                objImg.put("sno", sno);
                objImg.put("username", username);
                objImg.put("content", content);
                objImg.put("comment", comment_value);
                objImg.put("inserted_at", inserted_at);
                
                objImg.put("Elapsed", elapsed);
                objImg.put("location", location);
                objImg.put("like", like);
                objImg.put("likenames", likeprint);
                objImg.put("dislike", dislike);
                objImg.put("dislikenames", dislikeprint);
                //objImg.put("share", share);
                objImg.put("profile_image", profile_image);
                objImg.put("commentval", cmntobj);
                objImg.put("commentiphone", commentData);
                objImg.put("type", content_type);
                objImg.put("privacy", innerValues[9]);
                System.out.println("**********************privacy ************************"+ innerValues[9]);
                obj.put(("data" + Counter++).toString(), objImg);
            }
        }
        try {
            SqlBean db = new SqlBean();
            strqry1 = "select following from follow where username ='" + username + "'";
            followingNames = db.followingUser(strqry1);
            if (!followingNames.equals("")) {
                String arr2[] = followingNames.split(",");
                followingCount = (arr2.length);
            }
            strqry2 = "select username from follow where following ='" + username + "'";
            followerNames = db.followingUser(strqry2);
            if (!followerNames.equals("")) {
                String[] arr3;
                arr3 = followerNames.split(",");
                followerCount = (arr3.length);
            }
        } catch (Exception ex) {
            Logger.getLogger(Success_json.class.getName()).log(Level.SEVERE, null, ex);
        }
        obj.put("followingCount", followingCount);
        obj.put("followingNames", followingNames);
        obj.put("followerCount", followerCount);
        obj.put("followerNames", followerNames);
        obj.put("param", "True");
        return obj;
    }
}
