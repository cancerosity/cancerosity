/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.socialserver.json;

import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.simple.JSONObject;

import com.socialserver.database.SqlBean;

/**
 *
 * @author Bing
 */
public class Recent_News {

    public Object jsonPrint(String value) {
        String sno = "", username = "", name = "", dob = "", email_id = "", phone = "", status = "", profile_image = "";
        String action="",type="",message="",picAction="",content_name="",content_type="";
        String[] follow_request;

        int Counter = 0;
        
        JSONObject obj = new JSONObject();
        if (value.indexOf("-END_OF_DATA-LINE-") < 1) {
            obj.put("param", "false");
        } else {
            //JSONArray ar = new JSONArray();
            String[] retvalue = value.split("-END_OF_DATA-LINE-");
            String[] innerValues;
            String strValue = "";
             Vector<String> list = new Vector<String>();
            for (int i = 0; i < retvalue.length; i++) {
                list.add(retvalue[i]);
            }
            Iterator itr = list.iterator();
            while (itr.hasNext()) {
                message="";
                picAction="";
                 strValue = (String) itr.next();
                JSONObject objImg = new JSONObject();
                innerValues = strValue.split("-END-");
                username = innerValues[0];
                action = innerValues[1];
                type = innerValues[2];
                content_name = innerValues[3];
                sno = innerValues[4];
                if(type.equals("images")||type.equals("video")||type.equals("story")){
                    if(action.equals("liked")){
                        picAction="likes";
                    }
                    else{
                        picAction="dislikes";
                    }
                    if(type.equals("images")){
                        message=username+" "+picAction+" your photo";
                    }
                    else{
                        message=username+" "+picAction+" your "+type;
                    }
                    
                }
                else if(type.equals("commentimages")||type.equals("commentvideo")||type.equals("commentstory")){
                    if(type.equals("commentimages")){
                        message=username+" commented \'"+action+"\'"+" on your photo";
                    }
                    else{
                        message=username+" commented \'"+action+"\'"+" on your "+(type.substring(7));
                    }                   
                }
                else{
                    message=username+" is following you";
                }
                if(type.contains("comment")){
                    content_type=type.substring(7);
                }
                else{
                    content_type = type;
                }
                try{
                    SqlBean db =new SqlBean();
                    String qry="select picture from user where username='"+username+"'";
                    profile_image=db.executeQuery(qry);
                }
                catch(Exception e){
                    System.out.println(e);
                }
                /*objImg.put("username", username);
                objImg.put("action", action);*/
                objImg.put("type", content_type);
                objImg.put("profile_image", profile_image);
                objImg.put("message", message);
                objImg.put("sno", sno);
                obj.put(("data" + Counter++).toString(), objImg);
            }
            obj.put("param", "True");
        }
        return obj;

    }
}
