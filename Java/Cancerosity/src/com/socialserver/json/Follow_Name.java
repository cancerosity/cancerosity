/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.socialserver.json;

import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONObject;

/**
 *
 * @author Bing
 */
public class Follow_Name {

    public Object jsonPrint(String value) {
        String following = "", picture = "", status = "", followers = "",state="";
        int Counter = 0, count = 0;
        String[] innerValues;
        String[] innerValues1;
        JSONObject obj = new JSONObject();

        if (value.indexOf("-END_OF_DATA-LINE-") < 1) {
            obj.put("param", "False");
        } else {
            String[] retValue = value.split("-END_OF_DATA-LINE-");

            String strValue = "";
            Vector<String> list = new Vector<String>();
            for (int i = 0; i < retValue.length; i++) {
                list.add(retValue[i]);
            }
            Iterator itr = list.iterator();
            while (itr.hasNext()) {
                strValue = (String) itr.next();
                JSONObject objImg = new JSONObject();
                innerValues = strValue.split("-END-");
                following = innerValues[0];
                state = innerValues[1];
                picture = innerValues[2];
                status = innerValues[3];
                objImg.put("Name", following);
                objImg.put("state", state);
                objImg.put("picture", picture);
                objImg.put("status", status);
                obj.put(("data" + Counter++).toString(), objImg);
            }
            obj.put("param", "True");
        }
        
        return obj;
    }
}
