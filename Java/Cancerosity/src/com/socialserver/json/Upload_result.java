/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.socialserver.json;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.socialserver.beans.*;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
public class Upload_result {
    
    public Object jsonPrint(String message){
        System.out.println("editProfile Json:"+message);
        String[] retvalue = message.split("--endOfData--");
        JSONObject obj = new JSONObject();
        if(retvalue.length==1){
            obj.put("name", "");
            obj.put("link", "");
            obj.put("preview", "");
            obj.put("timestamp", "");
            obj.put("message", retvalue[0]);
        }else{
            obj.put("name", retvalue[0]);
            obj.put("link", retvalue[1]);
            obj.put("preview", retvalue[2]);
            obj.put("timestamp", retvalue[3]);
            obj.put("message", "success");   
            obj.put("location",retvalue[4]);
            obj.put("elapsed",retvalue[5]);
            obj.put("sno",retvalue[6]);
        }
    System.out.println(obj);
        return obj;
    }
    public Object jsonEditProfile(String message){
        System.out.println("editProfile Json:"+message);
        String[] retvalue = message.split(",");
        JSONObject obj = new JSONObject();
        if(retvalue.length==1){
            obj.put("name", "");
            obj.put("link", "");
            obj.put("timestamp", "");
            obj.put("message", retvalue[0]);
        }else{
            obj.put("name", retvalue[0]);
            obj.put("link", retvalue[1]);
            obj.put("timestamp", retvalue[2]);
            obj.put("message", "success");            
        }
    System.out.println(obj);
        return obj;
    }
}
