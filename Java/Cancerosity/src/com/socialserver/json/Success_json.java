/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.socialserver.json;

import java.io.FileWriter;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.socialserver.beans.*;
import com.socialserver.database.SqlBeanUpdated;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Success_json {

    public Object jsonPrint(String value) {
        /*
         Stories:
         -Message
         -Like
         Persons(comma seperated)
         -Post Time
         -Comments
         Persons
         -Message
         -Time
         -Likes
         -Persons(comma seperated)
         */

        JSONObject obj = new JSONObject();

        // HttpSession session=request.getSession();
        // RegistrationFormResultBean result_bean=(RegistrationFormResultBean)session.getAttribute("result");
        //String value = result_bean.getValue();
        String username = "", filename = "", url = "", urlpre = "", uptime = "";
        String picture;
        String id = "";
        Map objOuter = new LinkedHashMap();
        int Counter = 0;
        int imageCounter = 0;
        int videoCounter = 0;
        int storyCounter = 0;
        String [] profileData = null;

        // String [] retvalue = value.matches();
        if (value.indexOf("~") < 1) {
            String[] retvalue = value.split(",");
            //id = retvalue[0];
            username = retvalue[0];
            //id = retvalue[2];

        } else {
            String[] retvalue = value.split("~");
            String strValue = "";
            String mediaType = "";
            String[] innerValues;
            Map objInner1 = new LinkedHashMap();
            Map objInner2 = new LinkedHashMap();
            Map objInner3 = new LinkedHashMap();

            JSONArray objInnerlist1 = new JSONArray();
            JSONArray objInnerlist2 = new JSONArray();
            JSONArray objInnerlist3 = new JSONArray();

            //JSONArray  objlist = new JSONArray ();
            //JSONArray  objInnerlist = new JSONArray ();


            Vector<String> list = new Vector<String>();
            for (int i = 0; i < retvalue.length; i++) {
                list.add(retvalue[i].substring(retvalue[i].indexOf("=") + 1));
            }
            Iterator itr = list.iterator();

            while (itr.hasNext()) {

                strValue = (String) itr.next();//strValue contains the id,name,filename,url, urlpre,time for 
                mediaType = mediaCheck(strValue);
                if (mediaType.equals("image")) {
                    JSONObject objImg = new JSONObject();
                    innerValues = strValue.split(",");
                    //JSONArray objlist = new JSONArray();
                    username = innerValues[0];
                    filename = innerValues[1];
                    url = innerValues[2];
                    urlpre = innerValues[3];
                    uptime = innerValues[4];

                    objImg.put("username", username);
                    objImg.put("filename", filename);
                    objImg.put("url", url);
                    objImg.put("urlpre", urlpre);
                    objImg.put("uptime", uptime);

                    /*
                     objlist.add(innerValues[0]);
                     objlist.add(innerValues[1]);
                     objlist.add(innerValues[2]);
                     objlist.add(innerValues[3]);
                     objlist.add(innerValues[4]);
                     objlist.add(innerValues[5]);
                     //objInner1.put(("image" + imageCounter++).toString(), objlist);
                     */
                    //if (imageCounter < 6) {

                    obj.put(("data" + Counter++).toString(), objImg);
                    //}
                    //itr.remove();
                }
                if (mediaType.equals("video")) {
                    JSONObject objVid = new JSONObject();
                    innerValues = strValue.split(",");
                    //JSONArray objlist = new JSONArray();
                    username = innerValues[0];
                    filename = innerValues[1];
                    url = innerValues[2];
                    urlpre = innerValues[3];
                    uptime = innerValues[4];

                    objVid.put("username", username);
                    objVid.put("filename", filename);
                    objVid.put("url", url);
                    objVid.put("urlpre", urlpre);
                    objVid.put("uptime", uptime);
                    /*
                     objlist.add(innerValues[0]);
                     objlist.add(innerValues[1]);
                     objlist.add(innerValues[2]);
                     objlist.add(innerValues[3]);
                     objlist.add(innerValues[4]);
                     objlist.add(innerValues[5]);
                     //objInner2.put(("video" + videoCounter++).toString(), objlist);
                     */
                    //if (videoCounter < 6) {
                    // obj.put("", objVid);
                    obj.put(("data" + Counter++).toString(), objVid);
                    //}
                    //itr.remove();
                }
                if (mediaType.equals("story")) {
                    JSONObject objStory = new JSONObject();
                    innerValues = strValue.split(",");
                    //JSONArray objlist = new JSONArray();
                    username = innerValues[0];
                    url = innerValues[1];
                    uptime = innerValues[2];
                    filename = "";
                    urlpre = "";
                    /*
                     objlist.add(innerValues[0]);
                     objlist.add(innerValues[1]);
                     objlist.add(innerValues[2]);
                     objlist.add(innerValues[3]);
                     //objlist.add(innerValues[4]);
                     //objInner3.put(("story" + storyCounter++).toString(), objlist);
                     */
                    objStory.put("username", username);
                    objStory.put("filename", filename);
                    objStory.put("url", url);
                    objStory.put("urlpre", urlpre);
                    objStory.put("uptime", uptime);
                    // String strStory=innerValues[0]+","+innerValues[1]+","+"Story"+","+innerValues[2]+","+innerValues[3];
                    //if (storyCounter < 6) {
                    //obj.put("", strStory);
                    obj.put(("data" + Counter++).toString(), objStory);
                    //}
                    //itr.remove();
                }
            }
            /*
             objInnerlist1.add(objInner1);
             objOuter.put("images", objInnerlist1);
             objInnerlist2.add(objInner2);
             objOuter.put("videos", objInnerlist2);
             objInnerlist3.add(objInner3);
             objOuter.put("stories", objInnerlist3);
             */

        }
        try {
            /*
             while (imageCounter < 6) {
             obj.put(("image" + imageCounter++).toString(), "");

             }
             while (videoCounter < 6) {
             obj.put(("video" + videoCounter++).toString(), "");
             }
             while (storyCounter < 6) {
             obj.put(("story" + storyCounter++).toString(), "");
             }
             */
            //obj.put("user_id", id);
            profileData=new SqlBeanUpdated().getProfileData(username).split(",");
        } catch (Exception ex) {
            Logger.getLogger(Success_json.class.getName()).log(Level.SEVERE, null, ex);
        }
        obj.put("name", username);
        //obj.put("Profile data", objOuter);
        // obj.put("user_id",id );
        obj.put("param", "True");
        obj.put("profileImageLink", profileData[0]);
        obj.put("status", profileData[1]);
        //System.out.print(obj);
        return obj;
    }

    public String mediaCheck(String data) {
        //Image Formats  .webp, .jpg, .gif, .png, .bmp, 
        //Video Formats  .mov, .mp4, .m4v, .3gp, .m4a, .aac, .webm, .mkv
        String val = "";
        if ((data.indexOf(".jpg") > 1) || (data.indexOf(".jpeg") > 1) || (data.indexOf(".gif") > 1) || (data.indexOf(".png") > 1) || (data.indexOf(".webp") > 1) || (data.indexOf(".bmp") > 1)) {
            val = "image";
        } else if ((data.indexOf(".mov") > 1) || (data.indexOf(".mp4") > 1) || (data.indexOf(".m4v") > 1) || (data.indexOf(".3gp") > 1) || (data.indexOf(".m4a") > 1) || (data.indexOf(".aac") > 1) || (data.indexOf(".webm") > 1) || (data.indexOf(".mkv") > 1)) {
            val = "video";
        } else {
            val = "story";
        }

        //id = ((String[]) data.split(","))[0];
        //username = ((String[]) data.split(","))[1];

        return val;
    }
}
