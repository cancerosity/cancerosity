/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.socialserver.json;

import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.simple.JSONObject;

import com.socialserver.database.SqlBean;

/**
 *
 * @author Bing
 */
public class User_Search {

    public Object jsonPrint(String value) {
        /*
         Stories:
         -Message
         -Like
         Persons(comma seperated)
         -Post Time
         -Comments
         Persons
         -Message
         -Time
         -Likes
         -Persons(comma seperated)
         */

        JSONObject obj = new JSONObject();
        String sno = "", username = "", status = "", profile_image = "", name_first="", name_last="", name="";
        String type = "", commentby = "", comment_value = "", followingNames = "", followerNames = "", strqry1 = "", strqry2 = "";
        int followingCount = 0, followerCount = 0;
        String privacy="";
        int Counter = 0;
        String[] profileData = null;

        // String [] retvalue = value.matches();
        if (value.indexOf("-END_OF_DATA-LINE-") < 1) {
            String[] retvalue = value.split(",");
            //id = retvalue[0];
            username = retvalue[0];
            //id = retvalue[2];

        } else {
            String[] retvalue = value.split("-END_OF_DATA-LINE-");
            System.out.println("***********************complete JSON Sring Value*************************"+"        "+value);
            String strValue = "";
            String[] innerValues;

            Vector<String> list = new Vector<String>();
            for (int i = 0; i < retvalue.length; i++) {
                list.add(retvalue[i]);
            }
            Iterator itr = list.iterator();
            while (itr.hasNext()) {
                strValue = (String) itr.next();//strValue contains the id,name,filename,url, urlpre,time for 
                JSONObject objImg = new JSONObject();
                System.out.println("***********************JSON Sring Value*************************"+"        "+strValue);
                innerValues = strValue.split(",");
                //JSONArray objlist = new JSONArray();
                sno = innerValues[0];
                name = innerValues[4]+" "+innerValues[5];
                username = innerValues[1];
                try {
                    followingCount = 0;
                    followerCount = 0;
                    followingNames = "";
                    followerNames = "";
                    SqlBean db = new SqlBean();
                    strqry1 = "select following from follow where username ='" + username + "'";
                    followingNames = db.followingUser(strqry1);
                    System.out.println("***********************JSON followingNames Value*************************"+"        "+followingNames);
                    if (!followingNames.equals("")) {
                        String arr2[] = followingNames.split(",");
                        followingCount = (arr2.length);
                    }
                    strqry2 = "select username from follow where following ='" + username + "'";
                    followerNames = db.followingUser(strqry2);
                    System.out.println("***********************JSON followerNames Value*************************"+"        "+followerNames);
                    if (!followerNames.equals("")) {
                        String[] arr3;
                        arr3 = followerNames.split(",");
                        followerCount = (arr3.length);
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
                profile_image = innerValues[2];
                status = innerValues[3];
                privacy=innerValues[6];
                objImg.put("sno", sno);
                objImg.put("username", username);
                objImg.put("name", name);
                objImg.put("profile_image", profile_image);
                objImg.put("status", status);
                objImg.put("followingCount", followingCount);
                objImg.put("followingNames", followingNames);
                objImg.put("followerCount", followerCount);
                objImg.put("followerNames", followerNames);
                objImg.put("privacy", privacy);
                obj.put(("data" + Counter++).toString(), objImg);
            }
        }
        try {
        } catch (Exception ex) {
            Logger.getLogger(Success_json.class.getName()).log(Level.SEVERE, null, ex);
        }
        obj.put("param", "True");
        return obj;
    }
}
