/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.socialserver.json;

import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.simple.JSONObject;

import com.socialserver.controller.DateTimeComparison;
import com.socialserver.database.SqlBean;

/**
 *
 * @author Bing
 */
public class Story_Search {

    public Object jsonPrint(String value) {
        /*
         Stories:
         -Message
         -Like
         Persons(comma seperated)
         -Post Time
         -Comments
         Persons
         -Message
         -Time
         -Likes
         -Persons(comma seperated)
         */

        JSONObject obj = new JSONObject();
        DateTimeComparison dateobj = new DateTimeComparison();
        String sno = "", username = "", content = "", inserted_at = "", like = "", dislike = "", share = "", profile_image = "";
        String likename = "", qry = "", likeprint = "", dislikeprint = "", dislikename = "";
        String comment="",location="",elapsed = "",commentData="";
        int Counter = 0;
        String[] profileData = null;

        // String [] retvalue = value.matches();
        if (value.indexOf("-END_OF_DATA-LINE-") < 1) {
            String[] retvalue = value.split(",");
            //id = retvalue[0];
            username = retvalue[0];
            //id = retvalue[2];

        } else {
            String[] retvalue = value.split("-END_OF_DATA-LINE-");
            String strValue = "";
            String[] innerValues;

            Vector<String> list = new Vector<String>();
            for (int i = 0; i < retvalue.length; i++) {
                list.add(retvalue[i]);
            }
            Iterator itr = list.iterator();
            while (itr.hasNext()) {
                elapsed = "";
                strValue = (String) itr.next();//strValue contains the id,name,filename,url, urlpre,time for 
                JSONObject objImg = new JSONObject();
                innerValues = strValue.split("-END-");
                //JSONArray objlist = new JSONArray();
                sno = innerValues[0];
                username = innerValues[1];
                content = innerValues[2];
                String arr[] = innerValues[3].split("_");
                String arr1[] = innerValues[4].split("_");
                inserted_at=innerValues[6];
                
                likeprint="";
                dislikeprint="";
                try {
                    SqlBean db = new SqlBean();
                    for (int i = 1; i < arr.length; i++) {
                        qry = "select username from user where sno = '" + arr[i] + "'";
                        likename = db.executeQuery(qry);
                        likeprint = likeprint + "," + likename;
                    }
                    for (int j = 1; j < arr1.length; j++) {
                        qry = "select username from user where sno = '" + arr1[j] + "'";
                        dislikename = db.executeQuery(qry);
                        dislikeprint = dislikeprint + "," + dislikename;
                    }
                    qry = "select count(*) from comment,user where comment.altsno = '" + sno + "' and comment.type= 'story' and user.username = comment.username order by inserted_at";
                    comment = db.executeQuery(qry);
                    commentData="";
                    String comment_qry="select comment.username,comment.comment from comment,user where comment.altsno = '" + sno + "' and comment.type= 'story' and user.username = comment.username order by inserted_at desc limit 4";
                    System.out.println("comment_qry"+comment_qry);
                    commentData=db.showLatestComment(comment_qry);
                    
                    elapsed = dateobj.showtime(inserted_at);
                } catch (Exception e) {
                    System.out.println(e);
                }
                like = Integer.toString((arr.length)-1);
                dislike = Integer.toString((arr1.length)-1);
                location=innerValues[5];
                
                
                profile_image = innerValues[7];

                objImg.put("sno", sno);
                objImg.put("username", username);
                objImg.put("content", content);
                objImg.put("like", like);
                objImg.put("likenames", likeprint);
                objImg.put("dislike", dislike);
                objImg.put("dislikenames", dislikeprint);
                objImg.put("comment", comment);
                objImg.put("location", location);
                objImg.put("inserted_at", inserted_at);
                objImg.put("elapsed", elapsed);
                objImg.put("profile_image", profile_image);
                objImg.put("commentval", commentData);
                obj.put(("data" + Counter++).toString(), objImg);
            }
        }
        try {
        } catch (Exception ex) {
            Logger.getLogger(Success_json.class.getName()).log(Level.SEVERE, null, ex);
        }
        obj.put("param", "True");
        return obj;
    }
}
