/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.socialserver.json;

import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.simple.JSONObject;

import com.socialserver.controller.DateTimeComparison;
import com.socialserver.database.SqlBean;

/**
 *
 * @author Bing
 */
public class User_Deatil {

    public Object jsonPrint(String value) {
        String sno = "", username = "", name = "", dob = "", email_id = "", phone = "", status = "", profile_image = "";
        String email_notification = "", location = "", privacy = "", followingNames = "", followerNames = "";
        int followingCount = 0, followerCount = 0;
        //String[] retvalue = value.split("-END_OF_DATA-LINE-");
        String strValue = "";
        int Counter = 0;
        String[] innerValues;

        JSONObject objImg = new JSONObject();
        innerValues = value.split("-END_OF_DATA-LINE-");
        //JSONArray objlist = new JSONArray();
        sno = innerValues[0];
        username = innerValues[1];
        status = innerValues[2];
        profile_image = innerValues[3];
        privacy = innerValues[4];
        try {
            SqlBean db = new SqlBean();
            String strqry1 = "select following from follow where username ='" + username + "'";
            followingNames = db.followingUser(strqry1);
            if (!followingNames.equals("")) {
                String arr2[] = followingNames.split(",");
                followingCount = (arr2.length);
            }
            String strqry2 = "select username from follow where following ='" + username + "'";
            followerNames = db.followingUser(strqry2);
            if (!followerNames.equals("")) {
                String[] arr3;
                arr3 = followerNames.split(",");
                followerCount = (arr3.length);
            }
        } catch (Exception ex) {
            Logger.getLogger(Success_json.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        objImg.put("sno", sno);
        objImg.put("username", username);
        objImg.put("profile_image", profile_image);
        objImg.put("status", status);
        objImg.put("followingCount", followingCount);
        objImg.put("followingNames", followingNames);
        objImg.put("followerCount", followerCount);
        objImg.put("followerNames", followerNames);
        objImg.put("privacy", privacy);
        objImg.put("param", "True");
        return objImg;

    }
}
