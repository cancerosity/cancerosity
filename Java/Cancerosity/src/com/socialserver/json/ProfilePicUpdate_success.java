/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.socialserver.json;

import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.socialserver.controller.DateTimeComparison;
import com.socialserver.database.SqlBean;

public class ProfilePicUpdate_success {

    public Object jsonPrint(String value) {
        JSONObject obj = new JSONObject();
        try {
            //story,inserted_at,username,location
            obj.put("param", value);//new dateTimeComparison().showtime(timestamp)
        } catch (Exception ex) {
            Logger.getLogger(ProfilePicUpdate_success.class.getName()).log(Level.SEVERE, null, ex);
        }
        //System.out.print(obj);
        return obj;
    }
}
