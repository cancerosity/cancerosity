package com.socialserver.aws.util;

import java.security.MessageDigest;

public class MD5Util {

	public static void main(String[] args) {
		System.out.println( toMd5("abcd1234") );
	}
	/**
	 * Md5
	 * @param plainPassword
	 * @return
	 */
	public static String toMd5(String plainPassword){
		 MessageDigest messageDigest = null;  
		  
	        try {  
	            messageDigest = MessageDigest.getInstance("MD5");  
	  
	            messageDigest.reset();  
	  
	            messageDigest.update(plainPassword.getBytes("UTF-8"));  
	        } catch (Exception ex) {  
	           ex.printStackTrace();
	        }  
	  
	        byte[] byteArray = messageDigest.digest();  
	  
	        StringBuffer md5StrBuff = new StringBuffer();  
	  
	        for (int i = 0; i < byteArray.length; i++) {              
	            if (Integer.toHexString(0xFF & byteArray[i]).length() == 1)  
	                md5StrBuff.append("0").append(Integer.toHexString(0xFF & byteArray[i]));  
	            else  
	                md5StrBuff.append(Integer.toHexString(0xFF & byteArray[i]));  
	        }  
	  
	        return md5StrBuff.toString();  
	}

}
