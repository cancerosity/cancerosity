package com.socialserver.aws.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.S3ObjectSummary;

/**
 * 
 * @author Bing
 * 
 */
public class S3Util {
	private static AmazonS3 s3Client = null;
	private final static String FOLDER_SUFFIX = "/";

	public static void main(String[] args) throws Exception {
		//create("Bing");
		uploadNewFile("Bing/test2.txt", new FileInputStream(
				new File("D:/111.txt")), 24);
	}

	/**
	 * init
	 */
	static {
		s3Client = new AmazonS3Client(new BasicAWSCredentials(
				AWSConfig.accessKey, AWSConfig.secretKey));
		Region region = Region.getRegion(Regions.US_EAST_1);
		s3Client.setRegion(region);
		//s3Client.setEndpoint("");
	}

	/**
	 * 
	 * @param path
	 * @return
	 */
	public static synchronized boolean uploadNewFile(String path,
			InputStream stream, long length) {
		try {
			ObjectMetadata metadata = new ObjectMetadata();
			metadata.setContentLength(length);
			 
			PutObjectResult result = s3Client.putObject(new PutObjectRequest(
					AWSConfig.S3_BUCKET_NAME, path, stream, metadata).withCannedAcl(CannedAccessControlList.PublicRead));
			System.out.println(result);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * 
	 * @param prefix
	 * @return
	 */
	public static synchronized List<String> listFileWithPrefix(String prefix) {
		List<String> files = new ArrayList<String>();
		try {
			ObjectListing objectListing = s3Client
					.listObjects(new ListObjectsRequest().withBucketName(
							AWSConfig.S3_BUCKET_NAME).withPrefix(prefix));
			for (S3ObjectSummary objectSummary : objectListing
					.getObjectSummaries()) {
				files.add(objectSummary.getKey());
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return files;
	}

	/**
	 * 
	 * @param path
	 * @return
	 */
	public static synchronized boolean deleteFile(String path) {
		try {
			s3Client.deleteObject(AWSConfig.S3_BUCKET_NAME, path);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	/**
	 * 
	 * @param foldername
	 */
	public static void createFolder(String foldername) {

		try {
			ObjectMetadata metadata = new ObjectMetadata();
			metadata.setContentLength(0);

			// Create empty content
			InputStream emptyContent = new ByteArrayInputStream(new byte[0]);

			// Create a PutObjectRequest passing the folder name suffixed by /
			PutObjectRequest putObjectRequest = new PutObjectRequest(
					AWSConfig.S3_BUCKET_NAME, foldername + FOLDER_SUFFIX,
					emptyContent, metadata);

			// Send request to S3 to create folder
			s3Client.putObject(putObjectRequest);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}
}
