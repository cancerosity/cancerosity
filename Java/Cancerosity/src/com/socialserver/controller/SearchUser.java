/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.socialserver.controller;

import com.socialserver.database.SqlBean;
import com.socialserver.json.Story_Search;
import com.socialserver.json.User_Search;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.simple.JSONObject;

/**
 *
 * @author Bing
 */
public class SearchUser extends org.apache.struts.action.Action {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";
    private String username="";
    private String loginuser="";
    private String x="";
    /**
     * This is the action called from the Struts framework.
     *
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        if (doGet(request, response)) {
            printParameters(request, response);
        } else {
            printParameters(request, response);
        }
        return null;
    }
public boolean doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        try {
            SqlBean ob = new SqlBean();
            username = request.getParameter("username").toString();
            loginuser = request.getParameter("loginuser").toString();
            String sql_query = "select sno,username,picture,status,name_first,name_last,privacy from user where username like '%"+username+"%' and username not like '"+loginuser+"' limit 10";
            x=ob.searchUser(sql_query);
        } catch (Exception e) {
            System.out.println(e);
        }
        return true;
    }

    private void printParameters(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // Set response content type
       response.setContentType("application/json");
        //response.setCharacterEncoding("charset=UTF-8");

        // Actual logic goes here.
        PrintWriter out = response.getWriter();
        out.println((JSONObject) new User_Search().jsonPrint(x));
    }
}
