/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.socialserver.controller;

import java.util.Iterator;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.socialserver.database.SqlBean;

/**
 *
 * @author Bing
 */
public class RecentNewsEmailNotification extends org.apache.struts.action.Action {

    /* forward name="success" path="" */
    private static String SUCCESS = "success";
    String email_id="",action="",type="",post_by="",inserted_at="",message="",picAction="";
    /**
     * This is the action called from the Struts framework.
     *
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        SqlBean db= new SqlBean();
        try{
            String qry="select user.email_id,notification.action,notification.type,notification.post_by,notification.inserted_at from notification,user where user.username=notification.username order by notification.inserted_at desc limit 1";
            String result=db.newsEmailNotification(qry);
            if(result.equals("")){
                SUCCESS="failure";
            }
            else{
                String[] retValue;
                String[] innerValues;
                retValue=result.split("END_OF_DATA-LINE-");
                Vector<String> list = new Vector<String>();
                for(int i=0;i<retValue.length;i++){
                    list.add(retValue[i]);
                }
                Iterator itr= list.iterator();
                String strValue="";
                while(itr.hasNext()){
                    strValue=(String) itr.next();
                    innerValues = strValue.split("-END-");
                    email_id=innerValues[0];
                    action=innerValues[1];
                    type=innerValues[2];
                    post_by=innerValues[3];
                    inserted_at=innerValues[4];
                }
                if(type.equals("images")||type.equals("video")||type.equals("story")){
                    if(action.equals("liked")){
                        picAction="likes";
                    }
                    else{
                        picAction="dislikes";
                    }
                    if(type.equals("images")){
                        message=post_by+" "+picAction+" your photo";
                    }
                    else{
                        message=post_by+" "+picAction+" your "+type;
                    }
                    
                }
                else if(type.equals("commentimages")||type.equals("commentvideo")||type.equals("commentstory")){
                    if(type.equals("commentimages")){
                        message=post_by+" commented \'"+action+"\'"+" on your photo";
                    }
                    else{
                        message=post_by+" commented \'"+action+"\'"+" on your "+(type.substring(7));
                    }                   
                }
                else{
                    message=post_by+" is following you";
                }
                CircleNotification e_noti = new CircleNotification();
                int test = e_noti.test(email_id, message);
                System.out.println(test);
            }
        }
        catch(Exception e){
            System.out.println(e);
        }
        return mapping.findForward(SUCCESS);
    }
}
