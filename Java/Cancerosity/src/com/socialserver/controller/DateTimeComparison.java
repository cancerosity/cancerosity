/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.socialserver.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.joda.time.*;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

/**
 *
 * @author Bing
 */
public class DateTimeComparison {

    public String showtime(String dateFromDatabase) throws Exception {

         String elapsed="";
        //String dateFromDatabase = "2013-05-07 06:05:23";	// Date from database
        
        DateFormat date = new SimpleDateFormat("d MMM yyyy");
        DateFormat time = new SimpleDateFormat("hh:mm a");
        DateFormat formatterDateFromDatabase = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dateType = formatterDateFromDatabase.parse(dateFromDatabase);
        String strTime = time.format(dateType);
        String strDate = date.format(dateType);
        //System.out.println("Time:"+strTime);
        //System.out.println("Date:"+strDate);
        
        DateTime startDate = new DateTime(dateType);
        Date currentDate = new Date();  // Current Date
        DateTime dateNow = new DateTime(currentDate);
        //System.out.println("current Time:"+dateNow);

        
        Days diff = Days.daysBetween(startDate, dateNow);
        int diffDate=diff.getDays();
        //System.out.println(diffDate);
        if(diffDate==0){
            elapsed="today at "+strTime;
        }
        else if(diffDate==1){
            elapsed="yesterday at "+strTime;
        }
        else{
            elapsed=strDate+" at "+strTime;
        }
        System.out.println(elapsed);
        return elapsed;
    }
}
