 
package com.socialserver.controller;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.simple.JSONObject;

import com.socialserver.database.SqlBean;

/**
 *
 * @author Bing
 */
public class DeleteContentAction extends org.apache.struts.action.Action {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";
    private String username = "";
    private String sno = "";
    private String col_name = "";
    private String type = "";
    private String link = "";
    String sql_query = "";
    String status = "";

    /**
     * This is the action called from the Struts framework.
     *
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        boolean val = doGet(request, response);
        if (val) {
            printParameters(request, response, val);
        } else {
            printParameters(request, response, val);
        }
        return null;
    }

    public boolean doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            SqlBean ob = new SqlBean();
            type = request.getParameter("type").toString(); //whether it is image,video or story
            sno = request.getParameter("sno").toString();  // sno of content
            username = request.getParameter("username").toString();
            if ((type.equals("images")) || (type.equals("video"))) {
                if (type.equals("images")) {
                    sql_query = "select link_of_images from images where username='" + username + "' and sno='" + sno + "' ";
                } else {
                    sql_query = "select link_of_video from video where username='" + username + "' and sno='" + sno + "' ";
                }
                link = ob.executeQuery(sql_query);
                File f1 = new File(link);
                boolean success = f1.delete();
                if (!success) {
                    System.out.println("Deletion failed.");
                } else {
                    System.out.println("File deleted.");
                }
            }
            String qry = "delete from " + type + " where username='" + username + "' and sno ='" + sno + "'";
            status = ob.executeUpdate(qry);
            if (status.equals("ok")) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return true;
    }

    private void printParameters(HttpServletRequest request, HttpServletResponse response, boolean val) throws IOException {
        // Set response content type
        response.setContentType("application/json");
        //response.setCharacterEncoding("charset=UTF-8");
        String value = "";
        if (val == true) {
            value = "true";
        } else {
            value = "false";
        }
        // Actual logic goes here.
        PrintWriter out = response.getWriter();
        JSONObject obj = new JSONObject();
        obj.put("param", value);
        out.println(obj);
    }
}
