/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.socialserver.controller;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;

import java.util.List;

import javax.servlet.ServletException;

import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;

import java.io.PrintWriter;
import java.util.Random;

import javax.servlet.http.HttpSession;

import com.socialserver.aws.util.AWSConfig;
import com.socialserver.aws.util.S3Util;
import com.socialserver.database.SqlBean;
import com.socialserver.foler.*;
import com.socialserver.json.Upload_result;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.simple.*;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 *
 * @author Bing
 */
public class UploadContentAction extends org.apache.struts.action.Action {

    /* forward name="success" path="" */
    String baseDirectory = "";
    String imageDirectory = "";
    String videoDirectory = "";
    String userWebDefinedDirectory = "";
    String userDiskDirectory = "";
    String userDirectory = "";
    String userID = "";
    String username = "";
    String user_id = "";
    String table_name = "";
    String timestamp = "";
    String filename = "";
    String sql_query = "";
    String extraPath = "C:/";
    String web = "web";
    String IP;
    String[] pathStr;
    String imagePath;
    String videoPath;
    String locationLat="";
    String locationLong="";
    String location="";
    private boolean val = false;

    /**
     * This is the action called from the Struts framework.
     *
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        if (doPost(request, response)) {
            printParameters(request, response);
        } else {
            printParameters(request, response);
        }
        return null;
       
    }

    public boolean doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, FileUploadException {
        String OS = System.getProperty("os.name").toLowerCase();
        String confFileName;
        File configFile;
        if (OS.indexOf("win") >= 0 &&!AWSConfig.USE_S3) {
            confFileName = "config_windows.properties";
            System.out.println("In window Upload Content");
            configFile = new File(confFileName);
        } else {
            confFileName = "config_Server.properties";
            System.out.println("server Content Upload");
            configFile = new File(confFileName);
        }
        System.out.println("configFile:................."+configFile);
            //baseDirectory = new LoadProperties().loadPropertiesFile(confFileName);
            System.out.println("baseDirectory:................."+baseDirectory);
            //baseDirectory==/Cancer_test/MediaFiles
           // pathStr = baseDirectory.substring(baseDirectory.indexOf("/") + 1).split("/");
            

        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        if (!isMultipart) {
            System.out.println("false");
        } else {
            List<FileItem> items = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
            for (FileItem item : items) {
                if (item.isFormField()) {
                    // Process regular form field (input type="text|radio|checkbox|etc", select, etc).
                    String fieldname = item.getFieldName();
                    String fieldValue = item.getString();
                    System.out.println("fieldValue Android:..........."+fieldValue);
                    if (fieldValue.indexOf(",") > 0) {
                        if (((String[]) fieldValue.split(","))[1].equals("Android")) {
                            //We get input as username,Android in for field for Android platform.
                            //username,Android,locationLat,locationLong
                            username = ((String[]) fieldValue.split(","))[0];
                            locationLat = ((String[]) fieldValue.split(","))[2];
                            locationLong = ((String[]) fieldValue.split(","))[3];
                            try{
                                SqlBean ob = new SqlBean();
                                String sql_query ="select location from user where username = '"+username+"'";
                                String contentloc=ob.executeQuery(sql_query);
                                if(contentloc.equals("yes")){                                   
                                    location = new FindLocation().loc(locationLat, locationLong);
                                }
                                else{
                                    location="";
                                }
                            }
                            catch(Exception e){
                                
                            }
                            userID = username;
                        }
                    }

                    // ... (do your job here)
                } else {
                    // Process form file field (input type="file").
                    String fieldname = item.getFieldName();
                    System.out.println("fieldname iPhone:..........."+fieldname);
                    if (fieldname.indexOf(",") > 0) {
                        if (((String[]) fieldname.split(","))[1].equals("iphone")) {
                            //We get input as username,iphone in for field for iPhone platform.
                            username = ((String[]) fieldname.split(","))[0];
                            locationLat = ((String[]) fieldname.split(","))[2];
                            locationLong = ((String[]) fieldname.split(","))[3];
                            location = new FindLocation().loc(locationLat, locationLong);
                            userID = username;
                        }
                    }
                    filename = FilenameUtils.getName(item.getName().replaceAll(" ", "_"));
                    filename = filename.substring(0, filename.indexOf(".")) + new Random().nextInt(100000) + filename.substring(filename.indexOf("."));
                    /*
                     filename= filename.substring(0,filename.indexOf(".")-1 )
                     +new Date().toGMTString()
                     +filename.substring(filename.indexOf("."));
                     */
                    InputStream filecontent = item.getInputStream();
                    int x = 0;
                    try {
                        userDirectory = userID;
                        imageDirectory = "image";
                        videoDirectory = "video";
                        if (OS.indexOf("win") >= 0 &&!AWSConfig.USE_S3) {
                            System.out.println("In Window:");
                            imagePath = extraPath + File.separator + pathStr[0] + File.separator + web + File.separator + pathStr[1] + File.separator + userDirectory + File.separator + imageDirectory + File.separator;
                            System.out.println("Image Path:"+imagePath);
                            videoPath = extraPath + File.separator + pathStr[0] + File.separator + web + File.separator + pathStr[1] + File.separator + userDirectory + File.separator + videoDirectory + File.separator;
                            System.out.println("Video Path:"+videoPath);
                            IP = "http://127.0.0.1:8084/";
                            System.out.println("IP:"+IP);
                        } else {
                            System.out.println("In server:");
                            imagePath = imageDirectory;
                            System.out.println("Image Path:"+imagePath);
                            videoPath = videoDirectory;
                            System.out.println("Video Path:"+videoPath);
                            IP = AWSConfig.S3_BASE_URL;
                            System.out.println("IP:"+IP);
                        }

                        System.out.println("My extesion is here" + filename);
                        if ((filename.substring(filename.lastIndexOf("."), filename.length()).equalsIgnoreCase(".jpg"))
                                || (filename.substring(filename.lastIndexOf("."), filename.length()).equalsIgnoreCase(".png"))
                                || (filename.substring(filename.lastIndexOf("."), filename.length()).equalsIgnoreCase(".gif"))) {
                            userWebDefinedDirectory = baseDirectory + File.separator + userDirectory + File.separator + imageDirectory + File.separator;
                            userWebDefinedDirectory = userWebDefinedDirectory.replace('\\', '/');
                            userDiskDirectory = imagePath;
                            table_name = "images";
                            timestamp = getCurrentTime();

                        } else {
                            userWebDefinedDirectory = baseDirectory + File.separator + userDirectory + File.separator + videoDirectory + File.separator;
                            userWebDefinedDirectory = userWebDefinedDirectory.replace('\\', '/');
                            userDiskDirectory = videoPath;
                            table_name = "video";
                            timestamp = getCurrentTime();

                            HttpSession session = request.getSession();
                            //session.setAttribute("_upload_result", bean_upload);
                        }
                        if (OS.indexOf("win") >= 0 &&!AWSConfig.USE_S3) {
                            File savedFile = new File(userDiskDirectory + filename);
                            item.write(savedFile);
                        } else {
//                            File savedFile = new File("/home/cancerci/apache-tomcat-7.0.35/webapps/" + userDiskDirectory + filename);
//                            item.write(savedFile);
                        	
                         
                        	S3Util.uploadNewFile(username+"/"+userDiskDirectory+"/"+filename, item.getInputStream(), item.getSize());
                        }


                        SqlBean ob = new SqlBean();
                        sql_query = "insert into  " + table_name
                                + "(username,name_of_" + table_name + ",link_of_" + table_name + ",preview,inserted_at,location) values("
                                + "'" + username + "'" + ","
                                + "'" + filename + "'" + ","
                                + "'" + IP + username+"/"+userDiskDirectory+"/"+filename+ "'" + ","
                                + "'" + IP + username+"/"+userDiskDirectory+"/"+filename   + "'" + ","
                                + "'" + timestamp + "'" +","
                                + "'" + location + "'" + ")";
                        System.out.println("Insert Items:"+sql_query);
                        x = ob.content_upload(sql_query.replace('\\', '/'));


                    } catch (Exception e) {
                        System.out.println("false");
                        System.out.println("in File uploading cancer circle exception" + e);
                        val = false;
                    } 

                    System.out.println("true");

                    if (x == 0) {
                        val = false;
                    } else {
                        val = true;
                    }
                }
            }
        }

        return val;
    }

    public String getCurrentTime() {

        DateTime dt = new DateTime();
        DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        String str = fmt.print(dt);
        System.out.println(str);
        return str;




        //DateFormat dateFormat = new SimpleDateFormat("dd/mm/yyyy HH:mm:ss");
        //Date date = new Date();
        //return dateFormat.format(date);
    }

    private void printParameters(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // Set response content type
        response.setContentType("application/json");
        //response.setCharacterEncoding("charset=UTF-8");

        // Actual logic goes here.
        PrintWriter out = response.getWriter();
        String parameters="";
        try {
            parameters = filename + "--endOfData--"
             + IP+username+"/"+userDiskDirectory+"/"+filename + "--endOfData--"
             + username+"/"+userDiskDirectory+"/"+filename + "--endOfData--"
             + timestamp+ "--endOfData--"
             + location+ "--endOfData--"
             + new DateTimeComparison().showtime(timestamp)+ "--endOfData--"
             + new SqlBean().executeQuery("select sno from "+ table_name +" where username ='" + username + "' LIMIT 0 , 1 ");
            System.out.println("******************** select sno from "+ table_name +"where username ='" + username + "' LIMIT 0 , 1 ***************");
            //SELECT sno FROM video WHERE username = 'Vinit' LIMIT 0 , 10
        } catch (Exception ex) {
            Logger.getLogger(UploadContentAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        out.println((JSONObject) new Upload_result().jsonPrint(parameters));
    }
}
