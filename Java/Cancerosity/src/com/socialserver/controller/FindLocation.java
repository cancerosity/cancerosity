/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.socialserver.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author Bing
 */
public class FindLocation {

    public String loc(String latitude, String longitude) {
        String Location = "";
        String ExactLocation = "";
        String type = "", city = "", country = "";
        String url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + latitude + "," + longitude + "&sensor=false";
        int i = 0;
        try {
            URL oracle = new URL(url);
            URLConnection yc = oracle.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    yc.getInputStream()));
            String inputLine = "";
            String result = "";
            JSONParser parser = new JSONParser();
            while ((inputLine = in.readLine()) != null) {
                //System.out.println(i++);
                result = result + inputLine;
            }
            /*Location= result.substring(result.indexOf("formatted_address")+22);
        
             System.out.println("Location:  "+ Location);
             ExactLocation= Location.substring(0, Location.indexOf("\","));*/
            Object obj = parser.parse(result);
            JSONObject jsonObject = (JSONObject) obj;
            //JSONObject lineItems1 =  (JSONObject) jsonObject.get("results");
            JSONArray lineItems = (JSONArray) jsonObject.get("results");
            for (Object o : lineItems) {
                JSONObject jsonLineItem = (JSONObject) o;
                JSONObject objImg = new JSONObject();
                JSONArray Items = (JSONArray) jsonLineItem.get("address_components");
                for (Object o1 : Items) {
                    JSONObject jsonLineItem1 = (JSONObject) o1;
                    type = jsonLineItem1.get("types").toString();
                    if (type.contains("locality")) {
                        city = jsonLineItem1.get("long_name").toString();
                    }
                    if (type.contains("country")) {
                        country = jsonLineItem1.get("long_name").toString();
                    }
                }
                break;
            }
            ExactLocation = city + "," + country;
            if(ExactLocation==null||ExactLocation.equals("")||ExactLocation.equals(",")){
                ExactLocation="Location is not found";
            }
            System.out.println("ExactLocation:" + ExactLocation);
            in.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        return ExactLocation;
    }
}
