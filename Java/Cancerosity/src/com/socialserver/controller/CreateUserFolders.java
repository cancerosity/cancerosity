/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.socialserver.controller;

import com.socialserver.aws.util.AWSConfig;
import com.socialserver.aws.util.S3Util;
import com.socialserver.foler.FolderManipulation;
import com.socialserver.foler.LoadProperties;
import com.socialserver.foler.PropertiesFile;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileUploadException;

/**
 * 
 * @author Bing
 */
public class CreateUserFolders {

	String baseDirectory = "";
	String imageDirectory = "";
	String videoDirectory = "";
	String profileImageDirectory = "";
	String userWebDefinedDirectory = "";
	String userDiskDirectory = "";
	String userDirectory = "";
	String userID = "";
	String username = "";
	String user_id = "";
	String table_name = "";
	String timestamp = "";
	String filename = "";
	String sql_query = "";
	String extraPath = "";
	String web = "web";
	String IP = "http://127.0.0.1:8084/";
	String[] pathStr;
	private boolean val = false;

	/*
	 * public boolean doPost(HttpServletRequest request, HttpServletResponse
	 * response, String name) throws ServletException, IOException,
	 * FileUploadException { createFolder(request, response, name); return true;
	 * }
	 */

	public void createFolderWindows(HttpServletRequest request,
			HttpServletResponse response, String userName)
			throws ServletException, IOException, FileUploadException {
		File configFile = new File("config_windows.properties");
		System.out.println("Username:   " + userName);
		extraPath = "D:\\test_live";
		System.out.println("extra Path:" + extraPath);
		if (!configFile.exists()) {
			// Create Properties File for the project
			String basePath = request.getRequestURI();

			new PropertiesFile().createPropFile(
					basePath.substring(0, basePath.lastIndexOf("/")),
					"config_windows.properties");
			// getServlet().getServletConfig().getServletContext().getContextPath().toString()
			// System.out.println("basePath:   "+basePath.substring(0,basePath.lastIndexOf(File.separator)));
			baseDirectory = new LoadProperties()
					.loadPropertiesFile("config_windows.properties");
			System.out.println("windows Base directory:   " + baseDirectory);
			// Path to view on web Browser
			// http://localhost:8080/Cancer_test/Cancer_circle/akash/image/vcm_s_kf_m160_160x128.jpg
			// Path to save on disk F:\VirtuosoNetsoft\New Projects\25 Dec
			// 2012\Web service Source\Cancer_test\web\MediaFiles
		} else {
			baseDirectory = new LoadProperties()
					.loadPropertiesFile("config_windows.properties");
			// baseDirectory==/Cancer_test/MediaFiles
			System.out.println("windows Base directory:   " + baseDirectory);
			pathStr = baseDirectory.substring(baseDirectory.indexOf("/") + 1)
					.split("/");
		}
		String userFolder = extraPath + File.separator + pathStr[0]
				+ File.separator + web + File.separator + pathStr[1]
				+ File.separator + userName;
		File directory_check = new File(userFolder);
		if (!directory_check.exists()) {
			userDirectory = new FolderManipulation().createDirectory(extraPath
					+ File.separator + pathStr[0] + File.separator + web
					+ File.separator + pathStr[1], userName);
		}

		File directory_check_image = new File(userFolder + File.separator
				+ "image");
		if (!directory_check_image.exists()) {
			imageDirectory = new FolderManipulation().createDirectory(
					(userFolder), "image");
		}

		File directory_check_video = new File(userFolder + File.separator
				+ "video");
		if (!directory_check_video.exists()) {
			videoDirectory = new FolderManipulation().createDirectory(
					(userFolder), "video");
		}
		File directory_check_Profile_image = new File(userFolder
				+ File.separator + "profile_image");
		if (!directory_check_Profile_image.exists()) {
			profileImageDirectory = new FolderManipulation().createDirectory(
					(userFolder), "profile_image");
		}
	}
	
	/**
	 * For S3
	 * @param request
	 * @param response
	 * @param userName
	 * @throws ServletException
	 * @throws IOException
	 * @throws FileUploadException
	 */
	public void createFolder(HttpServletRequest request,
			HttpServletResponse response, String userName)
			throws ServletException, IOException, FileUploadException {
		System.out.println("Username:   " + userName);
		baseDirectory = AWSConfig.S3_BASE_URL; 
		//this try/catch added by bing for prevent create folder bug
		try {
			
			//create folder named username
			S3Util.createFolder(userName);
			//create image folder
			S3Util.createFolder(userName+"/image");
			//create video folder
			S3Util.createFolder(userName +"/video");
			//create profile_image folder
			S3Util.createFolder(userName +"/profile_image");
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public void createFolder_no_Windows(HttpServletRequest request,
			HttpServletResponse response, String userName)
			throws ServletException, IOException, FileUploadException {
		System.out.println("Username:   " + userName);
		 
		File configFile = new File("config_Server.properties");

		if (!configFile.exists()) {
			// Create Properties File for the project
			String basePath = request.getRequestURI();
			new PropertiesFile().createPropFile(
					basePath.substring(0, basePath.lastIndexOf("/")),
					"config_Server.properties");
			System.out.println("basePath:   "
					+ basePath.substring(0,
							basePath.lastIndexOf(File.separator)));
			baseDirectory = new LoadProperties()
					.loadPropertiesFile("config_Server.properties");
			// Path to view on web Browser
			// http://localhost:8080/Cancer_test/Cancer_circle/akash/image/vcm_s_kf_m160_160x128.jpg
			// Path to save on disk F:\VirtuosoNetsoft\New Projects\25 Dec
			// 2012\Web service Source\Cancer_test\web\MediaFiles
		} else {
			baseDirectory = new LoadProperties()
					.loadPropertiesFile("config_Server.properties");
			// baseDirectory==/Cancer_test/MediaFiles
			pathStr = baseDirectory.substring(baseDirectory.indexOf("/") + 1)
					.split("/");
		}
		
		//this try/catch added by bing for prevent create folder bug
		try {
			String userFolder = pathStr[0] + File.separator + pathStr[1]
					+ File.separator + userName;
			System.out.println("pathStr[0]:   " + pathStr[0]);
			System.out.println("pathStr[1]:   " + pathStr[1]);
			System.out.println(userFolder);
			File directory_check = new File(userFolder);
			if (!directory_check.exists()) {
				System.out.println("User Folder  to be Created " + pathStr[0]
						+ File.separator + pathStr[1]);
				new FolderManipulation().createDirectory(pathStr[0]
						+ File.separator + pathStr[1], userName);
				System.out.println("User Folder  Created ");
			}

			File directory_check_image = new File(userFolder + File.separator
					+ "image");
			if (!directory_check_image.exists()) {
				System.out.println("User's image Folder  to be Created: "
						+ userFolder);
				new FolderManipulation().createDirectory((userFolder), "image");
				System.out.println("Image folder under user created");
			}

			File directory_check_video = new File(userFolder + File.separator
					+ "video");
			if (!directory_check_video.exists()) {
				System.out.println("User's video Folder  to be Created: "
						+ userFolder);
				new FolderManipulation().createDirectory((userFolder), "video");
				System.out.println("Video folder under user created");
			}
			File directory_check_Profile_image = new File(userFolder
					+ File.separator + "profile_image");
			if (!directory_check_Profile_image.exists()) {
				System.out
						.println("User's profile_image Folder  to be Created: "
								+ userFolder);
				new FolderManipulation().createDirectory((userFolder),
						"profile_image");
				System.out.println("Profile_image folder under user created");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}
}
