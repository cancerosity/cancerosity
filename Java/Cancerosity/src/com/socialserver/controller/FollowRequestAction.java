/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.socialserver.controller;

import com.socialserver.database.SqlBean;
import com.socialserver.json.Follow_Request;
import com.socialserver.json.User_Search;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.simple.JSONObject;

/**
 *
 * @author Bing
 */
public class FollowRequestAction extends org.apache.struts.action.Action {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";
    String username = "";
    String following = "";
    String status = "";
    String privacy = "";
    String follow_request = "";
    String sql_query = "";

    /**
     * This is the action called from the Struts framework.
     *
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        if (doGet(request, response)) {
            printParameters(request, response);
        } else {
            printParameters(request, response);
        }
        return null;
    }

    public boolean doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            SqlBean ob = new SqlBean();
            username = request.getParameter("username").toString();

            String qry1 = "select follow_request from user where username='" + username + "'";
            follow_request = ob.executeQuery(qry1);

        } catch (Exception e) {
            System.out.println(e);
        }
        return true;
    }

   private void printParameters(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // Set response content type
       response.setContentType("application/json");
        //response.setCharacterEncoding("charset=UTF-8");

        // Actual logic goes here.
        PrintWriter out = response.getWriter();
        out.println((JSONObject) new Follow_Request().jsonPrint(follow_request));
    }
}
