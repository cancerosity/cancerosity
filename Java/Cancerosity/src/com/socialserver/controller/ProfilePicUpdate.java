/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.socialserver.controller;

import com.socialserver.database.SqlBean;
import com.socialserver.database.SqlBeanCircle;
import com.socialserver.json.ProfilePicUpdate_success;
import com.socialserver.json.Success_Circle;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.simple.JSONObject;

/**
 *
 * @author Bing
 */
public class ProfilePicUpdate extends org.apache.struts.action.Action {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";
    String result_profilePicUpdate = "";
    String profilePicLink="";
    String username="";
    /**
     * This is the action called from the Struts framework.
     *
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        if (doPost(request, response)) {
            printParameters(request, response);
        } else {
            printParameters(request, response);
        }
        return null;
    }

    public boolean doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        profilePicLink = request.getParameter("profilePicLink");
        username = request.getParameter("username");
        try {
            //SqlBeanCircle sqlbean = new SqlBeanCircle();
            SqlBean sqlbean = new SqlBean();
            //LoginFormResultBean result_bean_circle = new LoginFormResultBean();
            String sqlQuery = "UPDATE `cancer_live`.`user` SET `picture` = '"+profilePicLink+"' WHERE `user`.`username` ='"+username+"'";
            System.out.println("Update profile pic Qry:" + sqlQuery);
            result_profilePicUpdate = sqlbean.executeUpdate(sqlQuery);

            System.out.println(result_profilePicUpdate + "-----------------------");
            //result_bean_circle.setCircleValue(result_circle);
            //HttpSession session = request.getSession();
            //session.setAttribute("circle", result_circle);
        } catch (Exception ex) {
            Logger.getLogger(CancerFormController.class.getName()).log(Level.SEVERE, null, ex);
            result_profilePicUpdate = "False";
            return false;
        }
        result_profilePicUpdate = "True";
        return true;
    }

    private void printParameters(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // Set response content type
        response.setContentType("application/json");
        //response.setCharacterEncoding("charset=UTF-8");

        // Actual logic goes here.
        PrintWriter out = response.getWriter();
        out.println((JSONObject) new ProfilePicUpdate_success().jsonPrint(result_profilePicUpdate));
    }
}
