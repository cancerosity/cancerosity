/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.socialserver.controller;

import com.socialserver.database.SqlBean;
import com.socialserver.json.Success_Circle;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.simple.JSONObject;

/**
 *
 * @author Bing
 */
public class LikedAction extends org.apache.struts.action.Action {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";
    private String sno = "";
    private String table_name = "";
    private String username = "";
    private String result = "";
    private String action = "";

    /**
     * This is the action called from the Struts framework.
     *
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        if (doPost(request, response)) {
            printParameters(request, response);
        } else {
            printParameters(request, response);
        }
        return null;

    }

    public boolean doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            SqlBean db = new SqlBean();
            sno = request.getParameter("sno").toString();//images which is liked
            table_name = request.getParameter("type").toString();
            username = request.getParameter("username").toString();
            action = request.getParameter("action").toString();

            String qry = "select " + action + " from " + table_name + " where sno ='" + sno + "'";
            String value = db.executeQuery(qry);
            String arr[] = value.split("_");
            String qry1 = "select sno from user where username = '" + username + "'";

            String likedby = db.executeQuery(qry1);
            if (value.contains(likedby)) {
                result = Integer.toString((arr.length)-1);
                return true;
            } else {
                value = value + "_" + likedby;
                qry = "update " + table_name + " set " + action + "='" + value + "' where sno =" + sno + "";
                String status = db.executeUpdate(qry);
                if (status.equals("ok")) {
                    result = Integer.toString(arr.length);
                    String name_qry="select username from "+table_name+" where sno ='"+sno+"'";
                    String name=db.executeQuery(name_qry);
                    new RecentNews().news(name, action, table_name, username,sno);
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return true;
    }

    private void printParameters(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // Set response content type
        response.setContentType("application/json");
        //response.setCharacterEncoding("charset=UTF-8");

        // Actual logic goes here.
        PrintWriter out = response.getWriter();

        JSONObject obj = new JSONObject();
        obj.put("count", result);
        obj.put("param", "True");
        out.println(obj);
    }
}
