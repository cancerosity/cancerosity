/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.socialserver.controller;

import com.socialserver.database.SqlBean;
import com.socialserver.database.SqlBeanCircle;
import com.socialserver.json.Show_Comment;
import com.socialserver.json.Success_Circle;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.simple.JSONObject;

/**
 *
 * @author Bing
 */
public class ShowCommentAction extends org.apache.struts.action.Action {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";
    private String sno="";
    private String type="";
    String comment_value="";
    /**
     * This is the action called from the Struts framework.
     *
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
         if (doPost(request, response)) {
            printParameters(request, response);
        } else {
            printParameters(request, response);
        }
        return null;
    }

    public boolean doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            SqlBean db = new SqlBean();
            sno=request.getParameter("sno").toString();//images which is liked
            type=request.getParameter("type").toString();
            //LoginFormResultBean result_bean_circle = new LoginFormResultBean();
            String sqlQuery = "select comment.username,comment.comment,user.picture from comment,user where comment.altsno = '" +sno + "' and comment.type= '"+type+"' and user.username = comment.username order by inserted_at limit 10";
                   
            System.out.println("Comment:"+sqlQuery);
            comment_value = db.showComment(sqlQuery);
            

            System.out.println(comment_value + "-----------------------");
            //result_bean_circle.setCircleValue(result_circle);
            //HttpSession session = request.getSession();
            //session.setAttribute("circle", result_circle);
        } catch (Exception ex) {
            Logger.getLogger(CancerFormController.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    private void printParameters(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // Set response content type
        response.setContentType("application/json");
        //response.setCharacterEncoding("charset=UTF-8");

        // Actual logic goes here.
        PrintWriter out = response.getWriter();
        out.println((JSONObject) new Show_Comment().jsonPrint(comment_value));
    }
}

