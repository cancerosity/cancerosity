/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.socialserver.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.actions.DispatchAction;
import org.apache.struts.action.*;

import com.amazonaws.util.Md5Utils;
import com.socialserver.aws.util.MD5Util;
import com.socialserver.beans.*;
import com.socialserver.database.*;

import javax.servlet.http.HttpSession;

/**
 * 
 * @author Bing
 */
public class LoginFormController extends DispatchAction {

	/* forward name="success" path="" */
	private static String SUCCESS = "success";

	/**
	 * This is the Struts action method called on
	 * http://.../actionPath?method=myAction1, where "method" is the value
	 * specified in <action> element : ( <action parameter="method" .../> )
	 */
	public ActionForward myAction2(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String name;
		String password;
		LoginFormBean obj = (LoginFormBean) form;
		LoginFormResultBean result_bean = new LoginFormResultBean();
		name = obj.getUsername();
		password = obj.getPassword();
		String username = request.getParameter("username");
		SqlBean sqlbean = new SqlBean();
		SqlBeanCircle sqlbean1 = new SqlBeanCircle();
		String qry = "select" + "    `images`.`sno`,"
				+ "    `images`.`username`," + "    `images`.`link_of_images`,"
				+ "    `images`.`inserted_at`," + "    `images`.`liked`,"
				+ "    `images`.`dislike`," + "    `images`.`share`,"
				+ "    `images`.`location`," + "    `user`.`picture`" + "from"
				+ "    images,user " + "where" + " `images`.`username`='"
				+ username
				+ "'"
				+ "and"
				+ " `user`.`username`='"
				+ username
				+ "'"
				+ "union select "
				+ "    `video`.`sno`,"
				+ "    `video`.`username`,"
				+ "    `video`.`link_of_video`,"
				+ "    `video`.`inserted_at`,"
				+ "    `video`.`liked`,"
				+ "    `video`.`dislike`,"
				+ "    `video`.`share`,"
				+ "    `video`.`location`,"
				+ "     `user`.`picture`"
				+ "from"
				+ "    video,user "
				+ "where"
				+ " `video`.`username`='"
				+ username
				+ "'"
				+ "and"
				+ " `user`.`username`='"
				+ username
				+ "'"
				+ "union select "
				+ "    `story`.`sno`,"
				+ "    `story`.`username`,"
				+ "    `story`.`story`,"
				+ "    `story`.`inserted_at`,"
				+ "    `story`.`liked`,"
				+ "    `story`.`dislike`,"
				+ "    `story`.`share`,"
				+ "    `story`.`location`,"
				+ "     `user`.`picture`"
				+ "from"
				+ "    story,user  "
				+ "where"
				+ " `story`.`username`='"
				+ username
				+ "'"
				+ "and"
				+ " `user`.`username`='"
				+ username
				+ "'" + "order by inserted_at desc LIMIT 0,5";
		System.out.println("qry=" + qry);
		String result_profile = sqlbean1.getdetails(qry);
		System.out.println("LoginFormController Class:" + result_profile);
		// String result =
		// sqlbean.getdetails("select username from user where username = '" +
		// name + "' and password = '" + password + "'");
		// System.out.println(result + "-----------------------");
		result_bean.setResult(result_profile);
		HttpSession session = request.getSession();
		// session.setAttribute("result", result_bean);
		session.setAttribute("result", result_bean);
		System.out.println("session set value:" + result_bean);
		System.out.println("result_profile: "+result_profile.length());
		if (result_profile.length() < 2) {
			SUCCESS = "failure";
		} else {
			SUCCESS = "success";
		}

		return mapping.findForward(SUCCESS);
	}

	public ActionForward myAction1(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String name;
		String password;
		LoginFormBean obj = (LoginFormBean) form;
		LoginFormResultBean result_bean = new LoginFormResultBean();
		name = obj.getUsername();
		password = obj.getPassword();

		SqlBean sqlbean = new SqlBean();
		String val = sqlbean
				.executeQuery("select username from user where username = '"
						+ name + "' and password = '" + password + "'");
		System.out.println("********ExtraVal*****" + val);
		//add md5 password
		String md5Pass = MD5Util.toMd5(password);
		String result = sqlbean
				.showUserDetails("select sno,username,name_first,name_last,dob,email_id,phone,picture,status,location,email_notification,privacy,follow_request from user where username = '"
						+ name + "' and password = '" + md5Pass + "'");
		
		
		System.out.println(result + "-----------------------");
		result_bean.setValue1(result);
		HttpSession session = request.getSession();
		session.setAttribute("result1", result_bean);
		 
		if (result==null||result.length() < 2) {
			SUCCESS = "failure";
		} else {
			SUCCESS = "success1";
		}

		return mapping.findForward(SUCCESS);
	}
}
