/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.socialserver.controller;

import com.socialserver.aws.util.AWSConfig;
import com.socialserver.database.SqlBean;
import com.socialserver.foler.LoadProperties;
import com.socialserver.json.User_Search;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.simple.JSONObject;

/**
 *
 * @author Bing
 */
public class ChangeUserNameAction extends org.apache.struts.action.Action {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";
    private String newname = "";
    private String username = "";
    private String column_name = "";
    private String status = "";
    private String status1 = "";
    String extraPath = "D:\\cancer\\Source Files\\06 Mar 13";
    String baseDirectory = "";
    String path="";
    String[] pathStr;
    String web = "web";
    String IP;
    /**
     * This is the action called from the Struts framework.
     *
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        boolean val = doGet(request, response);
        if (val) {
            printParameters(request, response, val);
        } else {
            printParameters(request, response, val);
        }
        return null;
    }

    public boolean doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            SqlBean ob = new SqlBean();
            //oldname = request.getParameter("oldname").toString();
            newname = request.getParameter("newname").toString();
            username = request.getParameter("username").toString();
            String qry = "select username from user where username='" + newname + "'";
            System.out.println("qry:" + qry);
            status = ob.executeQuery(qry);
            if (status.length() == 0) {
                String sql_query = "update user,images,video,story set user.username='" + newname + "',images.username='" + newname + "',video.username='" + newname + "',story.username='" + newname + "' where user.username='" + username + "' and images.username='" + username + "' and video.username='" + username + "' and story.username='" + username + "'";
                System.out.println("Update username Qry:" + sql_query);
                status1 = ob.executeUpdate(sql_query);
                if (status1.equals("ok")) {
                    String OS = System.getProperty("os.name").toLowerCase();
                    String confFileName;
                    File configFile;
                    if (OS.indexOf("win") >= 0 &&!AWSConfig.USE_S3) {
                        confFileName = "config_windows.properties";
                        System.out.println("In window Upload Content");
                        configFile = new File(confFileName);
                    } else {
                        confFileName = "config_Server.properties";
                        System.out.println("server Content Upload");
                        configFile = new File(confFileName);
                    }
                    System.out.println("configFile:................." + configFile);
                    baseDirectory = new LoadProperties().loadPropertiesFile(confFileName);
                    System.out.println("baseDirectory:................." + baseDirectory);
                   // pathStr = baseDirectory.substring(baseDirectory.indexOf("/") + 1).split("/");
                    if (OS.indexOf("win") >= 0   &&!AWSConfig.USE_S3) {
                            System.out.println("In Window:");
                            path = extraPath + File.separator + pathStr[0] + File.separator + web + File.separator + pathStr[1] + File.separator + username + File.separator ;
                            path=path.replace('\\', '/');
                            System.out.println(" Path:"+path);

                            IP = "http://127.0.0.1:8084/";
                            System.out.println("IP:"+IP);
                        } else {
                            System.out.println("In server:");
                            //path = pathStr[0] + File.separator + File.separator + pathStr[1] + File.separator + username + File.separator ;
                           // path=path.replace('\\', '/');
                           // System.out.println(" Path:"+path);
                            IP = AWSConfig.S3_BASE_URL;
                            System.out.println("IP:"+IP);
                        }
                    /*File dir = new File((path + username));
                    File newName = new File((path + newname));
                    if (dir.isDirectory()) {
                        dir.renameTo(newName);
                    }*/
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return true;
    }

    private void printParameters(HttpServletRequest request, HttpServletResponse response, boolean val) throws IOException {
        // Set response content type
        response.setContentType("application/json");
        //response.setCharacterEncoding("charset=UTF-8");
        String value = "";
        if (val == true) {
            value = "true";
        } else {
            value = "false";
        }
        // Actual logic goes here.
        PrintWriter out = response.getWriter();
        JSONObject obj = new JSONObject();
        obj.put("param", value);
        out.println(obj);
    }
}
