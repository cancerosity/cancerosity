/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.socialserver.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.actions.DispatchAction;
import org.apache.struts.action.*;

import com.socialserver.beans.*;
import com.socialserver.database.*;
import com.socialserver.json.Success_Circle;
import com.socialserver.json.Upload_result;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;

/**
 *
 * @author Bing
 */
public class CancerFormController extends org.apache.struts.action.Action {

    /* forward name="success" path="" */
    //private static String SUCCESS = "success";
    String result_circle = "";

    /**
     * This is the action called from the Struts framework.
     *
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        if (doPost(request, response)) {
            printParameters(request, response);
        } else {
            printParameters(request, response);
        }
        return null;
    }

    public boolean doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            SqlBeanCircle sqlbean = new SqlBeanCircle();

            //LoginFormResultBean result_bean_circle = new LoginFormResultBean();
            String sqlQuery = "select"
                    + "    `images`.`sno`,"
                    + "    `images`.`username`,"
                    + "    `images`.`link_of_images`,"
                    + "    `images`.`inserted_at`,"
                    + "    `images`.`liked`,"
                    + "    `images`.`dislike`,"
                    + "    `images`.`share`,"
                    + "    `images`.`location`,"
                    + "    `user`.`picture`"
                    + "from"
                    + "    images,user "
                    + "where"
                    + " `images`.`username`=`user`.`username`"
                    + "union select "
                    + "    `video`.`sno`,"
                    + "    `video`.`username`,"
                    + "    `video`.`link_of_video`,"
                    + "    `video`.`inserted_at`,"
                    + "    `video`.`liked`,"
                    + "    `video`.`dislike`,"
                    + "    `video`.`share`,"
                    + "    `video`.`location`,"
                    + "    `user`.`picture`"
                    + "from"
                    + "    video,user "
                    + "where"
                    + " `video`.`username`=`user`.`username`"
                    + "union select "
                    + "    `story`.`sno`,"
                    + "    `story`.`username`,"
                    + "    `story`.`story`,"
                    + "    `story`.`inserted_at`,"
                    + "    `story`.`liked`,"
                    + "    `story`.`dislike`,"
                    + "    `story`.`share`,"
                    + "    `story`.`location`,"
                    + "    `user`.`picture`"
                    + "from"
                    + "    story,user  "
                    + "where"
                    + " `story`.`username`=`user`.`username`"
                    + "order by inserted_at desc LIMIT 0,3";
            System.out.println("Cancer Circle Qry:" + sqlQuery);
            result_circle = sqlbean.getdetails(sqlQuery);

            System.out.println(result_circle + "-----------------------");
            //result_bean_circle.setCircleValue(result_circle);
            //HttpSession session = request.getSession();
            //session.setAttribute("circle", result_circle);
        } catch (Exception ex) {
            Logger.getLogger(CancerFormController.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    private void printParameters(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // Set response content type
        response.setContentType("application/json");
        //response.setCharacterEncoding("charset=UTF-8");

        // Actual logic goes here.
        PrintWriter out = response.getWriter();
        out.println((JSONObject) new Success_Circle().jsonPrint(result_circle));
    }
}
