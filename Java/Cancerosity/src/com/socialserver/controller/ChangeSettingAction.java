/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.socialserver.controller;

import com.socialserver.database.SqlBean;
import com.socialserver.json.User_Search;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.simple.JSONObject;

/**
 *
 * @author Bing
 */
public class ChangeSettingAction extends org.apache.struts.action.Action {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";
    //private String oldname="";
    private String newname="";
    private String username="";
    private String column_name="";
    private String status="";
    /**
     * This is the action called from the Struts framework.
     *
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        boolean val=doGet(request, response);
        if (val) {
            printParameters(request, response,val);
        } else {
            printParameters(request, response,val);
        }
        return null;
}
    public boolean doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        try {
            SqlBean ob = new SqlBean();
            //oldname = request.getParameter("oldname").toString();
            newname = request.getParameter("newname").toString();
            username = request.getParameter("username").toString();
            column_name = request.getParameter("column_name").toString();
            String sql_query = "update user set "+column_name+" = '"+newname+"' where username='"+username+"'";
            status=ob.executeUpdate(sql_query);
            if(status.equals("ok")){
                return true;
            }
            else{
                return false;
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return true;
    }

    private void printParameters(HttpServletRequest request, HttpServletResponse response,boolean val) throws IOException {
        // Set response content type
       response.setContentType("application/json");
        //response.setCharacterEncoding("charset=UTF-8");
       String value = "";
       if(val==true){
           value="true";
       }
       else{
           value="false";
       }
        // Actual logic goes here.
        PrintWriter out = response.getWriter();
        JSONObject obj = new JSONObject();
        obj.put("param", value);
        out.println(obj);
    }
}

