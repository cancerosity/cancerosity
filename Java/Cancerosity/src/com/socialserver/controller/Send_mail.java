/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.socialserver.controller;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class Send_mail {

	public int test(String email_id, final String password) {

		Integer account_id = 0;
		Integer location_id = 0;

		String mail_subject = "Forget password test";
		int ok = 1;
		if ((ok = sendMail(email_id, account_id, location_id, mail_subject,
				password)) == 0) {
			System.out.println("Email Activation mail sent");
			return ok;
		} else
			return ok;

	}

	public int sendMail(String mail_recipient, Integer account_id,
			Integer loc_id, String mail_subject, final String password) {
		Properties props = new Properties();

		// String smtphost = "mail.globussoft.com";
		String smtphost = "smtp.gmail.com";
		// String smtphost = "smtp.mail.yahoo.com";
		String smtpport = "587";
		props.setProperty("mail.transfer.protocol", "smtps");
		props.setProperty("mail.host", smtphost.trim().replace("mail.", "")
				.trim());
		props.put("mail.smtp.host", smtphost);
		// props.put("mail.from", "");
		// props.put("mail.smtp.socketFactory.class",
		// "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.socketFactory.fallback", "false");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "587");
		props.put("mail.smtp.socketFactory.fallback", "false");
		props.put("mail.smtp.starttls.enable", "true");
		props.setProperty("mail.smtp.quitwait", "false");
		// Session session = Session.getInstance(props, null);
		Session session = Session.getDefaultInstance(props,
				new javax.mail.Authenticator() {

					@Override
					protected PasswordAuthentication getPasswordAuthentication() {

						return new PasswordAuthentication(
								"inter.huhu@gmail.com", "password");

					}
				});
		session.setDebug(true);
		try {
			MimeMessage msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress("inter.huhu@gmail.com"));
			msg.setSender(new InternetAddress("inter.huhu@gmail.com"));
			msg.setRecipient(Message.RecipientType.TO, new InternetAddress(
					mail_recipient));
			// change the second argument to new
			// InternetAddress(mail_recipients)

			// try{ msg.setRecipients(Message.RecipientType.BCC,
			// masterEmail.getBcc()); String bccMails[] =
			// masterMail.getBcc().split(","); for(int i=0; i";
			String mail_body = "Your default password is " + password
					+ " please login again with the given password";
			// Set the message conetnt

			MimeBodyPart messageBodyPart = new MimeBodyPart();

			// Filling the message
			messageBodyPart.setText(mail_body);
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);

			// Adding an attachment
			// messageBodyPart = new MimeBodyPart();
			// multipart.addBodyPart(messageBodyPart);
			msg.setContent(multipart);

			Transport.send(msg);
			System.out.println("sendMail executed succesfully !!");

		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

}
