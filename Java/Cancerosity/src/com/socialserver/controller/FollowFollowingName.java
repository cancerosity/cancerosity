/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.socialserver.controller;

import com.socialserver.database.SqlBean;
import com.socialserver.database.SqlBeanCircle;
import com.socialserver.json.Follow_Name;
import com.socialserver.json.Show_Comment;
import com.socialserver.json.Success_Circle;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.simple.JSONObject;

/**
 *
 * @author Bing
 */
public class FollowFollowingName extends org.apache.struts.action.Action {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";
    private String username="";
    String following_name="";
    String follow_name="";
    String result="";
    /**
     * This is the action called from the Struts framework.
     *
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
  if (doPost(request, response)) {
            printParameters(request, response);
        } else {
            printParameters(request, response);
        }
        return null;
    }

    public boolean doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            SqlBean db = new SqlBean();
            username=request.getParameter("username").toString();
            //LoginFormResultBean result_bean_circle = new LoginFormResultBean();
            String sqlQuery = "select follow.following,follow.status,user.picture,user.status from follow,user where follow.username='"+username+"' and user.username=follow.following";
            System.out.println("Following Name:"+sqlQuery);
            following_name = db.followingName(sqlQuery);
            
        } catch (Exception ex) {
            Logger.getLogger(CancerFormController.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    private void printParameters(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // Set response content type
        response.setContentType("application/json");
        //response.setCharacterEncoding("charset=UTF-8");

        // Actual logic goes here.
        PrintWriter out = response.getWriter();
        out.println((JSONObject) new Follow_Name().jsonPrint(following_name));
    }
}

