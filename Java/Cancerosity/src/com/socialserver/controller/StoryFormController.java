/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.socialserver.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.socialserver.beans.StoryFormBean;
import com.socialserver.beans.StoryFormResultBean;
import com.socialserver.database.SqlBean;

/**
 *
 * @author Bing
 */
public class StoryFormController extends org.apache.struts.action.Action {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";

    /**
     * This is the action called from the Struts framework.
     *
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        int x;
        String username1;
        String username;
        String user_id;
        String story;
        String sql_query;
        String result;
        String latitude;
        String longitude;
        String location = "";
        StoryFormBean obj = (StoryFormBean) form;


        username1 = obj.getUsername();
        username1.split(",");
        username = username1;
        story = obj.getStory();
        latitude = obj.getLatitude();
        longitude = obj.getLongitude();
        StringBuilder sb = new StringBuilder(story);
        String word = "'";
        System.out.println("Word=" + word);
        for (int i = -1; (i = story.indexOf(word, i + 1)) != -1;) {
            System.out.println(i);
            sb.insert(i, '\\');
            System.out.println("vishaaaaaaaalllllllllll" + sb);
        }
        SqlBean bean = new SqlBean();
        String query = "select location from user where username = '" + username + "'";
        String contentloc = bean.executeQuery(query);
        if (contentloc.equals("yes")) {
            location = new FindLocation().loc(latitude, longitude);
        } else {
            location = "";
        }
        //location= new FindLocation().loc(latitude, longitude);


        StoryFormResultBean result_bean = new StoryFormResultBean();
        sql_query = "insert into story(story,inserted_at,username,location) values('" + story + "',now(),'" + username + "','" + location + "');";
        System.out.println("insert Story:" + sql_query);
        x = bean.content_upload(sql_query);

        if (x > 0) {

            bean.get_story_detail("select story,inserted_at,username,location from story where username = '" + username + "' order by sno desc limit 1");
            result = bean.getResult();
            result_bean.setValue(result);
            System.out.println("888888888888888888888  story details from controler    "+result);
            HttpSession session = request.getSession();
            session.setAttribute("result_story", result_bean);

            return mapping.findForward("success");
        } else {
            return mapping.findForward("failure");
        }

    }
}
