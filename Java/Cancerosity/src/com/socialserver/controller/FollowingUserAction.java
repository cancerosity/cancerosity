/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.socialserver.controller;

import com.socialserver.database.SqlBean;
import com.socialserver.json.User_Search;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.simple.JSONObject;

/**
 *
 * @author Bing
 */
public class FollowingUserAction extends org.apache.struts.action.Action {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";
    String username = "";
    String following = "";
    String status = "";
    String privacy = "";
    String follow_request = "";
    String sql_query = "";
    String req="false";
    /**
     * This is the action called from the Struts framework.
     *
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        boolean val = doGet(request, response);
        if (val) {
            printParameters(request, response, val);
        } else {
            printParameters(request, response, val);
        }
        return null;
    }

    public boolean doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            SqlBean ob = new SqlBean();
            username = request.getParameter("username").toString();
            following = request.getParameter("following").toString();
            String qry = "select privacy from user where username='" + following + "'";
            privacy = ob.executeQuery(qry);
            if (privacy.equals("yes")) {
                String qry1 = "select follow_request from user where username='" + following + "'";
                follow_request = ob.executeQuery(qry1);
                //if(!follow_request.equals("none")){
                follow_request = follow_request + "," + username;
                
                sql_query = "update user set follow_request='" + follow_request + "' where username='" + following + "'";
                req="true";
            } else {
                if (username.equals(following)) {
                    return false;
                } else {
                    sql_query = "insert into follow(username,following)values('" + username + "','" + following + "')";
                    new RecentNews().news(following, "following", "you", username,"none");
                }
            }
            status = ob.executeUpdate(sql_query);
            if (status.equals("ok")) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            System.out.println(e);
        }
        return true;
    }

    private void printParameters(HttpServletRequest request, HttpServletResponse response, boolean val) throws IOException {
        // Set response content type
        response.setContentType("application/json");
        //response.setCharacterEncoding("charset=UTF-8");
        String value = "";
        if (val == true) {
            value = "true";
        } else {
            value = "false";
        }
        // Actual logic goes here.
        PrintWriter out = response.getWriter();
        //out.println((JSONObject) new User_Search().jsonPrint(x));
        JSONObject obj = new JSONObject();
        obj.put("param", value);
        obj.put("Follow_request", req);
        out.println(obj);
    }
}
