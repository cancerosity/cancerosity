/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.socialserver.controller;

import com.socialserver.database.SqlBean;
import com.socialserver.json.User_Search;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.simple.JSONObject;

/**
 *
 * @author Bing
 */
public class CommentAction extends org.apache.struts.action.Action {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";
    private String username = "";
    private String sno = "";
    private String comment = "";
    private String type = "";
    String status = "";
    String count="";

    /**
     * This is the action called from the Struts framework.
     *
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
         boolean val=doGet(request, response);
        if (val) {
            printParameters(request, response,val);
        } else {
            printParameters(request, response,val);
        }
        return null;
    }

    public boolean doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            SqlBean ob = new SqlBean();
            username = request.getParameter("username").toString(); //comment by user
            sno = request.getParameter("sno").toString(); //sno of image/video/story on which comment
            comment = request.getParameter("comment").toString(); //content of comment
            type = request.getParameter("type").toString(); //images/video/story
            String sql_query = "insert into comment(username,comment,inserted_at,type,altsno)values('" + username + "','" + comment + "',now(),'" + type + "','" + sno + "')";
            status = ob.executeUpdate(sql_query);
            if (status.equals("ok")) {
                String qry = "select count(*) from comment where altsno = '" + sno + "' and type= '" + type + "'";
                count=ob.executeQuery(qry);
                String name_qry="select username from "+type+" where sno ='"+sno+"'";
                String name=ob.executeQuery(name_qry);
                String table_name="comment"+type;
                new RecentNews().news(name, comment, table_name, username,sno);
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            System.out.println(e);
        }
        return true;
    }

    private void printParameters(HttpServletRequest request, HttpServletResponse response,boolean val) throws IOException {
        // Set response content type
        response.setContentType("application/json");
        //response.setCharacterEncoding("charset=UTF-8");
        String value = "";
        PrintWriter out = response.getWriter();
        JSONObject obj = new JSONObject();
       if(val==true){
           value="true";
           obj.put("count", count);
           obj.put("param", value);
       }
       else{
           value="false";
           obj.put("param", value);
       }
        // Actual logic goes here.
        
        //out.println((JSONObject) new User_Search().jsonPrint(x));
        
        
        out.println(obj);
    }
}
