/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.socialserver.controller;

import com.socialserver.database.SqlBean;
import com.socialserver.json.Follow_Name;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.simple.JSONObject;

/**
 *
 * @author Bing
 */
public class FollowersListAction extends org.apache.struts.action.Action {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";
    private String username="";
    String following_name="";
    String follow_name="";
    String result="";
    /**
     * This is the action called from the Struts framework.
     *
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
  if (doPost(request, response)) {
            printParameters(request, response);
        } else {
            printParameters(request, response);
        }
        return null;
    }

    public boolean doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            SqlBean db = new SqlBean();
            username=request.getParameter("username").toString();
           
            String Query = "select follow.username,follow.status,user.picture,user.status from follow,user where follow.following='"+username+"' and user.username=follow.username";
            System.out.println("Follow Name:"+Query);
            follow_name = db.followingName(Query);
        } catch (Exception ex) {
            Logger.getLogger(CancerFormController.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    private void printParameters(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // Set response content type
        response.setContentType("application/json");
        //response.setCharacterEncoding("charset=UTF-8");

        // Actual logic goes here.
        PrintWriter out = response.getWriter();
        out.println((JSONObject) new Follow_Name().jsonPrint(follow_name));
    }
}

