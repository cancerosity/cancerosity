/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.socialserver.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpSession;

import org.apache.struts.actions.DispatchAction;

import com.socialserver.aws.util.AWSConfig;
import com.socialserver.aws.util.MD5Util;
import com.socialserver.beans.*;
import com.socialserver.database.*;

/**
 *
 * @author Bing
 */
public class RegistrationFormController extends DispatchAction {

    /* forward name="success" path="" */
    private final static String SUCCESS = "success";

    /**
     * This is the Struts action method called on
     * http://.../actionPath?method=myAction1, where "method" is the value
     * specified in <action> element : ( <action parameter="method" .../> )
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {


        String name;
        String first;
        String last;
        String dob;
        String day;
        String month;
        String year;
        String phone;
        String email;
        String Gender;
        String pswd;
        //String filename;
        String result = "";
        //LoginFormBean obj =  (LoginFormBean) form;
        RegistrationFormBean obj = (RegistrationFormBean) form;
        RegistrationFormResultBean result_bean = new RegistrationFormResultBean();

        first = obj.getFirst();
        last = obj.getLast();
        day = obj.getDay();
        month = obj.getMonth();
        year = obj.getYear();
        name = obj.getName();
        phone = obj.getPhone();
        email = obj.getEmail();
        Gender = obj.getGender();
        pswd = obj.getPswd();
        dob = day + "/" + month + "/" + year;
        // filename = obj.getFilename();

        SqlBean bean = new SqlBean();
        // To check wheter username or emailID exists already or not
        String qry = "select email_id from user where email_id = '" + email + "'";
        String ExtraVal = bean.executeQuery(qry);
        System.out.println("*****ExtraValue*****" + ExtraVal);
        String check_register = bean.getdetails_register("select email_id,username from user where email_id = '" + email + "' or username = '" + name + "'");
        if (check_register == null) {
            check_register = "";
        }
        String regex = email + "," + name;
        Boolean bool = (check_register.contains(name) && check_register.contains(email));

        if (bool) {
            return mapping.findForward("registered");
        } else {
            /*if (pswd.equals("Facebook")) {
                double random = Math.random() * 575 + 1;
                int val = (int) random;
                String val1 = String.valueOf(val);
                pswd = first + val1;
                System.out.println("***Befor Enter Mail Send*****");
                PasswordMailNotification mailObj = new PasswordMailNotification();
                System.out.println("*****After Mail Send****");
                String sendto = email;
                String msg = "Your new password is " + pswd;
                mailObj.replymethod(msg, sendto);
            }*/
        	//add md5 
        	String md5Pass = MD5Util.toMd5(pswd);
            int x = bean.executesql("insert into user(name_first,name_last,dob,username,phone,email_id,password,created_at) values('" + first + "','" + last + "','" + dob + "','" + name + "','" + phone + "','" + email + "','" + md5Pass + "',now())");
            String OS = System.getProperty("os.name").toLowerCase();
            if(AWSConfig.USE_S3) {
            	System.out.println("Creating folder for this user on S3.");
            	new CreateUserFolders().createFolder(request, response, name);
            } else {
            	if (OS.indexOf("win") >= 0 &&!AWSConfig.USE_S3) {
                    new CreateUserFolders().createFolderWindows(request, response, name);
                } else {
                    new CreateUserFolders().createFolder_no_Windows(request, response, name);
                }
            }
            
            
            if (x > 0) {
                bean.executeUpdate("insert into facebook(username)values('"+name+"')");
                bean.executesql("select username from user where username='" + name + "'");
                result = bean.getResult();

                result_bean.setValue(result);
                System.out.println("Verifying the result-----------");
                System.out.println(result_bean.getValue());

                HttpSession session = request.getSession();
                session.setAttribute("result", result_bean);

                System.out.println("attribute for 'result' set as result_bean");
                RegistrationFormResultBean bean_verify = (RegistrationFormResultBean) session.getAttribute("result");
                System.out.println(bean_verify.getValue());


                System.out.println("now sending mathew to result.jsp");
                return mapping.findForward("success");
            } else {
                return mapping.findForward("failure");
            }
        }
    }
}
