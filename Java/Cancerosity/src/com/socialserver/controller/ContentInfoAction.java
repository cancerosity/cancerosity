/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.socialserver.controller;

import com.socialserver.database.SqlBeanCircle;
import com.socialserver.json.Success_Circle;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.simple.JSONObject;

/**
 *
 * @author Bing
 */
public class ContentInfoAction extends org.apache.struts.action.Action {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";
    String result_circle = "";
    String sno="";
    String type="";
    String link="";
    /**
     * This is the action called from the Struts framework.
     *
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
   if (doPost(request, response)) {
            printParameters(request, response);
        } else {
            printParameters(request, response);
        }
        return null;
    }

    public boolean doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            SqlBeanCircle sqlbean = new SqlBeanCircle();
            sno = request.getParameter("sno");
            type = request.getParameter("type");
            if(type.equals("images")){
                link = "link_of_images";
            }
            else if(type.equals("video")){
                link = "link_of_video";
            }
            else{
                link = "story";
            }
            //LoginFormResultBean result_bean_circle = new LoginFormResultBean();
            String qry = "select "+type+".sno,"+type+".username,"+type+"."+link+","+type+".inserted_at,"+type+".liked,"+type+".dislike,"+type+".share,"+type+".location,user.picture from "+type+",user where "+type+".sno='"+sno+"' and "+type+".username=user.username ";
            
            System.out.println("Content Info Qry:" + qry);
            result_circle = sqlbean.getdetails(qry);

            System.out.println(result_circle + "-----------------------");
            //result_bean_circle.setCircleValue(result_circle);
            //HttpSession session = request.getSession();
            //session.setAttribute("circle", result_circle);
        } catch (Exception ex) {
            Logger.getLogger(CancerFormController.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    private void printParameters(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // Set response content type
        response.setContentType("application/json");
        //response.setCharacterEncoding("charset=UTF-8");

        // Actual logic goes here.
        PrintWriter out = response.getWriter();
        out.println((JSONObject) new Success_Circle().jsonPrint(result_circle));
    }
}

