/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.socialserver.controller;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.commons.fileupload.FileItem;

import java.util.Iterator;

import org.apache.commons.fileupload.FileUploadException;

import java.util.List;

import javax.servlet.ServletException;

import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;

import java.io.PrintWriter;
import java.net.InetAddress;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import javax.servlet.http.HttpSession;

import org.json.simple.*;

import com.socialserver.aws.util.AWSConfig;
import com.socialserver.aws.util.S3Util;
import com.socialserver.beans.UploadContentFormBean;
import com.socialserver.database.SqlBean;
import com.socialserver.foler.*;
import com.socialserver.json.Success_json;
import com.socialserver.json.Upload_result;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 *
 * @author Bing
 */
public class EditProfileAction extends org.apache.struts.action.Action {
    /* forward name="success" path="" */

    String baseDirectory = "";
    String imageDirectory = "";
    String videoDirectory = "";
    String userWebDefinedDirectory = "";
    String userDiskDirectory = "";
    String userDirectory = "";
    String userID = "";
    String username = "";
    String user_id = "";
    String table_name = "";
    String timestamp = "";
    String filename = "";
    String sql_query = "";
    String extraPath = "D:\\cancer\\Source Files\\06 Mar 13";
    String web = "web";
    String IP;
    String[] pathStr;
    String imagePath;
    String videoPath;
    String status;
    private boolean val = false;

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        if (doPost(request, response)) {
            printParameters(request, response);
        } else {
            printParameters(request, response);
        }
        return null;
    }

    public boolean doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, FileUploadException {
        String OS = System.getProperty("os.name").toLowerCase();
        String confFileName;
        File configFile;
        if (OS.indexOf("win") >= 0 &&!AWSConfig.USE_S3) {
            confFileName = "config_windows.properties";
            configFile = new File(confFileName);
        } else {
            confFileName = "config_Server.properties";
            configFile = new File(confFileName);
        }

//        if (!configFile.exists()) {
//            //Create Properties File for the project
//            new PropertiesFile().createPropFile(getServlet().getServletConfig().getServletContext().getContextPath().toString(), confFileName);
//            baseDirectory = new LoadProperties().loadPropertiesFile(confFileName);
//            //Path to view on web Browser http://localhost:8080/Cancer_test/Cancer_circle/akash/image/vcm_s_kf_m160_160x128.jpg
//            //Path to save on disk F:\VirtuosoNetsoft\New Projects\25 Dec 2012\Web service Source\Cancer_test\web\MediaFiles 
//        } else {
//            baseDirectory = new LoadProperties().loadPropertiesFile(confFileName);
//            //baseDirectory==/Cancer_test/MediaFiles
//            pathStr = baseDirectory.substring(baseDirectory.indexOf("/") + 1).split("/");
//        }
       
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        if (!isMultipart) {
            System.out.println("false");
        } else {
            List<FileItem> items = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
            for (FileItem item : items) {
                if (item.isFormField()) {
                    // Process regular form field (input type="text|radio|checkbox|etc", select, etc).
                    String fieldname = item.getFieldName();
                    String fieldValue = item.getString();
                    if (fieldValue.indexOf(",") > 0) {
                        if (((String[]) fieldValue.split(","))[1].equals("Android")) {
                            username = ((String[]) fieldValue.split(","))[0];
                        }
                    }

                    // ... (do your job here)
                } else {
                    // Process form file field (input type="file").
                    String fieldname = item.getFieldName();
                    if (fieldname.indexOf(",") > 0) {
                        if (((String[]) fieldname.split(","))[1].equals("iphone")) {
                            username = ((String[]) fieldname.split(","))[0];
                        }
                    }
                    String userFolder = pathStr[0] + File.separator + pathStr[1] + File.separator + username + File.separator;
                    filename = FilenameUtils.getName(item.getName().replaceAll(" ", "_"));
                    filename = filename.substring(0, filename.indexOf(".")) + new Random().nextInt(100000) + filename.substring(filename.indexOf("."));
                    if (OS.indexOf("win") >= 0 &&!AWSConfig.USE_S3) {
                        imagePath = extraPath + File.separator + pathStr[0] + File.separator + web + File.separator + pathStr[1] + File.separator + userDirectory + File.separator + imageDirectory + File.separator;
                        IP = "http://127.0.0.1:8084/";
                    } else {
                        imagePath = userFolder + imageDirectory + File.separator;
                        IP = AWSConfig.S3_BASE_URL;
                    }
                    InputStream filecontent = item.getInputStream();
                    int x = 0;
                    try {
                        userID = username;
                        userDirectory = userID;
                        imageDirectory = "profile_image";

                        System.out.println("My extesion is here" + filename);
                        if ((filename.substring(filename.lastIndexOf("."), filename.length()).equalsIgnoreCase(".jpg"))
                                || (filename.substring(filename.lastIndexOf("."), filename.length()).equalsIgnoreCase(".png"))
                                || (filename.substring(filename.lastIndexOf("."), filename.length()).equalsIgnoreCase(".gif"))) {
                            userWebDefinedDirectory = userFolder + imageDirectory + File.separator;
                            userWebDefinedDirectory = userWebDefinedDirectory.replace('\\', '/');
                            System.out.println("userWebDefinedDirectory:   " + userWebDefinedDirectory);
                            userDiskDirectory = imagePath;
                            table_name = "images";
                            timestamp = getCurrentTime();
                        }
                        {
                            if (OS.indexOf("win") >= 0 &&!AWSConfig.USE_S3) {
                                File savedFile = new File(userDiskDirectory + filename);
                                item.write(savedFile);

                            } else {
//                                System.out.println("/home/cancerci/apache-tomcat-7.0.35/webapps/Cancer_final_Local_Development/MediaFiles/" + username + "/profile_image/" + filename);
//                                System.out.println("userDiskDirectory:   " + userDiskDirectory);
//                                File savedFile = new File("/home/cancerci/apache-tomcat-7.0.35/webapps/Cancer_final_Local_Development/MediaFiles/" + username + "/profile_image/" + filename);
//                                item.write(savedFile);
                            	
                            	S3Util.uploadNewFile(username+"/"+imageDirectory+"/"+filename, item.getInputStream(), item.getSize());
                            }
                        }
                        SqlBean ob = new SqlBean();
                        if (filename == null || filename.equals("")) {
                            sql_query = "UPDATE user SET status='" + status + "'"
                                    + "' WHERE username='" + username + "'";
                        } else {
                            sql_query = "UPDATE user SET picture='" + IP + username+"/"+imageDirectory+"/"+filename
                                    + "' WHERE username='" + username + "'";
                        }

                        System.out.println("Edit Profile image link:" + username+"/"+imageDirectory+"/"+filename);
                        System.out.println("sql_query edit profile:  " + sql_query);
                        x = ob.content_upload(sql_query.replace('\\', '/'));
                    } catch (Exception e) {
                        System.out.println("false");
                        System.out.println("in File uploading cancer circle exception" + e);
                        val = false;
                    }
                    System.out.println("true");

                    if (x == 0) {
                        val = false;
                    } else {
                        val = true;
                    }
                }
            }
        }

        return val;
    }

    public String getCurrentTime() {

        DateTime dt = new DateTime();
        DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        String str = fmt.print(dt);
        System.out.println(str);
        return str;




        //DateFormat dateFormat = new SimpleDateFormat("dd/mm/yyyy HH:mm:ss");
        //Date date = new Date();
        //return dateFormat.format(date);
    }

    private void printParameters(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // Set response content type
        response.setContentType("application/json");
        //response.setCharacterEncoding("charset=UTF-8");

        // Actual logic goes here.
        PrintWriter out = response.getWriter();
        String parameters = userID + ","
                + IP+userWebDefinedDirectory + filename + ","
                + timestamp;

        out.println((JSONObject) new Upload_result().jsonEditProfile(parameters));
    }
}
