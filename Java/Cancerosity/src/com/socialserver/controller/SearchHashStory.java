/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.socialserver.controller;

import com.socialserver.database.SqlBean;
import com.socialserver.json.Story_Search;
import com.socialserver.json.Success_Circle;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.simple.JSONObject;

/**
 *
 * @author Bing
 */
public class SearchHashStory extends org.apache.struts.action.Action {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";
    private String hashvalue="";
    private String x="";
    /**
     * This is the action called from the Struts framework.
     *
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        
        if (doGet(request, response)) {
            printParameters(request, response);
        } else {
            printParameters(request, response);
        }
        return null;
    }
public boolean doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        try {
            SqlBean ob = new SqlBean();
            hashvalue = request.getParameter("hashvalue").toString();
            //hashvalue = hashvalue.substring(1);
            hashvalue = "#"+hashvalue;
            String sql_query = "select story.sno,story.username,story.story,story.liked,story.dislike,story.location,story.inserted_at,user.picture from story,user where story.story like '%"+hashvalue+"%' and story.username=user.username";
            System.out.println("sql_query"+sql_query);
            x=ob.searchStory(sql_query);
           
        } catch (Exception e) {
            System.out.println(e);
        }
        return true;
    }

    private void printParameters(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // Set response content type
       response.setContentType("application/json");
        //response.setCharacterEncoding("charset=UTF-8");

        // Actual logic goes here.
        PrintWriter out = response.getWriter();
        out.println((JSONObject) new Story_Search().jsonPrint(x));
    }
}
