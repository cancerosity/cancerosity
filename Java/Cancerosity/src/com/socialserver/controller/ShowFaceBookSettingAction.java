/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.socialserver.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.simple.JSONObject;

import com.socialserver.database.SqlBean;

/**
 *
 * @author Bing
 */
public class ShowFaceBookSettingAction extends org.apache.struts.action.Action {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";
    String username="";
    String fbuser="";
    String token="";
    String status="";
    String[] strResult;
    /**
     * This is the action called from the Struts framework.
     *
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
      boolean val = doGet(request, response);
        if (val) {
            printParameters(request, response, val);
        } else {
            printParameters(request, response, val);
        }
        return null;
    }

    public boolean doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try{
            SqlBean db = new SqlBean();
            username=request.getParameter("username");
            String qry="select fbuser,token from facebook where username='"+username+"'";
            status=db.showFbPosting(qry);
            if(status.length()>0){
                strResult=status.split("-END-");
                fbuser=strResult[0];
                token=strResult[1];
                return true;
            }
            else{
                return false;
            }
        }
        catch(Exception e){
            System.out.println(e);
        }
        return true;
        
    }

    private void printParameters(HttpServletRequest request, HttpServletResponse response, boolean val) throws IOException {
        String value="";
        PrintWriter out= response.getWriter();
        JSONObject obj = new JSONObject();
        if(val==true){
            value="true";
            obj.put("fbuser", fbuser);
            obj.put("token", token);
        }
        else{
            value="false";
        }
        obj.put("param", value);
        out.println(obj);
    }
}

