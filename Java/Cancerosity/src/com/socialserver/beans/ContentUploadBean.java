/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.socialserver.beans;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 *
 * @author Bing
 */
public class ContentUploadBean extends org.apache.struts.action.ActionForm {
    
    private String username;
    private String file;
    private String destination;
    private int user_id;

    /**
     * @return
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param string
     */
    public void setUsername(String string) {
        username = string;
    }
    
    
    
    
    public String getFilename() {
        return file;
    }

    /**
     * @param string
     */
    public void setFilename(String string) {
        file = string;
    }
    
    
   

    /**
     * @return
     */
    
    
    
     public String getDestination() {
        return destination;
    }

    /**
     * @param string
     */
    public void setDestination(String string) {
        destination = string;
    }
    
    
    
    
    
    public int getUserId() {
        return user_id;
    }

    /**
     * @param i
     */
    public void setUserId(int i) {
        user_id = i;
    }

    /**
     *
     */
    public ContentUploadBean() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param request The HTTP Request we are processing.
     * @return
     */
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        if (getUsername() == null || getUsername().length() < 1) {
            errors.add("name", new ActionMessage("error.name.required"));
            // TODO: add 'error.name.required' key to your resources
        }
        return errors;
    }
}
