/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.socialserver.beans;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 *
 * @author Bing
 */
public class RegistrationFormBean extends org.apache.struts.action.ActionForm {
    
    private String name;
    private String dob;
    private String day;
    private String month;
    private String year;
    private String first;
    private String last;
    private String phone;
    private String email;
    private String Gender;
    private String pswd;
    private String filename;

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public String getLast() {
        return last;
    }

    public void setLast(String last) {
        this.last = last;
    }
    
    
    
    public String getName() {
        return name;
    }

    /**
     * @param string
     */
    public void setName(String string) {
        name = string;
    }

    /**
     * @return
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param i
     */
    public void setPhone(String string) {
        phone = string;
    }

    public String getEmail(){
        return email;
    }
    
    public void setEmail(String string){
        email = string;
    }
    
     public String getGender(){
        return Gender;
    }
    
    public void setGender(String string){
        Gender = string;
        
    }
    
     public String getPswd(){
        return pswd;
    }
    
    public void setPswd(String string){
       pswd = string;
    }
    
   /*
    public String getFilename(){
        return filename;
    }
    
    public void setFilename(String string){
       filename = string;
    }
    /**
     *
     */
    public RegistrationFormBean() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param request The HTTP Request we are processing.
     * @return
     */
    /*
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        if (getName() == null || getName().length() < 1) {
            errors.add("name", new ActionMessage("error.name.required"));
            // TODO: add 'error.name.required' key to your resources
        }
        if (getPhone() == null || getPhone().length() < 1) {
            errors.add("phone", new ActionMessage("error.phone.required"));
            // TODO: add 'error.name.required' key to your resources
        }
        if (getEmail() == null || getEmail().length() < 1) {
            errors.add("email", new ActionMessage("error.email.required"));
            // TODO: add 'error.name.required' key to your resources
        }
        if (getGender() == null || getGender().length() < 1) {
            errors.add("gender", new ActionMessage("error.gender.required"));
            // TODO: add 'error.name.required' key to your resources
        }
        
         if (getPswd() == null || getPswd().length() < 1) {
            errors.add("pswd", new ActionMessage("error.pswd.required"));
            // TODO: add 'error.name.required' key to your resources
        }
         */
         
       /*  if (getFilename() == null || getFilename().length() < 1) {
            errors.add("filename", new ActionMessage("error.filename.required"));
            // TODO: add 'error.name.required' key to your resources
        }
         */
        //return errors;
   // }
    
    public void reset(ActionMapping mapping, HttpServletRequest request) {
	        this.pswd = null;
                this.email = null;
                this.phone = null;
	    }
}
