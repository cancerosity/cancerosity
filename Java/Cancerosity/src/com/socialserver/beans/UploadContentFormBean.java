 
package com.socialserver.beans;


/**
 * 
 * @author Bing
 *
 */
public class UploadContentFormBean extends org.apache.struts.action.ActionForm {
    
    private int user_id;
    private String name_of_media="";
    private String link_of_media="";
    private String preview_of_media="";
    private String timestamp="";

    public String getLink_of_media() {
        return link_of_media;
    }

    public void setLink_of_media(String link_of_media) {
        this.link_of_media = link_of_media;
    }

    public String getName_of_media() {
        return name_of_media;
    }

    public void setName_of_media(String name_of_media) {
        this.name_of_media = name_of_media;
    }

    public String getPreview_of_media() {
        return preview_of_media;
    }

    public void setPreview_of_media(String preview_of_media) {
        this.preview_of_media = preview_of_media;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    
}
