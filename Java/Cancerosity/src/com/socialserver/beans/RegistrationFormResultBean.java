 
package com.socialserver.beans;

public class RegistrationFormResultBean {

	String value;

	public RegistrationFormResultBean() {
		value = " ";
	}

	public String getValue() {
		return value;
	}

	public void setValue(String v) {
		value = v;
	}

}
