/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.socialserver.beans;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 *
 * @author Bing
 */
public class ForgetPasswordBean extends org.apache.struts.action.ActionForm {
    
    private String contact;
    private String email;

    /**
     * @return
     */
    public String getContact() {
        return contact;
    }

    /**
     * @param string
     */
    public void setContact(String string) {
        contact = string;
    }

    /**
     * @return
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param i
     */
    public void setEmail(String i) {
        email = i;
    }

    /**
     *
     */
    public ForgetPasswordBean() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param request The HTTP Request we are processing.
     * @return
     */
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        if (getContact() == null || getContact().length() < 1) {
            errors.add("contact", new ActionMessage("error.contact.required"));
            // TODO: add 'error.name.required' key to your resources
        }
        return errors;
    }
}
