 
DROP DATABASE IF EXISTS `socialserver`;
CREATE DATABASE `socialserver` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `socialserver`;

#
# Source for table comment
#

DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment` (
  `sno` int(100) NOT NULL AUTO_INCREMENT,
  `username` varchar(500) NOT NULL,
  `comment` varchar(4000) DEFAULT NULL,
  `liked` varchar(4000) NOT NULL DEFAULT '0',
  `dislike` varchar(4000) NOT NULL DEFAULT '0',
  `inserted_at` datetime NOT NULL,
  `type` varchar(50) NOT NULL,
  `altsno` varchar(500) NOT NULL,
  PRIMARY KEY (`sno`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

#
# Source for table facebook
#

DROP TABLE IF EXISTS `facebook`;
CREATE TABLE `facebook` (
  `sno` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(500) NOT NULL DEFAULT '',
  `fbuser` varchar(500) NOT NULL DEFAULT 'false',
  `token` varchar(1000) NOT NULL DEFAULT 'false',
  PRIMARY KEY (`sno`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

#
# Source for table follow
#

DROP TABLE IF EXISTS `follow`;
CREATE TABLE `follow` (
  `sno` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `following` varchar(100) NOT NULL,
  PRIMARY KEY (`sno`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

#
# Source for table friends
#

DROP TABLE IF EXISTS `friends`;
CREATE TABLE `friends` (
  `sno` int(11) NOT NULL AUTO_INCREMENT,
  `friends` varchar(4000) DEFAULT NULL,
  PRIMARY KEY (`sno`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Source for table images
#

DROP TABLE IF EXISTS `images`;
CREATE TABLE `images` (
  `sno` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `name_of_images` varchar(1000) DEFAULT NULL,
  `link_of_images` varchar(1000) DEFAULT NULL,
  `preview` varchar(1000) NOT NULL DEFAULT '/web/image/default.gif',
  `inserted_at` datetime DEFAULT NULL,
  `liked` varchar(4000) NOT NULL DEFAULT '0',
  `dislike` varchar(4000) NOT NULL DEFAULT '0',
  `share` varchar(4000) NOT NULL DEFAULT '0',
  `location` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`sno`),
  KEY `user_id` (`sno`),
  KEY `id_index` (`sno`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Source for table notification
#

DROP TABLE IF EXISTS `notification`;
CREATE TABLE `notification` (
  `sno` int(15) NOT NULL AUTO_INCREMENT,
  `username` varchar(500) NOT NULL,
  `action` varchar(2000) NOT NULL,
  `type` varchar(100) NOT NULL,
  `post_by` varchar(500) NOT NULL,
  `inserted_at` datetime NOT NULL,
  `content_sno` varchar(50) NOT NULL,
  PRIMARY KEY (`sno`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

#
# Source for table report
#

DROP TABLE IF EXISTS `report`;
CREATE TABLE `report` (
  `sno` int(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(4000) NOT NULL,
  `content_no` varchar(4000) NOT NULL,
  `content_type` varchar(4000) NOT NULL,
  `content` varchar(4000) NOT NULL,
  `reason` varchar(2000) DEFAULT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'no',
  `count` int(50) NOT NULL DEFAULT '1',
  PRIMARY KEY (`sno`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

#
# Source for table story
#

DROP TABLE IF EXISTS `story`;
CREATE TABLE `story` (
  `sno` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `story` varchar(1000) DEFAULT NULL,
  `inserted_at` datetime NOT NULL,
  `liked` varchar(4000) NOT NULL DEFAULT '0',
  `dislike` varchar(4000) NOT NULL DEFAULT '0',
  `share` varchar(4000) NOT NULL DEFAULT '0',
  `location` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`sno`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Source for table user
#

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `username` varchar(45) NOT NULL DEFAULT '',
  `password` varchar(45) DEFAULT NULL,
  `name_first` varchar(45) DEFAULT NULL,
  `name_last` varchar(45) DEFAULT NULL,
  `dob` varchar(50) DEFAULT NULL,
  `email_id` varchar(500) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `picture` varchar(1000) NOT NULL DEFAULT 'https://s3-ap-southeast-2.amazonaws.com/filesmarcapp/editPic.png',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `lastlogin` datetime DEFAULT NULL,
  `numlogins` int(8) DEFAULT NULL,
  `status` varchar(1000) NOT NULL DEFAULT 'Please Update Your Status',
  `sno` int(11) NOT NULL AUTO_INCREMENT,
  `location` varchar(1000) NOT NULL DEFAULT 'yes',
  `email_notification` varchar(50) NOT NULL DEFAULT 'yes',
  `privacy` varchar(50) NOT NULL DEFAULT 'no',
  `follow_request` varchar(4000) NOT NULL DEFAULT 'none',
  PRIMARY KEY (`sno`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

#
# Source for table video
#

DROP TABLE IF EXISTS `video`;
CREATE TABLE `video` (
  `sno` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `name_of_video` varchar(1000) NOT NULL,
  `link_of_video` varchar(1000) NOT NULL,
  `preview` varchar(1000) NOT NULL DEFAULT '/web/image/default.gif',
  `inserted_at` datetime DEFAULT NULL,
  `liked` varchar(4000) NOT NULL DEFAULT '0',
  `dislike` varchar(4000) NOT NULL DEFAULT '0',
  `share` varchar(4000) NOT NULL DEFAULT '0',
  `comment` varchar(4000) NOT NULL DEFAULT '0',
  `location` varchar(4000) DEFAULT NULL,
  PRIMARY KEY (`sno`),
  KEY `user_id` (`sno`),
  KEY `id_index` (`sno`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
