<%-- 
    Document   : first_login
    Created on : Mar 18, 2013, 10:59:07 AM
    Author     : Bing
--%>

<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<html:html lang="true">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><bean:message key="welcome.title"/></title>
        <html:base/>
    </head>
    <body style="background-color: white">
        
        <html:form action="/login?method=myAction2" method="post">
            <table>
                <tr>
                    <td>User Name</td>
                    <td><input type="text" name="username" /></td>
                </tr>
                <tr>
                    <td>Password</td>
                    <td><input type="password" name ="password" /></td>
                </tr>
                <tr>
                    <td><input type="submit" value ="Submit" /></td>
                </tr>
                <tr>
                    <td><a href="forget_password.jsp">Forget Password</a>
        </td>
                </tr>
                <tr>
                    <td><a href="registration.jsp">Sign Up for New User</a>
        </td>
                </tr>
            </table>
        </html:form>
        
    </body>
</html:html>

