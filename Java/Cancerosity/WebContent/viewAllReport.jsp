<!DOCTYPE html>
<%@page import="java.sql.Connection"%>
<%@page import="com.socialserver.database.ConnectionManager"%>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%@page import="java.sql.Statement"%>
        <%@page import="java.sql.ResultSet"%>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>All Reports</title>
        <link href="css/style.css" rel="stylesheet" type="text/css" />

    </head>

    <body>

        <div id="container">
            <div class="bannerWrap"><img src="images/banner2.png" width="811" height="276" /></div>
            <div class="clr"></div>

            <table width="800" border="1">
                <tr>
                    <td>Content Type</td>
                    <td>Content</td>
                    <td>Cont</td>
                    <td>Detail</td>

                </tr>

                <%
                					Connection conn = ConnectionManager.getMysqlConnection();
                		
                                    String[] content = new String[5000];
                                    String[] contentCount = new String[5000];

                                    String[] content_no = new String[5000];
                                    String[] content_type = new String[5000];

                                    int i = 0, j = 0;

                                    ResultSet rsContCount = null, rsContDetail = null;
                                    Statement stmtContCount, stmtContDetail;
                                    stmtContCount = conn.createStatement();
                                    stmtContDetail = conn.createStatement();
                                    String getContCount = "select content,count(*) from report where status like 'no' group by content";
                                    String getContDetail = "";
                                    try {
                                        rsContCount = stmtContCount.executeQuery(getContCount);
                                        while (rsContCount.next()) {
                                            content[i] = rsContCount.getString(1);
                                            contentCount[i] = rsContCount.getString(2);
                                            i++;
                                        }
                                        for (int jj = 0; jj < i; jj++) {
                                            getContDetail = "select content,content_type,content_no from report where content like '" + content[jj] + "%' and status like 'no' ";
                                            rsContDetail = stmtContDetail.executeQuery(getContDetail);
                                            if(rsContDetail.next()) {
                %>
                <tr>
                    <td><%=rsContDetail.getString(2)%></td>
                    <% if ((rsContDetail.getString(2)).equalsIgnoreCase("images")) {
                    %>
                    <td> <center><img border="0" src="<%=rsContDetail.getString(1)%>"  width="50" height="50" /></center></td>
                    <%} else if ((rsContDetail.getString(2)).equalsIgnoreCase("video")) {
                    %>
                    <td><center>
                            <video width="230" height="100" controls>
                                <source src="<%=rsContDetail.getString(1)%>" type="video/mp4" />

                            </video>
                        </center></td>
                        <%} else {%>
                    <td><%=rsContDetail.getString(1)%></td>
                    <%
                    }
                    %>

                    <td><%=contentCount[jj]%></td>
                    <td><a href="deleteContentProcess.jsp?contentType=<%=rsContDetail.getString(2)%>&contentSno=<%=rsContDetail.getString(3)%>" >Delete</a> </td>
                </tr>
                <%


                            }
                        }
                    } catch (Exception ex) {
                    	ex.printStackTrace();
                        System.out.println("we are her ewhile expt" + ex);
                    } finally {
                    	ConnectionManager.freeResource(rsContCount, stmtContCount, conn);
                    	ConnectionManager.freeResource(rsContDetail, stmtContDetail, conn);
                    }
                    
                    %>




            </table>

            <div style="background:#f2efef; width:811px; float:left; height:25px; margin-top:40px; color:#9c9d9e; text-align:center;"></div>

        </div>

    </body>
</html>
