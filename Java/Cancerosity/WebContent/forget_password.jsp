<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<html:html lang="true">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><bean:message key="welcome.title"/></title>
        <html:base/>
    </head>
    <body style="background-color: white">
        
        <html:form action="/forget_password" method="post">
            <table>
                <tr>
                    <td>User contact</td>
                    <td><input type="text" name="contact" /></td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td><input type="text" name ="email" /></td>
                </tr>
                <tr>
                    <td><input type="submit" value ="Submit" /></td>
                </tr>
                
            </table>
        </html:form>
        
    </body>
</html:html>
